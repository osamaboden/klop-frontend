<?php

namespace App\Http\Controllers;

use App\Models\ProdukModel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//login &register & logout
Route::get('/',[HomeController::class, 'home'])->name('home');
Route::get('/login', [HomeController::class, 'login'])->name('login');
Route::get('/register', [HomeController::class, 'register'])->name('login');
Route::get('/forget', [HomeController::class, 'forget'])->name('login');
Route::get('/checkdata', [HomeController::class, 'uniquedata'])->name('checkdata');
Route::post('/postRegister', [HomeController::class, 'postregister'])->name('postregister');
Route::get('/verifikasi', [HomeController::class, 'verifikasi'])->name('verifikasi');
Route::post('/postlogin', [HomeController::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [HomeController::class, 'logout'])->name('logout');


//sendEmailVerifiy
Route::get('/sendemail/verify/{token}', [HomeController::class, 'sendverify'])->name('sendverify');
Route::get('/resendVerify', [HomeController::class, 'resendverify'])->name('resendverify');

//sendEmailForgetPassword
Route::post('/sendemail/forgetpass', [HomeController::class, 'forgetpass'])->name('forgetpass');
Route::get('/forgot', [HomeController::class, 'forgot'])->name('forgot');
Route::post('/resetpassword', [HomeController::class, 'resetpassword'])->name('resetpassword');

//produk
Route::get('/ulasan', [ProdukController::class, 'ulasan'])->name('ulasan');
Route::get('/gallery/gambar', [ProdukController::class, 'semua_gambar'])->name('semua_gambar');
Route::get('/gallery/instalasi', [ProdukController::class, 'semua_installasi'])->name('semua_gambar');
Route::get('/gallery/video', [ProdukController::class, 'semua_video'])->name('semua_video');
Route::get('/produk/detail/{meta}', [ProdukController::class, 'detail'])->name('detail');

Route::get('/produk/harga', [ProdukController::class, 'harga'])->name('harga');

Route::post('/produk/savekeranjang', [ProdukController::class, 'savekeranjang'])->middleware('auth')->name('savekeranjang');
Route::get('/produk/keranjangdelete', [ProdukController::class, 'keranjangdelete'])->middleware('auth')->name('keranjangdelete');

Route::get('/keranjang', [KeranjangController::class, 'keranjang'])->middleware('auth')->name('keranjang');
Route::post('/keranjang/update', [KeranjangController::class, 'updatekeranjang'])->middleware('auth')->name('updatekeranjang');

//checkout

Route::get('/checkout', [CheckoutController::class, 'index'])->middleware('auth')->name('checkout');
Route::get('/alamat', [CheckoutController::class, 'alamat'])->middleware('auth')->name('alamat');
Route::get('/hapus/alamat', [CheckoutController::class, 'hapusalamat'])->middleware('auth')->name('hapusalamat');
Route::get('/alamat/{id}', [CheckoutController::class, 'ubahalamat'])->middleware('auth')->name('alamat');
Route::post('/checkout/simpanalamat', [CheckoutController::class, 'simpanalamat'])->middleware('auth')->name('simpanalamat');
Route::get('/checkout/pemasangan', [CheckoutController::class, 'pemasangan'])->middleware('auth')->name('pemasangan');
Route::get('/checkout/changealamat', [CheckoutController::class, 'changealamat'])->middleware('auth')->name('changealamat');
Route::post('/checkout/simpan', [CheckoutController::class, 'simpanalamat'])->middleware('auth')->name('simpanalamat');
Route::get('/success/{id}', [CheckoutController::class, 'success'])->middleware('auth')->name('success');
Route::post('/saveorder', [CheckoutController::class, 'saveorder'])->middleware('auth')->name('saveorder');


//wilayahadiminstrasi

Route::get('/listkabupaten', [CheckoutController::class, 'kabupaten'])->name('kabupaten');
Route::get('/listkecamatan', [CheckoutController::class, 'kecamatan'])->name('kecamatan');


//transaksi

Route::get('/transaksi', [TransaksiController::class, 'daftar'])->middleware('auth')->name('transaksi');
Route::get('/transaksi/detail/{id}', [TransaksiController::class, 'detail'])->middleware('auth')->name('detail');
Route::get('/konfirmasi', [TransaksiController::class, 'konfirmasi'])->middleware('auth')->name('konfirmasi');
Route::get('/konfirmasi/check', [TransaksiController::class, 'check_konfirmasi'])->middleware('auth')->name('check');
Route::post('/postkonfirmasi', [TransaksiController::class, 'postkonfirmasi'])->middleware('auth')->name('postkonfirmasi');
Route::get('/transaksi/cancel', [TransaksiController::class, 'cancel'])->middleware('auth')->name('cancel');
Route::post('/setreview', [TransaksiController::class, 'setreview'])->middleware('auth')->name('setreview');

//profile

Route::get('/profil', [ProfilController::class, 'profil'])->middleware('auth')->name('profil');
Route::get('/validatepassword', [ProfilController::class, 'validatepassword'])->middleware('auth')->name('validatepassword');
Route::get('/validateemail', [ProfilController::class, 'validateemail'])->middleware('auth')->name('validateemail');
Route::post('/password/reset', [ProfilController::class, 'resetpassword'])->middleware('auth')->name('resetpassword');
Route::post('/update/biodata', [ProfilController::class, 'updatebiodata'])->middleware('auth')->name('updatebiodata');
Route::post('/changeprofil', [ProfilController::class, 'changeprofil'])->middleware('auth')->name('changeprofil');

//page
Route::get('/syarat-dan-ketentuan', [HomeController::class, 'syarat'])->name('page');
Route::get('/kebijakan-privasi', [HomeController::class, 'kebijakan'])->name('page');
Route::get('/tentang-kami', [HomeController::class, 'about'])->name('page');