<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/dologin', [ApiController::class, 'dologin'])->name('dologin');
Route::post('/updateprofile', [ApiController::class, 'updateprofile'])->name('updateprofile');
Route::post('/registrasi', [ApiController::class, 'registrasi'])->name('registrasi');
Route::post('/homedata', [ApiController::class, 'homedata'])->name('homedata');
Route::post('/detailproduk', [ApiController::class, 'detailproduk'])->name('detailproduk');
Route::post('/simpankeranjang', [ApiController::class, 'simpankeranjang'])->name('simpankeranjang');
Route::post('/precheckout', [ApiController::class, 'precheckout'])->name('precheckout');
Route::post('/getprovinsi', [ApiController::class, 'getprovinsi'])->name('getprovinsi');
Route::post('/getkabupaten', [ApiController::class, 'getkabupaten'])->name('getkabupaten');
Route::post('/getkecamatan', [ApiController::class, 'getkecamatan'])->name('getkecamatan');
Route::post('/simpanalamat', [ApiController::class, 'simpanalamat'])->name('simpanalamat');
Route::post('/simpanorder', [ApiController::class, 'simpanorder'])->name('simpanorder');
Route::post('/getongkoskirim', [ApiController::class, 'getongkoskirim'])->name('getongkoskirim');
Route::post('/getdaftaralamat', [ApiController::class, 'getdaftaralamat'])->name('getdaftaralamat');
Route::post('/getdataalamat', [ApiController::class, 'getdataalamat'])->name('getdataalamat');
Route::post('/getcontent', [ApiController::class, 'getcontent'])->name('getcontent');
Route::post('/getdaftarkeranjang', [ApiController::class, 'getdaftarkeranjang'])->name('getdaftarkeranjang');
Route::post('/hapuskeranjang', [ApiController::class, 'hapuskeranjang'])->name('hapuskeranjang');
Route::post('/getriwayat', [ApiController::class, 'getriwayat'])->name('getriwayat');
Route::post('/detailtransaksi', [ApiController::class, 'detailtransaksi'])->name('detailtransaksi');
Route::post('/bataltransaksi', [ApiController::class, 'bataltransaksi'])->name('bataltransaksi');
Route::post('/hapusalamat', [ApiController::class, 'hapusalamat'])->name('hapusalamat');
Route::post('/reorder', [ApiController::class, 'reorder'])->name('reorder');
Route::post('/daftartestimoni', [ApiController::class, 'daftartestimoni'])->name('daftartestimoni');
Route::post('/daftargaleri', [ApiController::class, 'daftargaleri'])->name('daftargaleri');
Route::post('/slidegaleri', [ApiController::class, 'slidegaleri'])->name('slidegaleri');
Route::post('/daftargalerivideo', [ApiController::class, 'daftargalerivideo'])->name('daftargalerivideo');
Route::post('/uploadavatar', [ApiController::class, 'uploadavatar'])->name('uploadavatar');
Route::post('/lupapassword', [ApiController::class, 'lupapassword'])->name('lupapassword');
Route::post('/agenlogin', [ApiController::class, 'agenlogin'])->name('agenlogin');
Route::post('/agentask', [ApiController::class, 'agentask'])->name('agentask');
Route::post('/agendetailtask', [ApiController::class, 'agendetailtask'])->name('agendetailtask');
Route::post('/agenbiodata', [ApiController::class, 'agenbiodata'])->name('agenbiodata');
Route::post('/agenselesai', [ApiController::class, 'agenselesai'])->name('agenselesai');