$(window).ready(function(){
    $('select').css('width', '100%')
    $('.form-select').select2({
        theme: "bootstrap-5",
        width: 'resolve',
    })
    $('#provinsi').change(function(){
        $('#kabupaten').val(null).trigger('change');
        var id_prov = $(this).val();
        $('#kabupaten option').remove();
        $.ajax({
            url:'/listkabupaten',
            type:'GET',
            data:'id='+id_prov,
            beforeSend:function(){
                showLoadingSwal();
            },
            success:function(result){
                Swal.close();
                if(result.is_success && id_prov!=""){
                    $('#kabupaten').append('<option></option>');
                    $('#kabupaten').select2({
                        theme: "bootstrap-5",
                        data: result.data,
                        width: 'resolve'
                    })
                    if($('#kabupaten').attr('data-selected')){
                       $('#kabupaten').val($('#kabupaten').attr('data-selected')).trigger('change') 
                    }
                    $('#kabupaten').attr('disabled', false);
                    $('#input-address').trigger('change');
                }else{
                    $('#kabupaten').attr('disabled', true);
                }
            }, error:function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status != 200){
                    Swal.close();
                    Swal.fire({
                        icon:'error',
                        text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
                        customClass:{
                            header: 'custom-swal'
                        },
                    })
                    console.log(errorThrown);
                }
            }
        })
    })

    $('#kabupaten').change(function(){
        var id_kab = $(this).val();
        $('#kecamatan option').remove();
        $.ajax({
            url:'/listkecamatan',
            type:'GET',
            data:'id='+id_kab,
            beforeSend:function(){
                showLoadingSwal();
            },
            success:function(result){
                Swal.close();
                if(result.is_success && id_kab!=""){
                    $('#kecamatan').append('<option></option>');
                    $('#kecamatan').select2({
                        theme: "bootstrap-5",
                        data: result.data,
                        width: 'resolve' 
                    })
                    if($('#kecamatan').attr('data-selected')){
                        $('#kecamatan').val($('#kecamatan').attr('data-selected')).trigger('change') 
                     }
                    $('#kecamatan').attr('disabled', false);
                    $('#input-address').trigger('change');
                }else{
                    $('#kecamatan').attr('disabled', true);
                }
            }, error:function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status != 200){
                    Swal.close();
                    Swal.fire({
                        icon:'error',
                        text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
                        customClass:{
                            header: 'custom-swal'
                        },
                    })
                    console.log(errorThrown);
                }
            }
        })
    })

    $('#kecamatan').change(function(){
        $('#input-address').trigger('change');
    })
    
    getLocation();
    if($('input[name="id_alamat"]').length > 0){
        $('#provinsi').trigger('change');  
    }
})

let map, lati="-7.7910403", long = "110.3714089", markerMap;

function initMap() {
    if($('input[name="id_alamat"]').length > 0){
        var koordinat = $('#koordinat').val().split(',');
        lati = koordinat[0];
        long = koordinat[1];
    }
  map = new google.maps.Map(document.getElementById("maps-box"), {
    center: { lat: parseFloat(lati), lng: parseFloat(long) },
    zoom: 13,
  });
  setMarker({ lat: parseFloat(lati), lng: parseFloat(long) })
  const geocoder = new google.maps.Geocoder();
  
//   map.addListener("click", (mapsMouseEvent) => {
//     geocoder.geocode({ location: mapsMouseEvent.latLng }, (results, status) => {
//         if (status === "OK") {
//             // console.log(results);
//             setAlamat(results[0])
//             setMarker(results[0].geometry.location);
//             $('#koordinat').val(results[0].geometry.viewport.Ua.g+","+results[0].geometry.viewport.La.g)
//         } else {
//         //   alert("Geocode was not successful for the following reason: " + status);
//         }
//       });
//   });

  function setMarker(position){
      if(markerMap){
        markerMap.setMap(null);
      }
      map.setCenter(position)
      markerMap = new google.maps.Marker({
        map: map,
        position: position,
        draggable: false
      });
    //   google.maps.event.addListener(markerMap, "dragend", function (e) {
    //     // console.log(e);
    //     // console.log(markerMap.getPosition())
    //     geocoder.geocode({ location: markerMap.getPosition() }, (results, status) => {
    //         if (status === "OK") {
    //             setAlamat(results[0])
    //             setMarker(results[0].geometry.location);
    //             $('#koordinat').val(results[0].geometry.viewport.Ua.g+","+results[0].geometry.viewport.La.g)
    //         } else {
    //         //   alert("Geocode was not successful for the following reason: " + status);
    //         }
    //       });
    //   })
  }

  function setAlamat(address){
    var urutan = ['route', 'street_number', 'administrative_area_level_5', 'administrative_area_level_4'];
    var alamat = "";
    $.each(urutan, function(k, type){
        $.each(address.address_components, function(key,value){
            if(type == value.types[0]){
                if(value.types[0] == "street_number"){
                    alamat += 'No.'+value.long_name+" ";
                }else{
                    alamat += value.long_name+" ";
                }
            }
        })
    })
    $('#input-address').val(alamat);
  }

  $("#input-address").change(function(){
      var val = $(this).val();
      var provinsi = $('#provinsi option:selected').text();
      var kabupaten = $('#kabupaten option:selected').text();
      var kecamatan = $('#kecamatan option:selected').text();
      if(val != "" && kecamatan != "" && kabupaten !="" && provinsi!=""){
        var add = val+", "+kecamatan+", "+kabupaten+", "+provinsi;
        geocoder.geocode({ address: add }, (results, status) => {
            if (status === "OK") {
                // console.log(results);
                // setAlamat(results[0])
                setMarker(results[0].geometry.location);
                $('#koordinat').val(results[0].geometry.viewport.Ua.g+","+results[0].geometry.viewport.La.g)
            } else {
            //   alert("Geocode was not successful for the following reason: " + status);
            }
          });
      }
    $( "#input-address" ).keypress(function(a) {
        if(a.which === 32){
            return true;
        }
    });
  })
}



function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(setLatLong);
    } else {
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  }

  function setLatLong(position){
    if(position.coords.latitude!=""){
        lati = position.coords.latitude;
        long = position.coords.longitude;
    }
  }

  $('#AlamatForm').validate({
    rules:{
        label_alamat:{
            required:{
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            }
        },
        provinsi:{
            required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
        },
        kabupaten:{
            required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
        },
        kecamatan:{
            required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
        },
        alamat:{
            required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
        },
    },
    messages:{
        label_alamat: "Label alamat Harus Diisi",
        provinsi: "Provinsi Harus Diisi",
        kabupaten: "Kabupaten Harus Diisi",
        kecamatan: "Kecamatan Harus Diisi",
        alamat: "Alamat Harus Diisi",
    },
    errorPlacement: function (error, element) {
        var name = $(element).attr("name");
        error.appendTo($("#" + name + "_error"));
    }
});

$('#SimpanAlamat').click(function(){
    if($('#AlamatForm').valid()){
        var post = $('#AlamatForm').serialize();
        $.ajax({
            url:'/checkout/simpanalamat',
            type:'POST',
            data:post,
            beforeSend:function(){
                showLoadingSwal()
            },success:function(result){
                if(result.is_success){
                    // window.location.href = window.history.back() - 1;
                    window.history.back();
                }
            }, error:function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status != 200){
                    Swal.close();
                    Swal.fire({
                        icon:'error',
                        text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
                        customClass:{
                            header: 'custom-swal'
                        },
                    })
                    console.log(errorThrown);
                }
            }
        })
    }
})