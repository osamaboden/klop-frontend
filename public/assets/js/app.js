// const { floor, flowRight } = require("lodash");

// mask
$('.angka').mask('00000000000');
var hargaProduk = "", expand_pop_toggle = "", expand_toggle="", cropper, infinate_page=1, infinate_status = true, filter_select = "all", owlVideo = "";

$(document).ready(function() {
	if($(window).width() < 576){

		if ($('#galleryGambar').length > 0) { //home
			$('#galleryGambar').owlCarousel({
				loop:false,
				center:false,
				margin:5,
				nav:true,
				navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
				dots:false,
				responsive:{
					0:{
						items:1
					},
					400:{
						items:1.5
					},
					519:{
						items:2
					},
					648:{
						items:2.5
					},
					826:{
						items:3
					},
					1500:{
						items:4
					}
				}
			})
		}

		if ($('#galleryVideo').length > 0) { //home
			$('#galleryVideo').addClass('owl-carousel owl-theme');
			owlVideo = $('#galleryVideo').owlCarousel({
				loop:false,
				margin:5,
				nav:true,
				navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
				dots:false,
				responsive:{
					0:{
						items:1
					},
					400:{
						items:1.5
					},
					519:{
						items:2
					},
					648:{
						items:2
					},
					826:{
						items:2.6
					},
					1500:{
						items:3
					}
				}
			})
			$('#galleryInstalasi').addClass('owl-carousel owl-theme');
			$('#galleryInstalasi').owlCarousel({
				loop:false,
				margin:5,
				nav:true,
				navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
				dots:false,
				responsive:{
					0:{
						items:1
					},
					400:{
						items:1.5
					},
					519:{
						items:2
					},
					648:{
						items:2
					},
					826:{
						items:2.6
					},
					1500:{
						items:3
					}
				}
			})
		}
	}else{
		if (typeof $('#galleryVideo').data('owl.carousel') != 'undefined') {
			$('#galleryVideo').data('owl.carousel').destroy();
		  }
		$('#galleryVideo').removeClass('owl-carousel owl-theme');
		if (typeof $('#galleryInstalasi').data('owl.carousel') != 'undefined') {
			$('#galleryInstalasi').data('owl.carousel').destroy();
		  }
	}


	if ($('#galleryTesti').length > 0) { //home
		$('#galleryTesti').owlCarousel({
		    loop:false,
		    margin:5,
		    nav:true,
		    navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
		    dots:false,
		    responsive:{
		        0:{
		            items:1
		        },
		        500:{
		            items:1.5
		        },
		        767:{
		            items:3
		        },
				1400:{
					items: 4
				}
		    }
		})
	}
	
	if($('.box-list-image').length > 0){ //detail 
		setSelectSatuanBeli()
		var width = $('.list-image').width();
		var item = $('.item-image').length * 82; 
		if(width > item){
			$('.list-image').css('min-width', (width)+'px');
		}else{
			$('.list-image').css('min-width', item+'px');
		}
		$(".box-list-image").animate({
			scrollLeft: $('.item-image.active').position().left
		}, 800);
		var easyzoom = $('.easyzoom').easyZoom ();
		var easyzoomapi      = easyzoom.data ('easyZoom');
		$('.item-image').click(function(){
			var url = $(this).find('img').attr('src');
			$('.item-image').removeClass('active');
			$(this).addClass('active');
			$('.display-image a').attr('href', url);
			$('.display-image .image').attr('src', url);
			easyzoomapi.swap(url,url)
		})

		$('.btn-plus').click(function(){
			var id = $(this).attr('data-id');
			var type =$(this).attr('data-type');
			var qty = $(this).attr('data-qty');
			var minimal = $(this).attr('data-minimal');
			setBtnPlus(id,type,qty, minimal);
			$('#text-error').text('');
		});
		var path = window.location.pathname.split('/');
		getHargaProduk(path[path.length - 1]);
		$('[data-toggle="qtyProduk"]').bind('keyup input',function(){
			$('#text-error').text('');
			var id = $(this).attr('data-id');
			var minimal = $(this).attr('data-minimal');
			var satuan_beli = $('[data-toggle="satuan_beli"][data-id="'+id+'"] option:selected').val();
			var pcs = $(this).val();
			if(satuan_beli == "pcs"){
				$('.qty_pcs[data-id="'+id+'"]').val(pcs);
			}
			setBtnPlus(id,'','', minimal, satuan_beli);
		});

		$('#BtnSaveKeranjang').click(function(){
			var satuan_beli = $('#satuan_beli').val();
			if(satuan_beli == "pcs"){
				var minimal = $('#qty-val').attr('data-minimal');
			}else{
				var minimal = $('#qty-val').attr('data-minimal-meter');
			}
			var val = $('#qty-val').val();
			if(parseInt(val) >= parseInt(minimal) && $('#ModalLogin').length == 0){
				$('#text-error').text('');
				var post = $('#SaveKeranjang').serialize();
				$.ajax({
					url:'/produk/savekeranjang',
					type:'POST',
					data:post+'&meta='+path[path.length - 1],
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){
						Swal.close();
						if(result.is_success){
							setTimeout(() => {
							// 	if(result.html_popup!=""){
							// 		Swal.fire({
							// 			title:'Berhasil dimasukkan ke dalam keranjang',
							// 			showCloseButton:true,
							// 			showConfirmButton:false,
							// 			showCancelButton:false,
							// 			html:result.html_popup
							// 			});
							// 			$('.swal2-header').attr('style', 'border-bottom: 1px solid;');
							// 			$('#swal2-title').attr('style', 'font-size: 17px; text-transform: uppercase;');
							// 			$('.swal2-popup.swal2-modal').attr('style', 'display: flex;width: 40rem;');
										
							// 	}else{
								// 	Swal.fire({
								// 		title: '<span style="color:#57D024">Barang Berhasil Ditambahkan</span>',
								// 		imageUrl: '/assets/img/keranjang_modal.svg',
								// 		showConfirmButton: false,
								// 		imageWidth:150,
								// 	});
								// }
								Swal.fire({
									icon: 'success',
									text: 'Berhasil dimasukkan ke dalam keranjang',
									showCancelButton:false,
									showConfirmButton:false,
									confirmButtonText:'Lihat Keranjang',
									customClass:{
										header: 'custom-swal',
										confirmButton: 'btn btn-success btn-custom',
									}
								}).then((result) => {
									if(result.value){
										window.location.href = '/keranjang';
									}
								});
								$('[data-id="Imgkeranjang"] span').remove();
								$('.list-keranjang-pop div').remove();
								$('#BoxKeranjang .pop-up-footer').remove();
								$('#BoxKeranjang').append(result.html_footer);
								$('.list-keranjang-pop').append(result.html);
								$('[data-id="Imgkeranjang"]').append('<span class="badge bg-danger badge-custom">'+result.jml+'</span>');
								$('[data-toggle="deletecart"]').click(function(){
									var session = $(this).attr('data-session');
									if(session){
										Swal.fire({
											icon:'warning',
											html:'<div>Apakah Anda yakin ingin menghapus <br> barang dari keranjang?</div>',
											// text: 'Apakah Anda yakin ingin menghapus <br> barang dari keranjang?',
											showCancelButton:true,
											cancelButtonText:'Batal',
											confirmButtonText:'Hapus',
											confirmButtonColor: '#EF4242',
											customClass:{
												cancelButton: 'cancel-swal',
												header:'custom-swal',
											}
										}).then((result) => {
											if(result.value){
												deletCart(session);
											}
										});
									}
								})
							}, 500);
						}else{
							setTimeout(() => {
								Swal.fire({
									icon:  'error',
									title: 'Barang Tidak Berhasil Ditambahkan',
									showConfirmButton: false,
									customClass:{
										header: 'custom-swal'
									}
								})
							}, 500);
						}
					}
				})
			}else{
				if(parseInt(val) < parseInt(minimal)){
					satuan_beli = satuan_beli.toLowerCase().replace(/\b[a-z]/g, function(letter) {
						return letter.toUpperCase();
					});
					$('#text-error').text('Minimal Pembelian '+minimal+' '+satuan_beli);
				}else if($('#ModalLogin').length > 0){
					setTimeout(() => {
						Swal.fire({
							icon:  'error',
							text:'Silahkan Login terlebih dahulu...',
							showConfirmButton: false,
							customClass:{
								header: 'custom-swal'
							}
						}).then((result)=>{
							if(result.dismiss){
								window.location.href = window.location.origin+'/login';
								// $('#ModalLogin').modal('show');
							}
						})
					}, 500);
				}
			}
		})
	}

	if($('#boxUlasan').length > 0){ //detail
		$('#ShowMoreUlasan').click(function(){
			ShowBoxUlasan()
		});
		// hideBoxUlasan();
	}

	if($('#boxDescProduk').length > 0){ //detail
		checkDescOver();
		$('#ShowDeskripsi').click(function(){
			var dataExpand = $(this).attr('data-expand');
			if(dataExpand == "false"){
				$('#ShowDeskripsi').attr('data-expand', 'true');
				// $('#ShowDeskripsi').text('Lihat Lebih Sedikit');
				// $('#ShowDeskripsi').prev().addClass('show');
				$('#ModalDesc').modal('show');
			}else{
				// $('#ShowDeskripsi').attr('data-expand', 'false');
				$('#ShowDeskripsi').prev().removeClass('show');
				// $('#ShowDeskripsi').text('Lihat Semua');
			}
		})
	}

	if($('#KeranjangBox').length > 0){ //keranjang
		setSelectSatuanBeli();
		$('.btn-plus').click(function(){
			var id = $(this).attr('data-id');
			var type =$(this).attr('data-type');
			var qty = $(this).attr('data-qty');
			var minimal = $(this).attr('data-minimal');
			setBtnPlus(id,type,qty, minimal);
			countTotalKeranjang();
			$('#text-error').text('');
		});
		var path = window.location.pathname.split('/');
		getHargaProduk(path[path.length - 1]);
		countTotalKeranjang();
		$('[data-toggle="qtyProduk"]').bind('keyup input',function(){
			$('#text-error').text('');
			var id = $(this).attr('data-id');
			var minimal = $(this).attr('data-minimal');
			var satuan_beli = $('[data-toggle="satuan_beli"][data-id="'+id+'"] option:selected').val();
			var pcs = $(this).val();
			if(satuan_beli == "pcs"){
				$('.qty_pcs[data-id="'+id+'"]').val(pcs);
			}
			setBtnPlus(id,'','', minimal, satuan_beli);
		});

		$('#BtnKeranjangForm').click(function(){
			if(checkValueKeranjang()){
				var post = $('#KeranjangForm').serialize();
				$.ajax({
					url:'/keranjang/update',
					type:'POST',
					data:post,
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){
						Swal.close();
						if(result.is_success){
							if(result.exist_address){
								window.location.href = window.location.origin+"/checkout";
							}else{
								window.location.href = window.location.origin+"/alamat";
							}
						}else{
							Swal.fire({
								icon:'error',
								text:'Terjadi kesalahan...',
								customClass:{
									header: 'custom-swal'
								}
							})
						}
					}
				})
			}
		})
	}

	if($('#CheckoutBox').length > 0){ //checkout
		$(".select2").select2({
			theme: "bootstrap-5",
			width: 'resolve'
		});
		$('.select2-container').css('width', '100%');

		$('#date').daterangepicker({
			"singleDatePicker": true,
			"autoApply": true,
			"locale": {
				"format": "DD MMMM YYYY",
				"separator": " - ",
				"applyLabel": "Apply",
				"cancelLabel": "Cancel",
				"fromLabel": "From",
				"toLabel": "To",
				"customRangeLabel": "Custom",
				"weekLabel": "W",
				"daysOfWeek": [
					"Mg",
					"Sn",
					"Sl",
					"Rb",
					"Km",
					"Jm",
					"Sa"
				],
				"monthNames": [
					"Januari",
					"Februari",
					"Maret",
					"April",
					"Mai",
					"Juni",
					"Juli",
					"Agustus",
					"September",
					"Oktober",
					"November",
					"Desember"
				],
				"firstDay": 1
			},
			"startDate": moment().add(1, 'days'),
			"endDate": moment().add(1, 'days'),
			"minDate": moment().add(1, 'days')
		}, function(start, end, label) {
		//    date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
		});
		counttotalCheckout();

		$('#instalasi').change(function(){
			var val = $(this).val();
			if(val == "ya"){
				$.ajax({
					url:'/checkout/pemasangan',
					type:'GET',
					beforeSend:function(){
						showLoadingSwal();
					},
					success:function(result){
						if(result.is_success){
							Swal.close();
							$('[data-total="pemasangan"]').removeClass('d-none');
							$('[data-total="pemasangan"] .float-end').text(''+FormatMoney(result.data.harga))
							$('#total_pemasangan').val(result.data.harga);
						}
						counttotalCheckout();
					}
				})
			}else{
				$('[data-total="pemasangan"]').addClass('d-none');
				$('[data-total="pemasangan"] .float-end').text(''+FormatMoney(0))
				$('#total_pemasangan').val(0);
				counttotalCheckout();
			}
		})

		$('#delivery').change(function(){
			var val = $(this).val();
			if(val == "ya"){
				$('[data-box="tanggal"] .form-label').text('Tanggal Pengiriman')
				$('[data-box="alamat"] .form-label').text('Alamat Pengiriman')
				$('[data-id="alamatUser"]').removeClass('d-none');
				$('[data-id="alamatToko"]').addClass('d-none');
				var id_alamat = $('#alamat').val();
				if($('[data-id="alamatUser"]').find('.label-alamat').text() == "" && $('[data-id="alamatUser"]').find('.alamat').text() == ""){
					id_alamat = 0;
				}
				changeAlamat(id_alamat);
			}else{
				$('[data-box="tanggal"] .form-label').text('Tanggal Pengambilan')
				$('[data-box="alamat"] .form-label').text('Alamat Pengambilan')
				$('[data-id="alamatUser"]').addClass('d-none');
				$('[data-id="alamatToko"]').removeClass('d-none');
				$('[data-total="pengiriman"]').addClass('d-none')
				$('#total_pengiriman').val(0)
				counttotalCheckout();
			}
		})

		$('[data-toggle="pilih_alamat"]').click(function(){
			var id = $(this).attr('data-id').replace('alamat-', '');
			$('#ModalAlamat').modal('hide');
			changeAlamat(id);
			$('[data-toggle="pilih_alamat"]').removeClass('d-none');
			$('[data-toggle="pilih_alamat"][data-id="alamat-'+id+'"]').addClass('d-none');
		})
	}

	if($('#ListTransaksiBox').length > 0){ //transaksi
		var queryParams = new URLSearchParams(window.location.search);
		if(queryParams.get('q')){
			$('.search-transaksi').val(queryParams.get('q'))
		}
		if(queryParams.get('status')){
			$('#statusTransaksi').val(queryParams.get('status')).trigger('change');
		}
		$("#statusTransaksi").select2({
			theme: "bootstrap-5",
			width: 'resolve' 
		});

		$('.search-transaksi').keypress(function(event){
			var keycode = (event.keyCode ? event.keyCode : event.which);
			if(keycode == '13'){
				queryParams.delete('page');
				queryParams.delete('status');
				if($(this).val() == ""){
				queryParams.delete('q');
				}else{
				queryParams.set('q', $(this).val());
				}
				window.location.href = window.location.origin+"/transaksi?"+queryParams.toString();
			}
		})

		$('#statusTransaksi').change(function(){
				queryParams.delete('page');
				queryParams.delete('q');
				if($(this).val() == "" || $(this).val() == "all"){
				queryParams.delete('status');
				}else{
				queryParams.set('status', $(this).val());
				}
				window.location.href = window.location.origin+"/transaksi?"+queryParams.toString();
		})

		$('.paging[data-page]').click(function(){
			queryParams.set('page', $(this).attr('data-page'));
			window.location.href = window.location.origin+"/transaksi?"+queryParams.toString();
		})
	}

	if($('#profilePage').length > 0){ //profile
		var $ModalCrop = $('#ModalImage');
		var imageCeop = document.getElementById('imageCrop');
		var checkActiveTab = window.location.href.split('#');
		if(window.location.href.indexOf('#') > 0){
			var activeTab = checkActiveTab[checkActiveTab.length - 1];
			if(activeTab){
				$('#'+activeTab).tab('show')
			}
		}
		$('#resetPasswordForm').validate({
			rules:{
				old_password:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
					minlength: 8,
					remote:{
						url: window.location.origin+'/validatepassword',
						type:'get',
						data:{
							old_password: function(){
								return $("#old_password").val();
							}
						}
					}
				},
				new_password:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
					minlength: 8
				},
				repeatnew_password:{
					equalTo: "#new_password"
				},
			},
			errorClass:'is-invalid',
			validClass:'is-valid',
			messages:{
				old_password: {
					required: "Password Harus Diisi",
					minlength: jQuery.validator.format("Password minimal {0} Karakter"),
					remote: 'Password Salah'
				},
				new_password: {
					required: "Password Harus Diisi",
					minlength: jQuery.validator.format("Password minimal {0} Karakter"),
				},
				repeatnew_password:{
					required: "Password tidak sama, silakan ulangi kembali",
					equalTo: "Password tidak sama, silakan ulangi kembali"
				},
			},
		});

		$('#BtnResetPassword').click(function(){
			if($('#resetPasswordForm').valid){
				var post = $('#resetPasswordForm').serialize();
				$.ajax({
					url:window.location.origin+'/password/reset',
					type:'POST',
					data:post,
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){;
						Swal.close();
						if(result.is_success){
							$('[type="password"]').val('');
							Swal.fire({
								icon:'success',
								text: 'Password Berhasil Diperbarui',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							})
						}else{
							Swal.fire({
								icon:'error',
								text: 'Password Tidak Berhasil Berhasil Diperbarui',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							})
						}
					}
				})
			}
		})

		$('#FormBiodata').validate({
			rules:{
				biodata_email:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
					minlength: 8,
					remote:{
						url: window.location.origin+'/validateemail',
						type:'get',
						data:{
							biodata_email: function(){
								return $("#biodata_email").val();
							}
						}
					}
				},
				biodata_nama:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
			},
			errorClass:'is-invalid',
			validClass:'is-valid',
			messages:{
				biodata_email: {
					required: "Email Harus Diisi",
					remote: 'Email Telah tersedia'
				},
				biodata_nama: {
					required: "Nama Harus Diisi",
				},
			},
		});

		$('#btnSaveBiodata').click(function(){
			if($('#FormBiodata').valid){
				var post = $('#FormBiodata').serialize();
				$('#modalBiodata').modal('hide');
				$.ajax({
					url:window.location.origin+'/update/biodata',
					type:'POST',
					data:post,
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){;
						Swal.close();
						if(result.is_success){
							$('[data-label="nama"]').text(result.nama);
							$('[data-label="email"]').text(result.email);
							Swal.fire({
								icon:'success',
								text: 'Biodata Berhasil Diperbarui',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							})
						}else{
							Swal.fire({
								icon:'error',
								text: 'Biodata Tidak Berhasil Berhasil Diperbarui',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							})
						}
					}
				})
			}
		})

		$("#gambar_file").on("change", function(e){
			var files = e.target.files;
			var done = function (url) {
			imageCeop.src = url;
			  $ModalCrop.modal('show');
			};
			var reader;
			var file;
			var url;
			if (files && files.length > 0 && files[0].size <= 1000000) {
			  file = files[0];
		
			  if (URL) {
				  
				done(URL.createObjectURL(file));
			  } else if (FileReader) {
				reader = new FileReader();
				reader.onload = function (e) {
					
				  done(reader.result);
				};
				reader.readAsDataURL(file);
			  }
			}
		});

		$ModalCrop.on('shown.bs.modal', function () {
			cropper = new Cropper(imageCeop, {
			aspectRatio: 1,
			viewMode: 3,
			});
			setTimeout(() => {
				cropper.setDragMode('move')				
			}, 200);
		}).on('hidden.bs.modal', function () {
		   cropper.destroy();
		   cropper = null;
		   $("#gambar_file").val(null);
		});
		$('#SaveCropImage').click(function(){
			var widthImage = cropper.getData();
			canvas = cropper.getCroppedCanvas({
				width: widthImage.width,
				height: widthImage.height,
			  });
		  
			  canvas.toBlob(function(blob) {
				url = URL.createObjectURL(blob);
				var reader = new FileReader();
				 reader.readAsDataURL(blob); 
				 reader.onloadend = function() {
					var base64data = reader.result;  
					$.ajax({
						type: "POST",
						dataType: "json",
						url: window.location.origin+'/changeprofil',
						data: {image: base64data, _token: $('[name="_token"]').val()},
						beforeSend:function(){
							$ModalCrop.modal('hide');
							showLoadingSwal();
						},
						success: function(result){;
							Swal.close();
							if(result.is_success){
								$('.image-profile img').attr('src', result.foto);
								Swal.fire({
									icon:'success',
									text: 'Foto Profil Berhasil Diperbarui',
									customClass:{
										header: 'custom-swal'
									},
									showConfirmButton:false
								})
							}else{
								Swal.fire({
									icon:'error',
									text: 'Foto Profil Tidak Berhasil Diperbarui',
									customClass:{
										header: 'custom-swal'
									},
									showConfirmButton:false
								})
							}
						}
					  });
				 }
			});
		})
		$('.nav-link').click(function(){
			window.history.pushState('','','#'+$(this).attr('id'));
		})
	}

	if($('#KonfirmasiBox').length > 0){ //konfirmasi
		$('.money').mask('000.000.000.000.000', {reverse: true});
		$('#KonfirmasiForm').validate({
			rules:{
				nomorOrder:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					}
				},
				nominalTransfer:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
				NamaTransfer:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
				bankTransfer:{
					required: {
						depends:function(){
							$(this).val($.trim($(this).val()));
							return true;
						}
					},
				},
				fileimage:{
					required: true,
				},
			},
			messages:{
				nomorOrder: {
					required: "ID Transaksi harus diisi",
				},
				nominalTransfer: {
					required: "Password Harus Diisi",
				},
				NamaTransfer: {
					required: "Atas Nama Harus Diisi",
				},
				bankTransfer: {
					required: "Nama Bank Harus Diisi",
				},
				fileimage: {
					required: "Bukti Transfer Harus Diisi",
					accept: "image/jpeg, image/png"
				}
			},
		});
		$('#fileimage').on('change', function(e){
			var file = e.target.files[0];
			var fileExtension = ['jpeg', 'jpg', 'png'];
			if(file.size < 2000000){
				$('.file-label').removeClass('text-danger');
				$('.file-label').text(file.name);
			}else{
				$('.file-label').addClass('text-danger');
				$('.file-label').text('Max. ukuran file 2mb, format file .jpg atau .png');
				$(this).val(null);
			}
		})

		$('#nomorOrder').change(function(){
			var id_order = $(this).val();
			if(id_order){
				$.ajax({
					url:'/konfirmasi/check',
					type:'GET',
					data:'id_order='+id_order,
					success:function(result){
						if(result.is_success){
							$('#BtnSaveKonfirmasi').attr('disabled', false);
							$('#nominalTransfer').val(FormatMoney(result.total));
						}else{
							$('#BtnSaveKonfirmasi').attr('disabled', true);
							$('#nomorOrder').after('<label id="nomorOrder-error" class="error" for="nomorOrder">ID Transaksi tidak tersedia</label>');
						}
					}
				})
			}
		})

		$('#KonfirmasiForm').submit(function(e){
			e.preventDefault();
			if($('#KonfirmasiForm').valid()){
				let formData = new FormData(this);
				$.ajax({
					url:'/postkonfirmasi',
					type:'POST',
					data: formData,
					contentType: false,
					processData: false,
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){;
						Swal.close();
						if(result.is_success){
							Swal.fire({
								icon:'success',
								text: 'Konfirmasi pembayaran telah berhasil kami terima, akan segera kami proses..',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							}).then((res) => {
								window.location.href = window.location.origin+'/transaksi/detail/'+result.id_order;
							})
						}else{
							Swal.fire({
								icon:'error',
								text: 'Terjadi kesalahan, silahkan ulangi lagi dalam beberapa saat',
								customClass:{
									header: 'custom-swal'
								},
								showConfirmButton:false
							})
						}
					}
				})
			}
		})

		// $('#BtnSaveKonfirmasi').click(function(){
		// 	if($('#KonfirmasiForm').valid()){
		// 		let formData = new FormData($('#KonfirmasiForm'));
		// 		
		// 		// $.ajax({
		// 		// 	url:'/postkonfirmasi',

		// 		// })
		// 	}
		// });
	}

	if($('#DetailTransaksi').length > 0){ //detailtransaksi
		const pathname = window.location.pathname.split('/');
		$('[data-toggle="batalkanPesanan"]').click(function(){
			var id = pathname[pathname.length - 1];
			if(id){
				Swal.fire({
					icon:'warning',
					text:'Apakah Anda yakin ingin membatalkan transaksi ini?',
					showCancelButton:true,
					cancelButtonText:'Tidak',
					confirmButtonText:'Batalkan Transaksi',
					confirmButtonColor: '#EF4242',
					customClass:{
						cancelButton: 'cancel-swal',
						header:'custom-swal'
					}
				}).then((result) => {
					if(result.value){
						$.ajax({
							url:window.location.origin+'/transaksi/cancel',
							type:'GET',
							data:'id='+id,
							beforeSend:function(){
								showLoadingSwal();
							},success:function(result){
								Swal.close();
								if(result.is_success){
									$('[data-toggle="batalkanPesanan"]').remove();
									$('[data-box="konfirmasi"]').remove();
									$('#statusBox').text('');
									$('#statusBox').append(result.boxStatus);
								}else{

								}
							}
						})
					}
				});
			}
		});

		$('#stars li').on('mouseover', function(){
			var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
		   
			// Now highlight all the stars that's not after the current hovered star
			$(this).parent().children('li.list-star').each(function(e){
				$(this).removeClass('selected');
			  if (e < onStar) {
				$(this).addClass('hover');
			  }
			  else {
				$(this).removeClass('hover');
			  }
			});

			var ratingValue = parseInt($('#stars li.hover').last().data('value'), 10);
			var msg = "";
			if(ratingValue == 1){
				msg = "Buruk Sekali";
			}else if(ratingValue == 2){
				msg = "Buruk";
			}else if(ratingValue == 3){
				msg = "Cukup";
			}else if(ratingValue == 4){
				msg = "Baik";
			}else if(ratingValue == 5){
				msg = "Baik Sekali";
			}
			$('.star-message').text(msg);
			
		  }).on('mouseout', function(){
			$(this).parent().children('li.list-star').each(function(e){
				if($(this).attr('data-selected')){
					$(this).click();
				}
			  $(this).removeClass('hover');
			});
		  });
		  
		  
		  /* 2. Action to perform on click */
		  $('#stars li').on('click', function(){
			var onStar = parseInt($(this).data('value'), 10); // The star currently selected
			var stars = $(this).parent().children('li.list-star');
			$('#stars li').removeAttr('data-selected', true);
			for (i = 0; i < stars.length; i++) {
			  $(stars[i]).removeClass('selected');
			}
			
			for (i = 0; i < onStar; i++) {
			  $(stars[i]).addClass('selected');
			}

			$('#stars li.selected').last().attr('data-selected', true);
			
			// JUST RESPONSE (Not needed)
			var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
			var msg = "";
			if(ratingValue == 1){
				msg = "Buruk Sekali";
			}else if(ratingValue == 2){
				msg = "Buruk";
			}else if(ratingValue == 3){
				msg = "Cukup";
			}else if(ratingValue == 4){
				msg = "Baik";
			}else if(ratingValue == 5){
				msg = "Baik Sekali";
			}
			$('#startvalue').val(ratingValue);
			$('#startvalue').keyup();
			$('.star-message').text(msg);
			
		  });
		
		  	$('#FormReview').validate({
				rules:{
					startvalue:{
						required: {
							depends:function(){
								$(this).val($.trim($(this).val()));
								return true;
							}
						},
					},
					reviewValue:{
						required: {
							depends:function(){
								$(this).val($.trim($(this).val()));
								return true;
							}
						},
					},
				},
				messages:{
					startvalue: {
						required: "Rating harus diisi",
					},
					reviewValue: {
						required: "Review harus diisi",
					}
				},
			});

			$('#FormReview').on('submit', function(e){
				e.preventDefault();
				let formData = new FormData(this);
				if($('#FormReview').valid()){
					var id = pathname[pathname.length - 1];
					formData.append('id_order', id);
					$.ajax({
						url:window.location.origin+'/setreview',
						type:'POST',
						data: formData,
						processData: false,
						contentType: false,
						cache: false,
						beforeSend:function(){
							$('#ModalReview').modal('hide');
							showLoadingSwal();
						},success:function(result){
							window.location.reload();
						}
					})
				}
			})
	}
	if($('#ReviewBox').length > 0){ //Review
		$('.filter li').click(function(){
			if(!$(this).hasClass('active')){
				$('.list-ulasan .card').remove();
				infinate_status = true;
				filter_select = $(this).attr('data-select');
				infinteLoadMore(0, 'ulasan')
				infinate_page = 1;
				$('.filter li').removeClass('active');
				$(this).addClass('active');
			} 
		})
	}
	if($('#BoxVideo').length > 0){ //video
		$('.lazy').Lazy({
			scrollDirection: 'vertical',
			delay: 500,
			afterLoad: function(element) {
				$(element).prev().find('a').attr('data-fancybox', 'gallery');
				$(element).prev().find('a').attr('href', $(element).prev().find('a').attr('data-href'));
				$(element).prev().find('a').removeAttr('data-href')
				$(element).removeClass('d-none lazy')
				$(element).parent().removeClass('lazy_loading')
			}
		});
	}
	if($('#BoxGambar').length > 0){ //gambar
			$('.lazy').Lazy({
				scrollDirection: 'vertical',
				delay: 1000
			});
			$('.listGambar').GridHorizontal({
				item: '.gallery-item',
				minWidth: 300,
				maxRowHeight: 250,
				gutter: 20,
				callback:{
					onSuccess: function(){
						setTimeout(() => {
							$.each($('.hiddenfile'),function(key,element){
									$(element).parent().attr('data-fancybox', 'gallery');
									$(element).parent().attr('href', $(element).parent().attr('data-href'));
									$(element).parent().removeAttr('data-href')
									$(element).parent().parent().removeClass('lazy_loading');
									$(element).removeClass('hiddenfile lazy')
							});
						}, 1000);
					}
				}
			});
			// alert(document.body.offsetHeight);
	}
})

$(window).on('resize', function(){
	if($('.box-list-image').length > 0){
		var width = $('.list-image').width();
		var item = $('.item-image').length * 60; 
		// console.log(item+" "+width);
		// if(width > item){
		// 	$('.list-image').css('min-width', (width+20)+'px');
		// }else{
			$('.list-image').css('min-width', item+'px');
		// }
		checkDescOver();
	}

	if($(window).width() < 576){
		if ($('#galleryVideo').length > 0) { //home
			$('#galleryVideo').addClass('owl-carousel owl-theme');
			owlVideo = $('#galleryVideo').owlCarousel({
				loop:false,
				margin:5,
				nav:true,
				navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
				dots:false,
				responsive:{
					0:{
						items:1
					},
					400:{
						items:1.5
					},
					519:{
						items:2
					},
					648:{
						items:2
					},
					826:{
						items:2.6
					},
					1500:{
						items:3
					}
				}
			})
			$('#galleryInstalasi').addClass('owl-carousel owl-theme');
			$('#galleryInstalasi').owlCarousel({
				loop:false,
				margin:5,
				nav:true,
				navText: ['<i class="fas fa-chevron-left"><i>', '<i class="fas fa-chevron-right"><i>'],
				dots:false,
				responsive:{
					0:{
						items:1
					},
					400:{
						items:1.5
					},
					519:{
						items:2
					},
					648:{
						items:2
					},
					826:{
						items:2.6
					},
					1500:{
						items:3
					}
				}
			})
		}
	}else{
		if (typeof $('#galleryVideo').data('owl.carousel') != 'undefined') {
			$('#galleryVideo').data('owl.carousel').destroy();
		  }
		$('#galleryVideo').removeClass('owl-carousel owl-theme');

		if (typeof $('#galleryInstalasi').data('owl.carousel') != 'undefined') {
			$('#galleryInstalasi').data('owl.carousel').destroy();
		  }
	}

	if($('#BoxGambar').length > 0){
		$('.listGambar').GridHorizontal({
			item: '.gallery-item',
			minWidth: 400,
			maxRowHeight: 350,
			gutter: 20,
		})
	}
});

const infinteLoadMore = (page, active) => {
	if(active == "ulasan" && infinate_status){
		$.ajax({
			url:window.location.href,
			type:'GET',
			data:'page='+page+'&star='+filter_select,
			beforeSend:function(){
				$('.load-infinate').show();
			},success:function(result){
				if(result.html != "" && infinate_status){
					$('.list-ulasan').append(result.html);
					$('.load-infinate').hide();
				}else{
					infinate_status = false;
					$('.load-infinate').hide();
				}
			}
		})
	}else if(active == "video" && infinate_status){
		$.ajax({
			url:window.location.href,
			type:'GET',
			data:'page='+page,
			beforeSend:function(){
				$('.load-infinate').show();
			},success:function(result){
				if(result.html != "" && infinate_status){
					$('.Listvideo').append(result.html);
					$('.lazy').Lazy({
						scrollDirection: 'vertical',
						delay: 500,
						afterLoad: function(element) {
							$(element).prev().find('a').attr('data-fancybox', 'gallery');
							$(element).prev().find('a').attr('href', $(element).prev().find('a').attr('data-href'));
							$(element).prev().find('a').removeAttr('data-href')
							$(element).removeClass('d-none lazy')
							$(element).parent().removeClass('lazy_loading')
						}
					});
					$('.load-infinate').hide();
				}else{
					infinate_status = false;
					$('.load-infinate').hide();
				}
			}
		})
	}else if(active == "gambar" && infinate_status){
		$.ajax({
			url:window.location.href,
			type:'GET',
			data:'page='+page,
			beforeSend:function(){
				$('.load-infinate').show();
			},success:function(result){
				if(result.html != "" && infinate_status){
					$('.gallerylist').append(result.html);
					$('.lazy').Lazy({
						scrollDirection: 'vertical',
						delay: 1000
					});
					setTimeout(() => {
					$('.listGambar').GridHorizontal({
						item: '.gallery-item',
						minWidth: 300,
						maxRowHeight: 250,
						gutter: 20,
						callback:{
							onSuccess: function(){
								setTimeout(() => {
									$.each($('.hiddenfile'),function(key,element){
										$(element).parent().attr('data-fancybox', 'gallery');
										$(element).parent().attr('href', $(element).parent().attr('data-href'));
										$(element).parent().removeAttr('data-href')
										$(element).parent().parent().removeClass('lazy_loading');
										$(element).removeClass('hiddenfile lazy')
								});
								}, 1000);
							}
						}
					});
					}, 500);
					$('.load-infinate').hide();
				}else{
					$('.load-infinate').hide();
				}
				infinate_status = (result.count == $('.gallery-item').length) ? false : true;
			}
		})
	}
} 
const fixedTo = (position, bottom) => {
	var vard = position;
	if(position != bottom){
		if(position > bottom){
			vard = Math.floor(position);
		}else if(position < bottom){
			vard = Math.ceil(position);
		}
	}
	
	return vard;
}
$(window).scroll(function () {
	$this = $(this);
	var position = (window.innerHeight + window.scrollY);
	if($('main').attr('id')){
		var bottom = document.getElementById($('main').attr('id')).offsetHeight;
	}else{
		var bottom = document.body.offsetHeight;
	}
	position = fixedTo(position, bottom);
	if($('#ReviewBox').length > 0){
		if((position == bottom || position > bottom) && infinate_status)  {
			infinate_page++;
			infinteLoadMore(infinate_page, 'ulasan');
		}
	}
	if($('#BoxVideo').length > 0){
		if((position == bottom || position > bottom) && infinate_status) {
			infinate_page++;
			infinteLoadMore(infinate_page, 'video');
		}
	}
	if($('#BoxGambar').length > 0){
		if((position == bottom || position > bottom) && infinate_status){
			infinate_page++;
			infinteLoadMore(infinate_page, 'gambar');
		}
	}
})

const getHargaProduk = (id) => {
	$.ajax({
		url:'/produk/harga',
		type:'GET',
		data:'meta='+id,
		beforeSend:function(){
			showLoadingSwal();
		},success:function(result){
			if(result.is_success){
				hargaProduk = result.list
				Swal.close();
				$('.qty_produk .btn-custom').attr('disabled', true);
			}else{
				$('.qty_produk .btn-custom').attr('disabled', true);
			}
		}
	})
}

 function setSelectSatuanBeli(){
	$.each($('[data-toggle="satuan_beli"]'), function(key, value){
		var id = "select-custom-"+generateString(5);
		if($(value).find('option:selected').text()){
			var selectText = $(value).find('option:selected').text();
			var selectVal = $(value).find('option:selected').val();
		}else{
			var selectText = $(value).find('option:first').text()
			var selectVal = $(value).find('option:first').val();
		}
		$(value).css('display', 'none');
		var html  = '<div class="form-select form-beli select-custom" data-expand="false" id="'+id+'">';
			html += '<div class="value">'+selectText+'</div>';
			html += '<div class="list" data-select-custom="'+id+'"><ul>'
			$.each($(value).find('option'), function(k, v){
				var val = $(v).val();
				var tex = $(v).text();
				if(selectVal == val){
					html += '<li data-select-id="'+val+'" data-select-custom="'+id+'" class="selected">'+tex+'</li>'
				}else{
					html += '<li data-select-id="'+val+'" data-select-custom="'+id+'">'+tex+'</li>'
				}
			})
			html += '</ul></div>';
			html += '</div>';
			$(value).attr('data-target', id);
			$(value).after(html);
			setConfSelectForm()
	})
 }

const setConfSelectForm = () => {
	$('.select-custom > .value').click(function(){
		var id = $(this).parent().attr('id');
		var expand =	$('.list[data-select-custom="'+id+'"]').hasClass('show');
		if($('[data-target="'+id+'"]').find('option:selected').text()){
			var selectVal = $('[data-target="'+id+'"]').find('option:selected').val();
		}else{
			var selectVal = $('[data-target="'+id+'"]').find('option:first').val();
		}
		console.log(selectVal);
		$('[data-select-id]').removeClass('selected');
		if(!expand){
			$('.list[data-select-custom="'+id+'"]').addClass('show');
			$('[data-select-custom="'+id+'"][data-select-id="'+selectVal+'"]').addClass('selected');
		}else{
			$('.list[data-select-custom="'+id+'"]').removeClass('show');
		}
	})

	$('[data-select-id]').click(function(){
		var id = $(this).attr('data-select-custom');
		var val = $(this).attr('data-select-id');
		var text = $(this).text();
		$('[data-target="'+id+'"]').val(val).trigger('change');
		$('.form-select.form-beli.select-custom > .value').text(text);
	})

	$(document).mouseup(function(e) 
	{
		$.each($(".select-custom > .value"), function(key, value){
			var container = $(value);
			if (!container.is(e.target) && container.has(e.target).length === 0) 
			{
				var id = $(this).parent().attr('id');
				$('.list[data-select-custom="'+id+'"]').removeClass('show');
			}
		})
	});
}


function generateString(length) {
	const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

$('[data-toggle="satuan_beli"]').change(function(){
	$('#text-error').text('');
	var panjang = $(this).attr('data-panjang');
	var id = $(this).attr('data-id');
	var minimal = $('[data-toggle="qtyProduk"][data-id="'+id+'"]').attr('data-minimal');
	var satuan_beli = $('[data-toggle="satuan_beli"][data-id="'+id+'"] option:selected').val();
	var pcs = $('.qty_pcs[data-id="'+id+'"]').val();
	if(satuan_beli == "pcs"){
		var jml = pcs;
		$('[data-toggle="qtyProduk"][data-id="'+id+'"]').val(pcs);
	}else{
		var jml = parseFloat(panjang) * pcs;
		$('[data-toggle="qtyProduk"][data-id="'+id+'"]').val(jml);
	}
	setBtnPlus(id,'',jml, minimal, satuan_beli);
})

const setBtnPlus = (id, type = "", qty, minimal, satuan_beli = "pcs") => {
	var jml = 1;
	var qty_produk = parseInt($('[data-toggle="qtyProduk"][data-id="'+id+'"]').val());
	var panjang = parseFloat($('[data-toggle="qtyProduk"][data-id="'+id+'"]').attr('data-panjang'));
	if(type == 'plus'){
		jml = qty_produk + parseInt(qty);
	}else if("min" == type){
		jml = qty_produk - parseInt(qty);
	}else{
		jml = (isNaN(qty_produk)) ? '0' : qty_produk;
	}
	$('[data-toggle="qtyProduk"][data-id="'+id+'"]').val(jml)
	if(satuan_beli == "meter"){
		var qty_pcs = Math.ceil(jml/panjang);
		$('.qty_pcs[data-id="'+id+'"]').val(qty_pcs);
		jml = qty_pcs;
	}
	if(jml > minimal){
		$('.btn-plus[data-type="min"][data-id="'+id+'"]').attr('disabled', false);
	}else{
		// jml = minimal;
		$('.btn-plus[data-type="min"][data-id="'+id+'"]').attr('disabled', true);
	}
	if(jml >= minimal){
		$('[data-id="error-'+id+'"]').text('');
	}else{
		if(satuan_beli == "meter"){
			$('[data-id="error-'+id+'"]').text('Minimal Pembelian '+(minimal * panjang)+' Meter');
		}else{
			$('[data-id="error-'+id+'"]').text('Minimal Pembelian '+minimal+' Pcs');
		}
	}
	setHargaProduk(id, jml);
}

const selectQty = (id,qty) => {
	var selectQty = "";
	for (let i = 0; i < hargaProduk[id].length; i++) {
		if(selectQty == "" && hargaProduk[id][i].qty <= qty){
			selectQty = i;
		}
	}
	if(selectQty == ""){
		selectQty = 0;
	}
	return selectQty;
}

const setHargaProduk = (id, jml) => {
	var qty = parseInt(jml);
	var harga = 0;
	var SelectQty = selectQty(id,qty);
	while (qty > 0) {
		var kurang = qty - hargaProduk[id][SelectQty].qty;
		if(kurang <= 0){
			harga += hargaProduk[id][SelectQty].harga * qty;
			qty = 0;
		}else if(kurang > 0){
			harga += hargaProduk[id][SelectQty].harga * hargaProduk[id][SelectQty].qty;
			qty = kurang;
		}
	}
	$('[data-total][data-id="'+id+'"]').not('.subtotal').val(''+FormatMoney(harga));
	$('.subtotal[data-total][data-id="'+id+'"]').text(''+FormatMoney(harga));
	if($('#KeranjangBox').length > 0){
		countTotalKeranjang();
	}
	return harga;
}

const FormatMoney = (x) => {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  }

  const hideBoxUlasan = () => {
	  var i = 0;
	$.each($('#boxUlasan .card.card-body'), function(key, value){
		if(i > 2){
			$(value).addClass('d-none');
		}	
		i++;
	});
	if(i > 3){
		$('#ShowMoreUlasan').removeClass('d-none');
		$('#ShowMoreUlasan').attr('data-expand', 'true');
	}else{
		$('#ShowMoreUlasan').addClass('d-none');
		$('#ShowMoreUlasan').attr('data-expand', 'false');
	}
  }

  const ShowBoxUlasan = () => {
	  var dataExpand = $('#ShowMoreUlasan').attr('data-expand');
	  if(dataExpand == "true"){
		$('#boxUlasan .card.card-body').removeClass('d-none');
		$('#ShowMoreUlasan').attr('data-expand', 'false');
		$('#ShowMoreUlasan').text('Lihat Lebih Sedikit');
	  }else{
		$('#ShowMoreUlasan').text('Lihat Semua');
		hideBoxUlasan() 
	  }
  }

  const checkDescOver = () => {
	if ($("#boxDescProduk .desc-produk").prop('scrollHeight') > $("#boxDescProduk .desc-produk").height() ) {
		$('#ShowDeskripsi').attr('data-expand', 'false');
		$('#ShowDeskripsi').removeClass('d-none');
	}else{
		$('#ShowDeskripsi').addClass('d-none');
		$('#ShowDeskripsi').attr('data-expand', 'true');
	}
  }

$('[data-toggle="pop-up-custom"]').click(function(){
	var target = $(this).attr('data-target');
	if(expand_toggle != target){ expand_pop_toggle = false; }
	$('.pop-up').not(target).attr('data-expand', 'false');
	$('.pop-up').not(target).removeClass('show');
	var expand = $(target).attr('data-expand');
	if( ("false" === expand || undefined === $(target).attr('data-expand')) && expand_pop_toggle != true){
		$('.navbar-toggler').attr('aria-expanded', false);
		$('.navbar-collapse.collapse').removeClass('show');		
		expand_pop_toggle = true;
		$(target).attr('data-expand', 'true');
		$(target).addClass('show');
		expand_toggle = target;
	}else{
		expand_pop_toggle = false;
		$(target).attr('data-expand', 'false');
		$(target).removeClass('show');
	}
})
$('[data-bs-toggle="collapse"]').click(function(){
	$('.pop-up').attr('data-expand', 'false');
	$('.pop-up').removeClass('show');
	expand_pop_toggle = false;
})

$('.pop-up .btn-close').click(function(){
	expand_pop_toggle = false;
	$(this).parent().parent().attr('data-expand', 'false');
	$(this).parent().parent().removeClass('show');
})

$('[data-toggle="deletecart"]').click(function(){
	var session = $(this).attr('data-session');
	if(session){
		Swal.fire({
			icon:'warning',
			html:'<div>Apakah Anda yakin ingin menghapus <br> barang dari keranjang?</div>',
			// text: 'Apakah Anda yakin ingin menghapus <br> barang dari keranjang?',
			showCancelButton:true,
			cancelButtonText:'Batal',
			confirmButtonText:'Hapus',
            confirmButtonColor: '#EF4242',
			customClass:{
				cancelButton: 'cancel-swal',
				header:'custom-swal',
			}
		}).then((result) => {
			if(result.value){
				deletCart(session);
			}
		});
	}
})

const deletCart = (session) => {
	$.ajax({
		url:'/produk/keranjangdelete',
		type:'GET',
		data:'data_session='+session,
		beforeSend:function(){
			showLoadingSwal();
		},success:function(result){
			Swal.close();
			if(result.is_success){
				$('#'+session).remove();
				$('[data-id="Imgkeranjang"] span').remove();
				if($('.list-keranjang-pop .keranjang').length <= 0){
					$('#BoxKeranjang .pop-up-footer').remove();
					$('.list-keranjang-pop').append(result.html);
					if($('#KeranjangBox').length > 0){
						$('#KeranjangBox #listKeranjang .card-body > *').remove();
						$('#KeranjangBox #listKeranjang').parent().attr('class', 'col-lg-5 col-md-6 col-sm-8 col-12 mx-auto ps-0 pe-0');
						$('#KeranjangBox #listKeranjang .card-body').append(result.html_keranjang);
						$('#KeranjangBox #listKeranjang .card-body').addClass('flex-center');
					}
				}else{
					$('[data-id="Imgkeranjang"]').append('<span class="badge bg-success badge-custom">'+result.jml+'</span>');
					if($('#KeranjangBox').length > 0){
						$('[data-delsession="'+session+'"]').remove();
						countTotalKeranjang();
					}
				}
			}else{

			}
		}
	})
}

const countTotalKeranjang = () =>{
	var total = 0;
	$.each($('[name="subtotal[]"]'), function(key,value){
		var subtotal = parseInt($(value).val().replace('', '').replace(/\./g,''));
			total += subtotal;
	})
	if(total){
		$('#TotalKeranjang').text(''+FormatMoney(total));
	}else{
		$('#TotalKeranjang').text('Rp0');
	}
}

const checkValueKeranjang = () => {
	var result = true;
	$.each($('[data-toggle="qtyProduk"]'), function(key, value){
		var minimal = parseInt($(value).attr('data-minimal'));
		var qty = parseInt($(value).val());
		var meta = $(value).attr('data-id');
		if(qty < minimal){
			$('[data-id="error-'+meta+'"]').text('Minimal pembelian '+minimal);
			result = false;
		}else{
			$('[data-id="error-'+meta+'"]').text('');
		}	
	})
	return result;
}

const counttotalCheckout = () => {
	var result = 0;
	var total_subtotal = parseInt($('#total_subtotal').val().split('.').join(''));
	var total_pengiriman = parseInt($('#total_pengiriman').val().split('.').join(''));
	var total_pemasangan = parseInt($('#total_pemasangan').val().split('.').join(''));
	result = total_subtotal + total_pemasangan + total_pengiriman;
	$('[data-total="total"] .float-end').text(''+FormatMoney(result));
	$('#total_total').val(result);
}

const showLoadingSwal = () => {
	Swal.fire({
		text:'',
		allowOutsideClick: false,
		showConfirmButton: false,
		customClass:{
			popup: 'swal-load-custom'
		}
	  })
	  Swal.showLoading('<div class="loading-gif"><img src="'+window.location.origin+'/assets/img/loading.gif"></di>');
	//   $('.swal2-actions.swal2-loading > *').remove();
	//   $('.swal2-actions.swal2-loading').append('<div class="loading-gif"><img src="'+window.location.origin+'/assets/img/loading.gif"></di>')	  
}

const changeAlamat = (id_alamat) => {
	if(id_alamat!= 0){
		$.ajax({
			url:'/checkout/changealamat',
			type:'GET',
			data:'id_alamat='+id_alamat,
			beforeSend:function(){
				showLoadingSwal();
			},success:function(result){
				if(result.is_success){
					Swal.close();
					$('[data-id="alamatUser"] .label-alamat').text(result.alamat.nama)
					$('[data-id="alamatUser"] .detail-alamat .alamat').text(result.alamat.alamat)
					$('#alamat').val(result.alamat.id);
					$('[data-total="pengiriman"]').removeClass('d-none')
					$('[data-total="pengiriman"] .float-end').text(''+FormatMoney(result.ongkir.ongkir))
					$('#total_pengiriman').val(result.ongkir.ongkir);
					counttotalCheckout();
				}
			}
		})
	}else{
		Swal.fire({
			icon: 'warning',
			text: 'Anda belum menambahkan alamat, silakan tambahkan terlebih dahulu...',
			showCancelButton: true,
			cancelButtonText: 'Tidak',
			confirmButtonText: 'Tambahkan',
			customClass:{
				header: 'custom-swal'
			}
		}).then((result) => {
			if("cancel" == result.dismiss){
				$('#delivery').val('tidak').trigger('change');
			}else if(result.value){
				window.location.href = window.location.origin+"/alamat";
			}
		})
	}
}

window.addEventListener( "pageshow", function ( event ) {
	var historyTraversal = event.persisted || 
						   ( typeof window.performance != "undefined" && 
								window.performance.navigation.type === 2 );
	if ( historyTraversal ) {
	  // Handle page restore.
	  window.location.reload();
	}
  });

$('[data-toggle="delete-alamat"]').click(function(){
	var id = $(this).attr('data-id-alamat')
	if(id){
		Swal.fire({
			icon:'warning',
			html:'<div>Apakah Anda yakin ingin menghapus <br> barang dari keranjang?</div>',
			showCancelButton:true,
			cancelButtonText:'Batal',
			confirmButtonText:'Hapus',
            confirmButtonColor: '#EF4242',
			customClass:{
				cancelButton: 'cancel-swal',
			}
		}).then((result) => {
			if(result.value){
				$.ajax({
					url:'/hapus/alamat',
					type:'GET',
					data:'id_alamat='+id,
					beforeSend:function(){
						showLoadingSwal();
					},success:function(result){
						Swal.close();
						if(result.is_success){
							setTimeout(() => {
								$('[data-alamat="alamat-'+result.id_alamat+'"]').remove();
								Swal.fire({
									icon:  'success',
									text:'Alamat berhasil dihapus',
									showConfirmButton: false,
									customClass:{
										header: 'custom-swal'
									},
								});
							}, 500);
						}else{
							setTimeout(() => {
								Swal.fire({
									icon:  'error',
									text:'Alamat tidak berhasil dihapus',
									showConfirmButton: false,
									customClass:{
										header: 'custom-swal'
									},
								});
							}, 500);
						}
					}
				})
			}
		})
	}
})

$("[data-scroll]").click(function(){
	var target = $(this).attr('data-scroll');
	if(target){
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 300);
	}
})

$('[data-copy-target]').click(function(){
	var value = $(this).attr('data-copy-target');
	if(value){
		CopyToClipboard(value,$(this));
	}
})

$('[data-copy-target]').mouseleave(function(){
	$(this).attr('data-bs-original-title', 'Copy to clipboard')
	.tooltip('show');
})

const CopyToClipboard = (id, $this) =>
{
	var r = document.createRange();
	r.selectNode(document.getElementById(id));
	window.getSelection().removeAllRanges();
	window.getSelection().addRange(r);
	document.execCommand('copy');
	window.getSelection().removeAllRanges();
	$($this).attr('data-bs-original-title', 'Copied!')
	.tooltip('show');
}

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})