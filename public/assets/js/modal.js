$('[data-toggle="modal"]').click(function() {
	var target = $(this).attr('data-bs-target');
	setTimeout(function(){
		$(target).modal('show');
	},500);
})


$(document).ready(function(){
	$('.show-password').click(function(){
		if(!$(this).hasClass('show')){
			$(this).prev().attr('type', 'text');
			$(this).addClass('show');
		}else{
			$(this).prev().attr('type', 'password');
			$(this).removeClass('show');
		}
	})
	if (getCookie('success_verifiy')) {
		eraseCookie('success_verifiy');
		Swal.fire({
		  icon: 'success',
		  title: 'Akun berhasil diverifikasi',
		  showConfirmButton: true,
		  customClass:{
			header: 'custom-swal'
		},
		}).then((result) => {
		//   if (result.value) {
		    // $('#ModalLogin').modal('show');
			// window.location
		//   }
		})
	}
	if(getCookie('un_login')){
		eraseCookie('un_login');
		setTimeout(() => {
			Swal.fire({
				icon:  'error',
				// title: 'Uppss',
				text:'Uppsss... Login dulu yukkkk',
				showConfirmButton: false,
				confirmButtonText: 'Login',
				customClass:{
					header: 'custom-swal'
				},
			}).then((result)=>{
				// console.log(result);
				// if(result.value){
					window.location.href = window.location.origin+'/login';
					// $('#ModalLogin').modal('show');
				// }
			})
		}, 500);
	}
	$('.telepon').mask('A8ZZZZZZZZZZZ', {
	  translation: {
	 	  'A': {
	 	  	pattern: /[0]/
	 	  },
	    'Z': {
	      pattern: /[0-9]/, optional: true
	    }
	  }
	});
    $('#RegisterForm').validate({
        rules:{
            name_register:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            email_register:{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email:true
            },
            telepon_register:{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength: 9,
                maxlength: 13
            },
            password_register:{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength: 8
            },
            password_repeat:{
                equalTo: "#password_register"
            },
        },
        messages:{
            name_register: "Nama Harus Diisi",
            email_register: {
                required: "Email Harus Diisi",
                email: 'Silakan masukan email yang valid'
            },
            telepon_register:{
                required: "Nomor Handphone Harus Diisi",
                minlength: jQuery.validator.format("Minimal {0} Karakter"),
                maxlength: jQuery.validator.format("Maximal {0} Karakter")
            },
            password_register: {
                required: "Password Harus Diisi",
                minlength: jQuery.validator.format("Password minimal {0} Karakter"),
            },
            password_repeat:{
            	required: "Password tidak sama, silakan ulangi kembali",
                equalTo: "Password tidak sama, silakan ulangi kembali"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        }
    });

    $('#RegisterForm').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
			if(!$('#DaftarButton').attr('disabled')){
				$('#DaftarButton').click();  
			}
        }
    });

       $('#DaftarButton').click(function(){
       	if ($('#RegisterForm').valid()) {
       		var post = $('#RegisterForm').serialize();
       		$.ajax({
       			url:'/postRegister',
       			type:'POST',
       			data:post,
       			beforeSend:function(){
       				$('#DaftarButton').attr('disabled', true);
					   showLoadingSwal();
       				// $('#DaftarButton').append('&nbsp;<i class="fas fa-circle-notch fa-spin"></i>');
       			},success:function(result){
					   Swal.close();
       				if (!result.is_error) {
       					$('#ModalRegister').modal('hide');
						// Swal.fire({
						// 	title: 'SELAMAT',
						// 	text: 'Akun anda sukses terdaftar, silakan cek email anda untuk verifikasi',
						// 	icon: 'success',
						// 	customClass:{
						// 		header: 'custom-swal'
						// 	},
						// 	showConfirmButton: false
						// })
						Swal.fire({
							html:
							'<div><img src="'+window.location.origin+'/assets/img/success_daftar.svg" alt="data"></div><div style="text-align: center;margin-bottom: 6px;margin-top: 30px;font-weight: 500;font-size: 20px;">Selamat, Anda Berhasil Mendaftar!</div>',
							customClass:{
								confirmButton: 'btn btn-success btn-custom',
								popup: 'swal-custom-padding'
							},
							confirmButtonText: 'Kembali ke beranda',
							width:600,
							confirmButtonColor:'#57D024',
							allowOutsideClick: false
						}).then((value) => {
							if(value.value){
								window.location.href = '/'
							}
						})
       				}else{
       					if (!result.no_hp && !result.email) {
       						$('#ModalRegister').modal('hide');
							Swal.fire({
							  text: 'Nomor handphone atau email sudah terdaftar',
							  icon: 'error',
							  confirmButtonText: 'Close',
							  customClass:{
								header: 'custom-swal'
							},
							})
       					}
       				}
       				setTimeout(function(){
	       				$('#DaftarButton').attr('disabled', false);
	       				$('#DaftarButton').text('DAFTAR');
       				}, 500);
       			}, error:function(jqXHR, textStatus, errorThrown){
					if(jqXHR.status != 200){
						Swal.close();
						Swal.fire({
							icon:'error',
							text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
							customClass:{
								header: 'custom-swal'
							},
						})
						console.log(errorThrown);
					}
				}
       		})
       	}
       });

    $('#LoginForm').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
			if(!$('#LoginButton').attr('disabled')){
				$('#LoginButton').click();  
			}
        }
    });

    $('#LoginForm').validate({
    	rules:{
            username:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            password:{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength: 8
            },
    	},
        messages:{
            password: {
	            required: "Password Harus Diisi",
                minlength: jQuery.validator.format("Minimal {0} Karakter"),
            },
            username: {
                required: "Username harus Diisi",
                email: 'Silakan masukan email yang valid',
                minlength: jQuery.validator.format("Minimal {0} Karakter"),
                maxlength: jQuery.validator.format("Maximal {0} Karakter")
            }
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        }
    });

    $('#username').keyup(function(){
    	var val = $(this).val();
    	var Exp = /^\d+$/;
    	$('#username').rules('remove')
    	if (typeof(parseInt(val)) == 'number' && !isNaN(parseInt(val)) && val.match(Exp)) {
    		$('#username').rules('add',{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength: 9,
                maxlength: 13
    		})
    		// console.log('nomor_telepon');
    	}else{
    		$('#username').rules('add',{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                email:true
    		})
    	}
    });

    $('#LoginButton').click(function(){
    	if($('#LoginForm').valid()){
    		var post = $('#LoginForm').serialize();
    		$.ajax({
    			url:'/postlogin',
    			type:'POST',
    			data: post,
    			beforeSend:function(){
       				$('#LoginButton').attr('disabled', true);
					   showLoadingSwal();
       				// $('#LoginButton').append('&nbsp;<i class="fas fa-circle-notch fa-spin"></i>');
					
    			}
    			,success:function(result){
					Swal.close();
    				if (result.is_success) {
    					window.location.href = window.location.origin;
    				}else{
    					$('#password_validate').text('');
    					if (!result.is_exist) {
    						$('#password_validate').append('<label id="password-error" class="error" for="password">Email/Nomor Telepon tidak terdaftar</label>')
    					}else if(!result.is_verified){
    						$('#password_validate').append('<div id="password-error" class="error">Akun belum terverifikasi,Klik <span id="reverif" class="text-primary pointer">disini</span> untuk verifikasi</div>');
							    $('#reverif').click(function(){
							    	$('#ModalLogin').modal('hide');
							    	$.ajax({
							    		url:'/resendVerify',
							    		type:'GET',
							    		data:'username='+$('#username').val(),
							    		beforeSend:function(){
									        Swal.fire({
									          text:'Tunggu Sebentar...',
									          allowOutsideClick: false,
									          showConfirmButton: false
									        })
									        Swal.showLoading();
							    		},
							    		success:function(result){
							    				Swal.close();
							    			if (result) {
							    				setTimeout(function(){
													Swal.fire({
														html:
														'<div><img src="'+window.location.origin+'/assets/img/mail_sukses.svg" alt="data"></div><div style="text-align: center;margin-bottom: 6px;margin-top: 30px;font-weight: 500;font-size: 20px;">Email Verifikasi berhasil terkirim!</div>',
														customClass:{
															confirmButton: 'btn btn-success btn-custom',
															popup: 'swal-custom-padding'
														},
														confirmButtonText: 'Kembali ke beranda',
														width:600,
														confirmButtonColor:'#57D024',
														allowOutsideClick: false
													}).then((value) => {
														if(value.value){
															window.location.href = '/'
														}
													})
							    				}, 500)
							    			}else{
							    				setTimeout(function(){
											        Swal.fire({
											          text:'Email Verifikasi tidak berhasil terkirim',
											          icon:'error',
													  customClass:{
														header: 'custom-swal'
													},
											        })
							    				}, 500)
							    			}
							    		}
							    	})
							    })
    					}else if(!result.is_success){
    						$('#password_validate').append('<label id="password-error" class="error" for="password">Email/Nomor Telepon/Password Salah, tolong periksa kembali</label>')
    					}
    				}
       				setTimeout(function(){
	       				$('#LoginButton').attr('disabled', false);
	       				$('#LoginButton').text('Login');
       				}, 500);
    			}, error:function(jqXHR, textStatus, errorThrown){
					if(jqXHR.status != 200){
						Swal.close();
						Swal.fire({
							icon:'error',
							text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
							customClass:{
								header: 'custom-swal'
							},
						})
						console.log(errorThrown);
					}
				}
    		})
    	}
    });

    $('#ForgotPasswordForm').validate({
    	rules:{
    		email_password:{
    			required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
    			},
    			email: true,
    		}
    	},
    	messages:{
    		email_password:{
    			required: "Email harus diisi",
    			email: "silakan masukan email yang valid"
    		}
    	},errorPlacement:function(error, element){
    		var name = $(element).attr('name')
    		error.appendTo($("#"+name+"_validate"));
    	}
    });

    $('#ForgotPasswordForm').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
			if(!$('#ResetButton').attr('disabled')){
				$('#ResetButton').click();  
			}
        }
    });

    $('#ResetButton').click(function(){
    	if ($('#ForgotPasswordForm').valid()) {
    		var post = $('#ForgotPasswordForm').serialize();
    		$.ajax({
    			url:'/sendemail/forgetpass',
    			type:'POST',
    			data:post,
    			beforeSend:function(){
       				// $('#ResetButton').attr('disabled', true);
					   showLoadingSwal();
       				// $('#ResetButton').append('&nbsp;<i class="fas fa-circle-notch fa-spin"></i>');
    			},
    			success:function(result){
					Swal.close();
    				$('#ResetButton').attr('disabled', false);
    				$('#ResetButton').text("RESET PASSWORD");
    				if (result.is_success) {
    					$('#ModalForgot').modal('hide');
    					setTimeout(function(){
							Swal.fire({
								html:
								'<div><img src="'+window.location.origin+'/assets/img/mail_sukses.svg" alt="data"></div><div style="text-align: center;margin-bottom: 6px;margin-top: 30px;font-weight: 500;font-size: 20px;">Periksa email Anda untuk mencari tautan untuk mengatur ulang kata sandi Anda. Jika tidak muncul dalam beberapa menit, periksa folder spam Anda!</div>',
								customClass:{
									confirmButton: 'btn btn-success btn-custom',
									popup: 'swal-custom-padding'
								},
								confirmButtonText: 'Kembali ke beranda',
								width:600,
								confirmButtonColor:'#57D024',
								allowOutsideClick: false
							}).then((value) => {
								if(value.value){
									window.location.href = '/'
								}
							})
    					}, 500)
    				}else{
    					$('#ModalForgot').modal('hide');
    					setTimeout(function(){
    						Swal.fire({
    							icon:'error',
    							text:'Terjadi kesalahan, tunggu beberapa saat dan coba lagi',
    							showConfirmButton: false,
								customClass:{
									header: 'custom-swal'
								},
    						});
    					}, 500)
    				}
    			}, error:function(jqXHR, textStatus, errorThrown){
					if(jqXHR.status != 200){
						Swal.close();
						Swal.fire({
							icon:'error',
							text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
							customClass:{
								header: 'custom-swal'
							},
						})
						console.log(errorThrown);
					}
				}
    		})
    	}
    });

    $('#ResetPassword').validate({
        rules:{
            password:{
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength: 8
            },
            password_repeat:{
                equalTo: "#password"
            },
        },
        messages:{
            password: {
                required: "Password Harus Diisi",
                minlength: jQuery.validator.format("Password minimal {0} Karakter"),
            },
            password_repeat:{
            	required: "Password tidak sama, silakan ulangi kembali",
                equalTo: "Password tidak sama, silakan ulangi kembali"
            },
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        }
    });

    $('#ResetPassword').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
			if(!$('#BtnReset').attr('disabled')){
				$('#BtnReset').click();  
			}
        }
    });


    $('#BtnReset').click(function(){
    	if($("#ResetPassword").valid()){
    		var urlParams = new URLSearchParams(window.location.search);
    		var post = $('#ResetPassword').serialize();
    		var token = urlParams .get('token');
    		var email = urlParams .get('email');
    		$.ajax({
    			url:'/resetpassword',
    			type:'POST',
    			data:post+'&token='+token+'&email='+email,
    			beforeSend:function(){
    				$('#ResetButton').attr('disabled', true);
					showLoadingSwal();
    				// $('#ResetButton').append('&nbsp;<i class="fas fa-circle-notch fa-spin"></i>');
    			}, success:function(result){
					Swal.close();
    				if (result.is_success) {
    					setTimeout(function(){
							Swal.fire({
								html:
								'<div><img src="'+window.location.origin+'/assets/img/password_sukses.svg" alt="data"></div><div style="text-align: center;margin-bottom: 6px;margin-top: 30px;font-weight: 500;font-size: 20px;">Password berhasil diperbarui!</div>',
								customClass:{
									confirmButton: 'btn btn-success btn-custom',
									popup: 'swal-custom-padding'
								},
								confirmButtonText: 'Login sekarang',
								width:600,
								confirmButtonColor:'#57D024',
								allowOutsideClick: false
							}).then((value) => {
								if(value.value){
									window.location.href = '/login'
								}
							})
    					}, 500)
    				}else{
    					setTimeout(function(){
    						Swal.fire({
    							icon:'error',
    							text:'Terjadi kesalahan, tunggu beberapa saat dan coba lagi',
    							showConfirmButton: false,
								customClass:{
									header: 'custom-swal'
								},
    						});
    					}, 500)
    				}
    			}, error:function(jqXHR, textStatus, errorThrown){
					if(jqXHR.status != 200){
						Swal.close();
						Swal.fire({
							icon:'error',
							text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat',
							customClass:{
								header: 'custom-swal'
							},
						})
						console.log(errorThrown);
					}
				}
    		})
    	}
    })
})



function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
} 

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

// Swal.fire({
//     html:
//     '<div><img src="http://localhost:8000/assets/img/success_daftar.svg" alt="data"></div><div style="text-align: center;margin-bottom: 6px;margin-top: 30px;font-weight: 500;font-size: 20px;">Selamat, Anda Berhasil Mendaftar!</div>',
//     customClass:{
//     	confirmButton: 'btn btn-success btn-custom',
// 		popup: 'swal-custom-padding'
//     },
//     confirmButtonText: 'Kembali ke beranda',
// 	width:600,
// 	confirmButtonColor:'#57D024',
// 	allowOutsideClick: false
// }).then((value) => {
// 	if(value.value){
// 		window.location.href = '/'
// 	}
// })