$(document).ready(function(){
    $('#CheckoutForm').validate({
        rules:{
            nama:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                }
            },
            nomor:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                minlength:9,
                maxlength:13
            },
            proyek:{
                required:false,
            },
            instalasi:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            delivery:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            },
            date:{
                required:{
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
            }
        },
        messages:{
            nama: "Nama Harus Diisi",
            telepon:{
                required: "Nomor Handphone Harus Diisi",
                minlength: jQuery.validator.format("Minimal {0} Karakter"),
                maxlength: jQuery.validator.format("Maximal {0} Karakter")
            },
            instalasi: "Pemasangan Harus Diisi",
            delivery: "Metode Pengiriman Harus Diisi",
            date: "Tanggal ini Harus Diisi",
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_error"));
        }
    })
	$('.telepon').mask('A8ZZZZZZZZZZZ', {
        translation: {
             'A': {
                 pattern: /[0]/
             },
          'Z': {
            pattern: /[0-9]/, optional: true
          }
        }
      });
})

$('#btnSaveCheckout').click(function(){
    if($('#CheckoutForm').valid()){
        var post = $('#CheckoutForm').serialize();
        $.ajax({
            url:window.location.origin+'/saveorder',
            type:'POST',
            data:post,
            beforeSend:function(){
                showLoadingSwal();
            },
            success:function(result){
                Swal.close();
                if(result.is_success){
                    window.location.href = result.redirect;
                }else{
                    Swal.fire({
                        icon:'error',
                        title:'Oops...',
                        text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat'
                    })
                }
            }, error:function(jqXHR, textStatus, errorThrown){
                if(jqXHR.status != 200){
                    Swal.close();
                    Swal.fire({
                        icon:'error',
                        title:'Oops...',
                        text:'terjadi kesalahan, silakan anda coba lagi dalam beberapa saat'
                    })
                    console.log(jqXHR);
                }
            }
        })
    }
})