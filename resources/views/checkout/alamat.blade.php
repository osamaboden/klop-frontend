@include('layouts.header')
<main id="AlamatBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/checkout">Checkout</a></li>
          <li class="breadcrumb-item active">Tambah Alamat</li>
        </ol>
    </nav>
    <form id="AlamatForm">
        @csrf
        <div class="row m-0">
            <div class="col-md-12 mx-auto">
                @if (is_null($data['data']))
                    <div class="title-page">Tambah Alamat</div>                    
                @else
                    <div class="title-page">Ubah Alamat</div> 
                    <input type="hidden" id="id_alamat" name="id_alamat" value="{{ $data['data']->id }}">                   
                @endif
                <div class="row m-0">
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="form-group">
                            <label class="form-label">Label Alamat</label>
                            <input type="text" name="label_alamat" placeholder="contoh: Alamat rumah, Alamat Kantor, Apartemen" id="label_alamat" @if (!is_null($data['data'])) value="{{ $data['data']->nama }}" @endif class="form-control">
                            <span id="label_alamat_error" class="text-danger mb-2 mt-2"></span> 
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="form-group">
                            <label class="form-label">Provinsi</label>
                            <select name="provinsi" id="provinsi" required class="provinsi form-select" data-placeholder="Provinsi">
                                <option></option>
                                @foreach ($data['provinsi'] as $item)
                                    <option value="{{ $item->id }}" @if (!is_null($data['data'])) @if($data['data']->provinsi == $item->id) selected @endif @endif>{{ $item->nama }}</option> 
                                @endforeach
                            </select>
                            <span id="provinsi_error" class="text-danger mb-2 mt-2"></span> 
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="form-group">
                            <label class="form-label">Kabupaten</label>
                            <select name="kabupaten" id="kabupaten" required @if (!is_null($data['data'])) data-selected="{{ $data['data']->kabupaten }}" @endif  disabled class="kabupaten form-select" data-placeholder="Kabupaten">
                                <option></option>
                            </select>
                            <span id="kabupaten_error" class="text-danger mb-2 mt-2"></span> 
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 mb-3">
                        <div class="form-group">
                            <label class="form-label">Kecamatan</label>
                            <select name="kecamatan" id="kecamatan" @if (!is_null($data['data'])) data-selected="{{ $data['data']->kecamatan }}" @endif required disabled class="kabupaten form-select" data-placeholder="Kecamatan">
                                <option></option>
                            </select>
                            <span id="kecamatan_error" class="text-danger mb-2 mt-2"></span> 
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 mb-3">
                        <div class="form-group">
                            <label class="form-label">Alamat</label>
                            <textarea class="form-control" id="input-address" name="address" placeholder="Isi dengan nama jalan, nomor rumah, nomor kompleks, nama gedung, lantai atau nomor unit" rows="4">@if (!is_null($data['data'])) {{ $data['data']->alamat }} @endif</textarea>
                            <input type="hidden" name="koordinat" id="koordinat" @if (!is_null($data['data'])) value="{{ $data['data']->koordinat }}" @endif>
                            <span id="alamat_error" class="text-danger mb-2 mt-2"></span> 
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 mb-3">
                        <div class="form-group">
                            <label class="form-label">Catatan</label>
                            <textarea class="form-control" id="catatan" name="catatan" placeholder="Catatan" rows="4">@if (!is_null($data['data'])) {{ $data['data']->catatan }} @endif</textarea>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 mb-3">
                        <div id="maps-box"></div>
                    </div>
                    <div class="col-5 col-md-3 col-sm-4 col-lg-3 mx-auto mb-3 mt-2">
                        <button type="button" id="SimpanAlamat" class="btn btn-success btn-custom">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>

@include('layouts.footer')