@include('layouts.header')
<main id="CheckoutBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active">Checkout</li>
        </ol>
    </nav>
    <form id="CheckoutForm">
    @csrf
    <div class="row m-0">
        <div class="col-lg-6 col-md-12 col-sm-12 col-12 ps-0 pe-0">
            <div class="title-checkout">
              <span>Info Pemesanan</span>
              <span>Tolong isikan info pemesanan</span>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0">
                <label class="form-label" for="nama">Nama Lengkap</label>
                <input type="text" name="nama" id="nama" required readonly placeholder="Nama Lengkap" class="form-control" value="{{ $data['data']->nama }}">
                <div class="text-danger pt-2 pb-2" id="nama_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0">
              <label class="form-label" for="nomor">Nomor Handphone</label>
              <input type="text" name="nomor" id="nomor" value="{{ $data['data']->nomor }}" required placeholder="contoh 08123456789" class="form-control telepon">
              <div class="text-danger pt-2 pb-2" id="nomor_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0">
              <label class="form-label" for="proyek">Pilihan Proyek (Optional)</label>
              <select name="proyek" id="proyek" class="select2 form-select" data-placeholder="Gedung/Rumah/Kantor">
                <option></option>
                @foreach ($data['proyek'] as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @endforeach
              </select>
              <div class="text-danger pt-2 pb-2" id="proyek_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0">
              <label class="form-label" for="instalasi">Termasuk Pemasangan</label>
              <select name="instalasi" id="instalasi" required class="select2 form-select" data-placeholder="Ya/Tidak">
                <option></option>
                <option value="ya">Ya</option>
                <option value="tidak" selected>Tidak</option>
              </select>
              <div class="text-danger pt-2 pb-2" id="instalasi_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0">
              <label class="form-label" for="delivery">Metode Pengiriman</label>
              <select name="delivery" id="delivery" class="select2 form-select" required data-placeholder="Diantarkan/Ambil Sendiri">
                <option></option>
                <option value="ya">Diantarkan</option>
                <option value="tidak" selected>Ambil Sendiri</option>
              </select>
              <div class="text-danger pt-2 pb-2" id="delivery_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0" data-box="tanggal">
              <label for="date" class="form-label">Tanggal Pengiriman</label>
              <input type="text" class="form-control" id="date" name="date" required placeholder="Tanggal Pengiriman">
              <div class="text-danger pt-2 pb-2" id="date_error"></div>
            </div>
            <div class="col-12 mb-0 mb-sm-4 p-0" data-box="alamat">
              <label class="form-label label-alamat">Alamat Pengambilan</label>
              <button type="button" data-bs-target="#ModalAlamat" data-id="alamatUser" data-bs-toggle="modal" class="btn btn-success btn-sm btn-custom alamat-button d-none">Pilih Alamat Lain</button> 
              <a href="https://www.google.com/maps/search/?api=1&query={{ (new \App\Helpers\helper)->getSetting('koordinat_toko') }}" target="_blank" data-id="alamatToko" class="btn btn-success btn-sm btn-custom alamat-button">Lihat Di Google Maps</a> 
              <div class="clearfix"></div>
              <div class="box-alamat-top border border-custom d-none" data-id="alamatUser">
                <div class="icon-alamat">
                  <div class="icon">
                    <i class="fas fa-home"></i>
                  </div>
                </div>
                <div class="detail-alamat">
                  <div>
                    @if (!is_null($data['alamat']['active']))
                    <div class="label-alamat"><span>{{ $data['alamat']['active']->nama }}</span></div>
                      <div class="alamat">{{ ucwords($data['alamat']['active']->alamat) }}</div>
                      <input type="hidden" name="alamat" id="alamat" value="{{ $data['alamat']['active']->id }}">
                    @else
                      <div class="label-alamat"></div>
                      <div class="alamat"></div>
                      <input type="hidden" name="alamat" id="alamat" value="0">
                    @endif
                  </div>
                </div>
              </div>
              <div class="box-alamat-top border border-custom" data-id="alamatToko">
                <div class="icon-alamat">
                  <div class="icon">
                    <i class="fas fa-store"></i>
                  </div>
                </div>
                <div class="detail-alamat">
                  <div>
                    <div class="label-alamat"><span>{{ (new \App\Helpers\helper)->getSetting('nama') }}</span></div>
                    <div class="alamat">{{ (new \App\Helpers\helper)->getSetting('alamat') }}</div>
                  </div>
                </div>
              </div>
            </div>
        </div>
          <div class="col-lg-6 col-md-12 col-sm-12 col-12 ps-lg-6 ps-0 pe-0">
            <div id="rangkuman" class="border border-custom">
              <div class="title-checkout">
                <span>Rangkuman Pemesanan</span>
                <span></span>
              </div>
              <div class="list-rangkuman">
                <?php $data['total_keranjang'] = 0; ?>
                @foreach ($data['keranjang'] as $item)
                <?php $data['total_keranjang'] += $item['harga_total']; ?>
                <div class="card mb-3" id="{{ $item['session'] }}">
                  <div class="row g-0">
                    <div class="col-sm-4 col-3">
                      <img src="{{ ENV('DATA_URL')."/galeri/".$item['image'] }}" style="max-height:100%;max-width:100%;border-radius: 10px;" alt="gambar">
                    </div>
                    <div class="col-sm-8 col-9">
                      <div class="card-body p-0 pl-2 pr-2">
                        <div class="row m-0">
                          <div class="col-6 list-name">{{ $item['nama'] }}</div>
                          <div class="col-6 price ps-0">{{ number_format($item['harga_total'], 0, '.','.') }}</div>
                          <div class="col-6 list-qty pe-0">
                              <span>Jumlah :&nbsp;</span>
                              <span> {{ $item['qty'] }}{{ $item['satuan_beli'] }}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
  
              <div class="price-total">
                <div class="subtotal clearfix" data-total="subtotal">
                  <div class="float-start">Subtotal</div>
                  <div class="float-end">{{ number_format($data['total_keranjang'], 0,'.','.') }}</div>
                  <input type="hidden" name="total_subtotal" id="total_subtotal" value="{{$data['total_keranjang']}}">
                </div>
                <div class="subtotal clearfix d-none" data-total="pengiriman">
                  <div class="float-start">Pengiriman</div>
                  @if (!is_null($data['alamat']['active']))
                  <div class="float-end">{{ number_format(0, 0, '.','.') }}</div>
                  <input type="hidden" name="total_pengiriman" id="total_pengiriman" value="{{0}}">
                  @else
                  <div class="float-end">{{ number_format(0, 0, '.','.') }}</div>
                  <input type="hidden" name="total_pengiriman" id="total_pengiriman" value="{{0}}">
                  @endif
                </div>
                <div class="subtotal clearfix d-none" data-total="pemasangan">
                  <div class="float-start">Pemasangan</div>
                  <div class="float-end">Rp0</div>
                    <input type="hidden" name="total_pemasangan" id="total_pemasangan" value="0">
                </div>
                <div class="total clearfix" data-total="total">
                  <div class="float-start">Total</div>
                  <div class="float-end">Rp5.000</div>
                  <input type="hidden" name="total_total" id="total_total" value="0">
                </div>
                <button type="button" id="btnSaveCheckout" class="btn btn-success btn-custom mb-3 mt-3">Beli Sekarang</button>
              </div>
            </div>
          </div>
    </div>
    </form>
</main>

<div class="modal fade" id="ModalAlamat" tabindex="-1" aria-labelledby="ModalAlamatLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalAlamatLabel">Daftar Alamat</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <a href="{{ url('/alamat') }}" class="btn btn-success btn-custom">Tambah Alamat</a>
        <div class="list-alamat mt-4">
          @foreach ($data['alamat']['list'] as $item)
          <div class="box-alamat row m-0" data-id="alamat-{{ $item->id }}">
            <div class="col-10">
              <div class="alamat-label">{{ $item->nama }}</div>
              <div class="alamat-desc">{{ ucwords($item->alamat) }}</div>
              <a href="{{ url('/alamat/'.$item->id) }}" class="change-alamat">Ubah Alamat</a>
            </div>
            <div class="col-2 d-flex justify-content-center align-items-center">
              @if(!is_null($data['alamat']['active']))
              <button type="button" class="btn btn-outline-success @if($item->id == $data['alamat']['active']->id) d-none @endif" data-id="alamat-{{ $item->id }}" data-toggle="pilih_alamat">Pilih</button>
              @endif
            </div>            
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>

@include('layouts.footer')