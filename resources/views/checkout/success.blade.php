@include('layouts.header')
<main id="SuccessBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active">Checkout</li>
          <li class="breadcrumb-item active">Pembayaran</li>
        </ol>
    </nav>
    <div class="row m-0">
        <div class="col-lg-7 col-md-9 col-sm-11 col-12 mx-sm-auto ps-0 pe-0">
            <div class="card border-success success-box">
                <div class="card-body">
                    <div class="detail-bayar row m-0 text-center">
                        <div class="col-12 label">ID Transaksi</div>
                        <div class="col-12 text-greeen">{{ $data['id'] }}</div>
                        <div class="col-12 label">Total(Rp):</div>
                        <div class="col-12 price-total">{{ number_format($data['total'], 0, '.','.') }}</div>
                        <div class="col-8 mx-auto box-rekening">
                            <div class="bank"><img src="{{ ENV('DATA_URL').'/master_data/'.(new \App\Helpers\helper)->getSetting('logo_bank') }}"></div>
                            <div class="mb-2">Bank {{ (new \App\Helpers\helper)->getSetting('nama_bank') }}  </div>
                            <div class="mb-2"><span>Atas Nama</span> {{ (new \App\Helpers\helper)->getSetting('nama_rekening') }}  </div>
                            <div id="NomorRekening"><span>Rekening  </span>{{ (new \App\Helpers\helper)->getSetting('nomor_rekening') }} <span data-copy-target="NomorRekening" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Copy to clipboard">Salin</span></div>
                        </div>
                        {{-- <div class="col-12 text-konfirmasi">Konfimasi Pembayaran</div> --}}
                        {{-- <div class="col-12 button-konfirmasi"><a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" class="btn btn-outline-success btn-custom"><img src="{{ asset('assets/img/whatsapp_button.svg') }}" alt="img"> Melalui Whatsapp</a> <a href="{{ url('/konfirmasi?id_order='.$data['id']) }}" class="btn btn-outline-success btn-custom"><img src="{{ asset('assets/img/konfirmasi_button.svg') }}" alt="img"> Melalui Web</a></div> --}}
                        <div class="col-12 col-sm-6 mb-3"><a href="{{ url('/transaksi/detail/'.$data['id']) }}" class="btn btn-success btn-large btn-custom">Lihat Detail Transaksi</a></div>
                        <div class="col-12 col-sm-6 mb-3">
                            <div class="dropdown">
                                <button class="btn btn-success btn-large btn-custom dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    Konfirmasi Pembayaran
                                </button>
                                <ul style="width: 100%" class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                  <li><a href="{{ url('/konfirmasi?id_order='.$data['id']) }}" class="dropdown-item"><img src="{{ asset('assets/img/konfirmasi_button.svg') }}" alt="img" class="me-2">Melalui Website</a></li>
                                  <li><a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" target="_blank" class="dropdown-item"><img src="{{ asset('assets/img/whatsapp_button.svg') }}" style="width: 26px" alt="img" class="me-2">Melalui Whatsapp</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>



@include('layouts.footer')