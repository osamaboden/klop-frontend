    @include('layouts.header')
        <main id="KeranjangBox" class="container-xxl">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb produk">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
                  <li class="breadcrumb-item active">Keranjang</li>
                </ol>
            </nav>
            <form id="KeranjangForm">

            @csrf
            <div class="row m-0">
                <?php
                $keranjang = (new \App\Helpers\helper)->getlistKeranjang();
                $total = 0;
                ?>
                @if (count($keranjang) > 0)
                <div class="col-lg-10 col-md-12 col-sm-12 col-12 mx-auto ps-0 pe-0">
                @else
                <div class="col-lg-5 col-md-6 col-sm-6 col-12 mx-auto ps-0 pe-0">
                @endif
                    <div id="listKeranjang">
                        <div class="card-body ps-0 pe-0 text-center @if (count($keranjang) <= 0) flex-center @endif" >
                            @if (count($keranjang) > 0)
                            <div class="card-title">Keranjang</div>
                            <table class="table table-responsive mt-4">
                                <thead>
                                    <tr>
                                        <th>Produk</th>
                                        <th style="width: 1%">Jumlah</th>
                                        <th style="width: 17%">Total</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($keranjang as $item)
                                    <tr data-delsession="{{ $item['session'] }}">
                                        <td class="image">
                                            <div class="box-image-keranjang">
                                                <div class="image"><img src="{{ ENV('DATA_URL').'/galeri/'.$item['image'] }}" alt=""></div>
                                                <div class="image-caption">{{ $item['nama'] }}</div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="spin-input">
                                                        <input type="text" class="form-control angka text-center" data-id="{{ $item['meta'] }}" data-toggle="qtyProduk" value="{{ $item['qty'] }}" data-minimal="{{ $item['qty_satuan'] }}" data-minimal-meter="{{ ceil($item['qty_satuan']*$item['panjang']) }}" data-panjang="{{ $item['panjang'] }}" name="qty-val[]" placeholder="Kuantitas">
                                                        <select name="satuan_beli[]" id="satuan_beli" data-toggle="satuan_beli" data-id="{{ $item['meta'] }}" data-minimal="{{ $item['qty_satuan'] }}" data-panjang="{{ $item['panjang'] }}" onfocus="this.size=2;" onblur="this.size=1;" onchange="this.size=1; this.blur();" class="form-select form-beli">
                                                            <option value="pcs" @if($item['satuan_beli'] == "pcs") selected @endif>Pcs</option>
                                                            <option value="meter" @if($item['satuan_beli'] == "m") selected @endif>Meter</option>
                                                        </select>
                                                        {{-- <button type="button" class="btn btn-plus" @if($item['qty'] <= $item['qty_satuan']) disabled @endif data-id="{{ $item['meta'] }}" data-minimal="{{ $item['qty_satuan'] }}" data-type="min" data-qty="1"><i class="fas fa-caret-down"></i></button>
                                                        <button type="button" class="btn btn-plus" data-id="{{ $item['meta'] }}" data-minimal="{{ $item['qty_satuan'] }}" data-type="plus" data-qty="1"><i class="fas fa-caret-up"></i></button> --}}
                                                    </div>
                                                </div>
                                                <span class="text-danger" id="text-error" data-id="error-{{ $item['meta'] }}"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="subtotal" data-total="{{ $item['meta'] }}" data-id="{{ $item['meta'] }}">{{ number_format($item['harga_total'], 0, '.','.') }}</div>
                                            <input type="hidden" name="subtotal[]" class="form-control" readonly value="{{ number_format($item['harga_total'], 0, '.','.') }}" data-total="{{ $item['meta'] }}" data-id="{{ $item['meta'] }}">
                                            <input type="hidden" name="session[]" value="{{ $item['session'] }}">
                                            <input type="hidden" name="qty_pcs[]" class="qty_pcs" value="{{ $item['qty_pcs'] }}" data-id="{{ $item['meta'] }}">
                                        </td>
                                        <td><a href="#" class="text-danger" data-toggle="deletecart" data-session="{{ $item['session'] }}"><i class="far fa-trash-alt"></i></a></td>
                                    </tr>
                                    <?php $total += $item['harga_total']; ?>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">Total Belanja</td>
                                        <td colspan="1" id="TotalKeranjang">{{ number_format($total, 0, '.','.') }}</td>
                                        <td></td>
                                    </tr>
                                </tfoot>
                            </table>
                            <button type="button" id="BtnKeranjangForm" class="btn btn-success btn-custom mt-3 mb-3" style="width: unset">Bayar Sekarang</button>
                            @else
                            <div class="keranjang-blank">
                                <div>
                                    <img src="/assets/img/keranjang_times.svg" style="width: 55%;" alt="keranjang_times">
                                </div>
                                <div>
                                    <span>Tidak ada barang di keranjang!</span>

                                    <a href="/" class="btn btn-success btn-custom mt-3">Kembali Ke Beranda</a>
                                </div>
                            </div>   
                            @endif
                        </div> 
                    </div>
                </div>
            </div>
            
            </form>
        </main>

    @include('layouts.footer')