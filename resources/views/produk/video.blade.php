@include('layouts.header')
<main id="BoxVideo" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Produk</a></li>
          <li class="breadcrumb-item active" aria-current="page">Semua Video</li>
        </ol>
    </nav>
    <div class="row m-0">
        <div class="col-sm-12 col-md-12 col-12 ps-0 pe-0">
            <div class="text-center mb-4">
                <div><h3>Galeri Video</h3></div>
            </div>
            <div class="Listvideo row me-0 ms-0" id="ListVideo">
                @foreach ($data['data'] as $value)
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <div class="box-gallery lazy_loading">
                        <div class="show-detail gallery-video">
                            <a data-caption="<?php echo $value->title;?>" class="fancybox-media" data-href="<?php echo $value->nama_file;?>">
                                <svg width="94" height="93" viewBox="0 0 94 93" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="46.9864" cy="46.4322" r="45.3589" stroke="white" stroke-width="2"/>
                                    <path d="M76.2734 44.2269C78.2734 45.3816 78.2734 48.2684 76.2734 49.4231L35.1822 73.1471C33.1822 74.3018 30.6822 72.8584 30.6822 70.549L30.6822 23.101C30.6822 20.7916 33.1822 19.3482 35.1822 20.5029L76.2734 44.2269Z" fill="white"/>
                                </svg>	
                            </a>
                        </div>
                        <img data-src="{{ $value->url_gambar }}" class="image lazy d-none" alt="image1">
                    </div>
                </div>
                @endforeach
            </div>
            <div class="load-infinate" style="display: none">
                <img src="{{ asset('assets/img/loading.gif') }}" alt="">
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')