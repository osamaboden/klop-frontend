@include('layouts.header')
<main id="ReviewBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Produk</a></li>
          <li class="breadcrumb-item active" aria-current="page">Ulasan</li>
        </ol>
    </nav>
    <div class="row m-0">
        <div class="col-sm-12 col-md-12 col-12 ps-0">
            <div class="text-center mb-4">
                <div><h3>Ulasan</h3></div>
            </div>
            <div class="filter">
                <span>Filter</span>
                <ul>
                    <li data-select="all" class="active">Semua</li>
                    <li data-select="5">5&nbsp;<i class="fas fa-star"></i></li>
                    <li data-select="4">4&nbsp;<i class="fas fa-star"></i></li>
                    <li data-select="3">3&nbsp;<i class="fas fa-star"></i></li>
                    <li data-select="2">2&nbsp;<i class="fas fa-star"></i></li>
                    <li data-select="1">1&nbsp;<i class="fas fa-star"></i></li>
                </ul>
            </div>
            <div class="list-ulasan" id="listulasan">
                @foreach ($data['data'] as $item)
                <div class="card card-body ps-0 ps-sm-3 pe-0 pe-sm-3">
                    <div class="row m-0">
                        <div class="col-lg-2 col-md-3 col-12 d-flex">
                            <div class="icon-ulasan">
                                <img src="{{ ENV('DATA_URL')."/profile/".$item->foto_profile }}" alt="user">
                            </div>
                            <div class="ulasan-name">
                                <div>{{ $item->nama }}</div>
                                <div class="star">
                                    <?php
                                    $star = $item->skor;
                                    ?>
                                    <span class="star">
                                        @for ($i = 0; $i < $star; $i++)
                                        <i class="fas fa-star"></i>
                                        @endfor
                                        @for ($i = 0; $i < (5-$star); $i++)
                                        <i class="far fa-star"></i>
                                        @endfor
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-9 pt-3 pt-md-0 col-12 desc">
                            {{ $item->review }}
                        </div>
                    </div>
                </div>                             
                @endforeach
            </div>
            <div class="load-infinate" style="display: none">
                <img src="{{ asset('assets/img/loading.gif') }}" alt="">
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')