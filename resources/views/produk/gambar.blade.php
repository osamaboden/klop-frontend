@include('layouts.header')
<main id="BoxGambar" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="#">Produk</a></li>
          <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
        </ol>
    </nav>
    <div class="row m-0">
        <div class="col-sm-12 col-md-12 col-12 ps-0">
            <div class="text-center mb-4">
                <div><h3>{{ $title }}</h3></div>
            </div>
            <div class="listGambar gallerylist" id="listGambar">
                {!! $data['html'] !!}
                {{-- @foreach ($data['data'] as $item)
                @if ($item->tipe == "image")
                <div class="gallery-item lazy_loading">
                    <a data-href="{{ ENV('DATA_URL').'/galeri/'.$item->nama_file }}" class="content">
                        <img src="{{ ENV('DATA_URL').'/galeri/'.$item->nama_file }}" class="hiddenfile" data-src="{{ ENV('DATA_URL').'/galeri/'.$item->nama_file }}" class="lazy" alt="{{ $item->nama_file }}">
                    </a>
                </div>
                @else
                    
                @endif
                @endforeach --}}
            </div>
            <div class="load-infinate" style="display: none">
                <img src="{{ asset('assets/img/loading.gif') }}" alt="">
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')