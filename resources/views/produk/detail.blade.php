    @include('layouts.header')
        <main id="DetailProduk" class="container-xxl">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb produk">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
                  <li class="breadcrumb-item"><a href="#">Produk</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Detail</li>
                </ol>
            </nav>
            <div class="row m-0">
                <div class="col-sm-12 col-md-5 col-12 ps-0 ps-md-3 pb-3 pb-md-0 pe-0 pe-md-3">
                    <div class="box-image">
                        <div class="display-image easyzoom--overlay easyzoom">
                            <a href="{{ ENV('DATA_URL').'/galeri/'.$data['image']['active'] }}">
                                <img src="{{ ENV('DATA_URL').'/galeri/'.$data['image']['active'] }}" class="image" alt="{{ $data['image']['active'] }}">
                            </a>
                        </div>
                        <div class="box-list-image">
                            <div class="list-image">
                                <?php 
                                $active = 0;
                                ?>
                                @foreach ($data['image']['list'] as $item)
                                <div class="item-image @if($item->nama_file == $data['image']['active'] && 0 == $active) active <?php $active = 1; ?> @endif"><img src="{{ ENV('DATA_URL').'/galeri/'.$item->nama_file }}" alt="{{ $item->nama_file }}"></div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7 col-12 ps-0 ps-md-3 pe-0 pe-md-3">
                    <div class="box-detail" id="box-detail">
                        <div class="detail-title">{{ $data['detail']->nama }}</div>
                        <div class="detail-review">
                            <?php
                            $star = $data['ulasan']['star'];
                            ?>
                            <span class="star">
                                @for ($i = 0; $i < $star; $i++)
                                <i class="fas fa-star"></i>
                                @endfor
                                @for ($i = 0; $i < (5-$star); $i++)
                                <i class="far fa-star"></i>
                                @endfor
                            </span>
                            <span class="review">({{$data['ulasan']['jml']}} customer review)</span>
                        </div>
                        <div class="detail-price">{{ number_format($data['harga']['active']->harga, 0, '.','.') }}</div>
                        <form id="SaveKeranjang">
                        @csrf
                        <div class="qty-produk">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="spin-input">
                                        <input type="text" class="form-control angka text-center" data-id="{{ $data['detail']->meta_url }}" data-toggle="qtyProduk" value="{{ $data['harga']['active']->qty }}" id="qty-val" data-panjang="{{ $data['detail']->panjang }}" data-minimal="{{ $data['harga']['active']->qty }}" data-minimal-meter="{{ ceil($data['harga']['active']->qty*$data['detail']->panjang) }}" name="qty-val" placeholder="Kuantitas">
                                        {{-- <button type="button" class="btn btn-plus" disabled data-id="{{ $data['detail']->meta_url }}" data-minimal="{{ $data['harga']['active']->qty }}" data-type="min" data-qty="1"><i class="fas fa-caret-down"></i></button> --}}
                                        {{-- <button type="button" class="btn btn-plus" data-id="{{ $data['detail']->meta_url }}" data-minimal="{{ $data['harga']['active']->qty }}" data-type="plus" data-qty="1"><i class="fas fa-caret-up"></i></button> --}}
                                        <select name="satuan_beli" id="satuan_beli" data-toggle="satuan_beli" data-id="{{ $data['detail']->meta_url }}" data-minimal="{{ $data['harga']['active']->qty }}" data-panjang="{{ $data['detail']->panjang }}" onfocus="this.size=2;" onblur="this.size=1;" onchange="this.size=1; this.blur();" class="form-select">
                                            <option value="pcs">Pcs</option>
                                            <option value="meter">Meter</option>
                                        </select>
                                    </div>
                                    <div class="subtotal" data-total="{{ $data['detail']->meta_url }}" data-id="{{ $data['detail']->meta_url }}">{{ number_format(($data['harga']['active']->harga * $data['harga']['active']->qty), 0, '.','.') }}</div>
                                    <input type="hidden" name="qty_pcs" class="qty_pcs" value="{{ $data['harga']['active']->qty }}" data-id="{{ $data['detail']->meta_url }}">
                                    <input type="hidden" name="subtotal" class="form-control" readonly value="{{ number_format(($data['harga']['active']->harga * $data['harga']['active']->qty), 0, '.','.') }}" id="subtotal" data-total="{{ $data['detail']->meta_url }}" data-id="{{ $data['detail']->meta_url }}">
                                </div>
                                <span class="text-danger" id="text-error" data-id="error-{{ $data['detail']->meta_url }}"></span>
                            </div>
                            {{-- <div class="form-group row mt-2"> --}}
                                {{-- <label for="qty-val" class="col-sm-2 col-form-label">Sub Total</label> --}}
                                {{-- <div class="col-sm-6 col-md-5 col-lg-4"> --}}
                                {{-- </div> --}}
                            {{-- </div> --}}
                            <div class="form-group row mt-2 mb-2">
                                <div class="col-12 col-sm-5 col-md-4 col-lg-4">
                                    <button type="button" id="BtnSaveKeranjang" class="btn btn-success btn-custom"><i class="fas fa-plus"></i> Beli Sekarang</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-12 ps-0 ps-md-3 pe-0 pe-md-3" id="boxDescProduk">
                    <div class="clearfix">
                        <div class="float-start"><h5>Deskripsi</h5></div>
                        <div class="float-end"><div data-expand="true" class="link_show_more" id="ShowDeskripsi">Lihat Semua</div></div>
                    </div>
                    <div class="desc-produk">
                        {{ $data['detail']->deskripsi }}
                    </div> 
                </div>
                <div class="col-sm-12 col-md-12 col-12 ps-0 ps-md-3 pe-0 pe-md-3" id="boxUlasan">
                    <div class="clearfix">
                        <div class="float-start"><h5>Ulasan</h5></div>
                        <div class="float-end">
                            {{-- <a href="#" data-expand="false" class="link_show_more" id="ShowMoreUlasan">Lihat Semua</a> --}}
                            <a href="{{ url('/ulasan') }}" class="link_show_more">Lihat Semua</a>
                        </div>
                    </div>
                    
                    <div class="list-ulasan" id="listulasan">
                        @foreach ($data['ulasan']['list'] as $item)
                        <div class="card card-body ps-0 ps-sm-3 pe-0 pe-sm-3">
                            <div class="row m-0">
                                <div class="col-lg-2 col-md-3 col-12 d-flex">
                                    <div class="icon-ulasan">
                                        <img src="{{ ENV('DATA_URL')."/profile/".$item->foto_profile }}" alt="user">
                                    </div>
                                    <div class="ulasan-name">
                                        <div>{{ $item->nama }}</div>
                                        <div class="star">
                                            <?php
                                            $star = $item->skor;
                                            ?>
                                            <span class="star">
                                                @for ($i = 0; $i < $star; $i++)
                                                <i class="fas fa-star"></i>
                                                @endfor
                                                @for ($i = 0; $i < (5-$star); $i++)
                                                <i class="far fa-star"></i>
                                                @endfor
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 pt-3 pt-md-0 col-12 desc">
                                    {{ $item->review }}
                                </div>
                            </div>
                        </div>                          
                        @endforeach

                    </div>
                </div>
            </div>
        </main>

        <div class="modal fade" id="ModalDesc" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Deskripsi</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row me-0 ms-0">
                        <div class="col-sm-12 col-12 col-md-4 ps-0">
                            <img src="{{ ENV('DATA_URL').'/galeri/'.$data['image']['active'] }}" alt="{{ ENV('DATA_URL').'/galeri/'.$data['image']['active'] }}" style="max-width: 100%; max-height:100%">
                        </div>
                        <div class="col-sm-12 col-12 col-md-8 ps-0 pt-3 pt-md-0">
                            <div class="detail-title">{{ $data['detail']->nama }}</div>
                            <div class="detail-review">
                                <?php
                                $star = $data['ulasan']['star'];
                                ?>
                                <span class="star">
                                    @for ($i = 0; $i < $star; $i++)
                                    <i class="fas fa-star"></i>
                                    @endfor
                                    @for ($i = 0; $i < (5-$star); $i++)
                                    <i class="far fa-star"></i>
                                    @endfor
                                </span>
                            </div>
                        </div>
                        <div class="col-12 desc ps-0">
                            {{ $data['detail']->deskripsi }}
                        </div>
                    </div>
                    
                </div>
              </div>
            </div>
          </div>

    @include('layouts.footer')