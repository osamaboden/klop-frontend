@include('layouts.header')
		<main id="page" class="container-xxl">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb produk">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
                  <li class="breadcrumb-item active">{{ $data['title'] }}</li>
                </ol>
            <div class="row me-0 ms-0 mt-3">
                <div class="col-md-3 d-none d-md-block">
                    <ul class="list-syarat">
                        <li><a href="{{ url('/syarat-dan-ketentuan') }}" @if($data['title'] == "Syarat dan Ketentuan") class="active" @endif>Syarat dan Ketentuan</a></li>
                        <li><a href="{{ url('/kebijakan-privasi') }}" @if($data['title'] == "Kebijakan Privasi") class="active" @endif>Kebijakan Privasi</a></li>
                        <li><a href="{{ url('/tentang-kami') }}" @if($data['title'] == "Tentang Kami") class="active" @endif>Tentang Kami</a></li>
                    </ul>
                </div>

                <div class="col-md-9 col-12 ps-0 ps-md-5">
                    <h1 class="page-title">{{ $data['title'] }}</h1>
                    <div class="desc-page">
                        {!! $data['value'] !!}
                    </div>
                </div>
            </div>
		</main>
@include('layouts.footer')