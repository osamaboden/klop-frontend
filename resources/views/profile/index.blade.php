@include('layouts.header')
<main id="profilePage" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active">Profil</li>
        </ol>
    </nav>

    <div class="row m-0">
        <div class="col-md-12 mx-auto pt-3 ps-0 pe-0">
            <ul class="nav nav-custom nav-tabs border-0" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="informasi-tab" data-bs-toggle="tab" data-bs-target="#informasi" type="button" role="tab" aria-controls="informasi" aria-selected="true">Informasi Umum</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="alamat-nav-tab" data-bs-toggle="tab" data-bs-target="#alamat-nav" type="button" role="tab" aria-controls="alamat-nav" aria-selected="false">Alamat</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="password-nav-tab" data-bs-toggle="tab" data-bs-target="#password-nav" type="button" role="tab" aria-controls="password-nav" aria-selected="false">Password</button>
                </li>
              </ul>
              <div class="tab-content tab-custom" id="myTabContent">
                <div class="tab-pane fade show active" id="informasi" role="tabpanel" aria-labelledby="informasi-tab">
                    <div class="row m-0">
                        <div class="col-md-3 col-sm-4 col-12 border-1-success box-profil">
                            <div class="image-profile"><img src="{{ ENV('DATA_URL')."/profile/".$data['biodata']->foto_profile }}" alt="image"></div>
                            <div class="input-file"><label for="gambar_file">pilih foto</label><input type="file" class="d-none" id="gambar_file" name="gambar_file" accept="image/jpeg, image/png"></div>
                            <div class="label">Besar file: maksimum 1 MB. Ekstensi file yang diperbolehkan: .JPG .JPEG .PNG</div>
                        </div>
                        <div class="col-md-9 col-sm-8 col-12">
                            <div class="biodata">
                                <div class="col-md-3 col-sm-4 col-5 label">Nama</div>
                                <div class="col-md-9 col-sm-8 col-7 value" data-label="nama">{{ $data['biodata']->nama }}</div>
                            </div>
                            <div class="biodata">
                                <div class="col-md-3 col-sm-4 col-5 label">Email</div>
                                <div class="col-md-9 col-sm-8 col-7 value" data-label="email">{{ $data['biodata']->email }}</div>
                            </div>
                            <div class="biodata">
                                <div class="col-md-3 col-sm-4 col-5 label">Nomor Telepon</div>
                                <div class="col-md-9 col-sm-8 col-7 value">{{ $data['biodata']->no_hp }}</div>
                            </div>
                            <div class="row m-0 pt-5">
                                <div class="col-12 text-end">
                                    <button data-bs-target="#modalBiodata" data-bs-toggle="modal" class="btn btn-success btn-custom border-0" style="width:unset">Edit Biodata</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="alamat-nav" role="tabpanel" aria-labelledby="alamat-nav-tab">
                    <div class="nav-title clearfix">
                        <span class="d-inline-block float-start">Daftar Alamat</span>
                        <div class="float-end"><a href="{{ url('/alamat') }}" class="btn btn-success btn-custom border-0"><i class="fas fa-plus-circle"></i>&nbsp;Tambah Alamat</a></div>
                    </div>

                    <div class="row m-0 listalamat">
                        @foreach ($data['alamat'] as $item)
                        <div class="col-12 box-alamat" data-alamat="alamat-{{ $item->id }}">
                            <div class="col-md-9 col-sm-12 col-12 alamat-detail">
                                <div class="label">{{ $item->nama }}</div>
                                <div class="address">{{ $item->alamat }}</div>
                            </div>
                            <div class="col-md-2 col-sm-12 col-12 alamat-button">
                                <a href="{{ url('/alamat/'.$item->id) }}" title="edit">
                                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.3275 7.74203L22.258 13.6697L16.3275 7.74203ZM20.1411 2.51994L4.10525 18.5558C3.27669 19.3832 2.7116 20.4374 2.48123 21.5855L1 29L8.41453 27.516C9.56255 27.2864 10.6154 26.7236 11.4442 25.8947L27.4801 9.85887C27.9619 9.37699 28.3442 8.80491 28.605 8.1753C28.8658 7.5457 29 6.87089 29 6.18941C29 5.50792 28.8658 4.83311 28.605 4.20351C28.3442 3.5739 27.9619 3.00182 27.4801 2.51994C26.9982 2.03806 26.4261 1.65581 25.7965 1.39502C25.1669 1.13423 24.4921 1 23.8106 1C23.1291 1 22.4543 1.13423 21.8247 1.39502C21.1951 1.65581 20.623 2.03806 20.1411 2.51994V2.51994Z" stroke="#57D024" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
                                </a>
                                <a data-id-alamat="{{ $item->id }}" data-toggle="delete-alamat" title="Hapus alamat ini">
                                    <svg width="30" height="35" viewBox="0 0 30 35" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 6.94189H28.897" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><path d="M4.1001 33.916H25.7977" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><path d="M4.1001 6.94189L4.1001 33.9159" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><path d="M25.7983 6.94189L25.7983 33.9159" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><path d="M11.8491 13.2881L11.8491 27.5684" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><path d="M18.0474 13.2881L18.0474 27.5684" stroke="#FE4B4B" stroke-width="2" stroke-linecap="round"/><mask id="path-7-inside-1" fill="white"><path d="M22.3098 7.93353C22.3098 5.82943 21.4934 3.8115 20.0402 2.32368C18.5869 0.835852 16.6159 1.58855e-07 14.5607 0C12.5055 -1.58855e-07 10.5344 0.835851 9.0812 2.32368C7.62795 3.8115 6.81152 5.82942 6.81152 7.93353L14.5607 7.93353L22.3098 7.93353Z"/></mask><path d="M22.3098 7.93353C22.3098 5.82943 21.4934 3.8115 20.0402 2.32368C18.5869 0.835852 16.6159 1.58855e-07 14.5607 0C12.5055 -1.58855e-07 10.5344 0.835851 9.0812 2.32368C7.62795 3.8115 6.81152 5.82942 6.81152 7.93353L14.5607 7.93353L22.3098 7.93353Z" stroke="#FE4B4B" stroke-width="4" mask="url(#path-7-inside-1)"/></svg>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane fade" id="password-nav" role="tabpanel" aria-labelledby="password-nav-tab">
                    <div class="nav-title"><span>Password</span></div>
                    <form id="resetPasswordForm">
                        @csrf
                        <div class="row me-0 ms-0 mt-3 mt-md-4">
                            <div class="form-box row">
                                <label for="old_password" class="col-sm-4 col-md-4 col-lg-3 col-12 col-form-label">Password Lama</label>
                                <div class="col-lg-4 col-md-5 col-sm-6 col-12">
                                <input type="password" name="old_password" class="form-control" id="old_password">
                                </div>
                            </div>
                            <div class="form-box row">
                                <label for="new_password" class="col-sm-4 col-md-4 col-lg-3 col-form-label">Password Baru</label>
                                <div class="col-lg-4 col-md-5 col-sm-6 col-12">
                                <input type="password" name="new_password" class="form-control" id="new_password">
                                </div>
                            </div>
                            <div class="form-box row">
                                <label for="repeatnew-password" class="col-sm-4 col-md-4 col-lg-3 col-form-label">Ulangi Password Baru</label>
                                <div class="col-lg-4 col-md-5 col-sm-6 col-12">
                                <input type="password" name="repeatnew_password" class="form-control" id="repeatnew_password">
                                </div>
                            </div>
                            <div class="col-12 text-end">
                                <button class="btn btn-success btn-custom border-0" type="button" id="BtnResetPassword" style="width:unset">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
        </div>
    </div>
</main>

<div class="modal fade" id="modalBiodata" tabindex="-1" aria-labelledby="labelModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelModal">Edit Biodata</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form id="FormBiodata">
            @csrf
        <div class="modal-body">
            <div class="mb-3">
                <label for="biodata_nama" class="form-label">Nama</label>
                <input type="text" class="form-control" value="{{ $data['biodata']->nama }}" required name="biodata_nama" id="biodata_nama" placeholder="Ketikan nama disini">
            </div> 
            <div class="mb-3">
                <label for="biodata_email" class="form-label">Email</label>
                <input type="email" class="form-control" value="{{ $data['biodata']->email }}" required name="biodata_email" id="biodata_email" placeholder="Ketikan email disini">
            </div>  
          <button type="button" id="btnSaveBiodata" class="btn btn-success btn-custom float-end mt-2" style="width: unset">Simpan Biodata</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="ModalImage" tabindex="-1" aria-labelledby="labelModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="labelModal">Edit Biodata</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="img-container ps-2 pe-2">
                <div class="row">
                    <div class="col-md-12 ps-0 pe-0">
                        <img id="imageCrop" src="{{ ENV('DATA_URL')."/profile/".$data['biodata']->foto_profile }}" alt="image-crop">
                    </div>
                </div>
            </div>
          <button type="button" class="btn btn-success btn-custom float-end mt-2" id="SaveCropImage" style="width: unset">Crop & Simpan</button>
        </div>
      </div>
    </div>
  </div>    

@include('layouts.footer')