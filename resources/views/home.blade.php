@include('layouts.header')
		<main class="body container-xxl">
			<div id="corousel">
				<div id="slideShow" class="carousel slide" data-bs-ride="carousel">
				  <div class="carousel-indicators">
					@foreach ($data['slideshow'] as $key => $item)
					<button type="button" data-bs-target="#slideShow" data-bs-slide-to="{{ $key }}" class="@if($key == 0) active @endif" aria-current="true" aria-label="Slide {{ $key+1 }}"></button>
					@endforeach
				  </div>
				  <div class="carousel-inner">
					@foreach ($data['slideshow'] as $key => $item)
					<div style="background-image: url({{ ENV('DATA_URL').'/corousel/'.$item->gambar }})" class="carousel-item @if($key == 0) active @endif" >
					  {{-- <img src="{{ ENV('DATA_URL').'/corousel/29W9RscuUMw.png' }}" class="d-block w-100" alt="{{ ENV('DATA_URL').'/corousel/29W9RscuUMw.png' }}"> --}}
					  <div class="carousel-caption title-corousel">
						<div>Selamat Datang Di Toko Kami</div>
					  </div>
					  <div class="carousel-caption btn-corousel">
						<a href="#tentangkami" type="button" class="btn">Tentang Kami</a>
					  </div>
					</div>
					@endforeach
				  </div>
				</div>
			</div>
			@if(!empty($data['galeri']['gambar']))
			<div id="galeri-foto">
				<div class="row m-0" id="tentangkami">
					<div class="col-lg-7 col-md-12 col-sm-12 col-12 row m-0">
						@foreach ($data['galeri']['gambar'] as $key => $item)
						<div class="foto " style="background-image: url({{ ENV('DATA_URL')."/galeri/".$item->nama_file }})">
							@if($key == 3) <a href="{{ url('/gallery/gambar') }}" class="w-100"><div class="more">Lihat Lebih</div></a> @endif
						</div>
						@endforeach
					</div>
					<div class="col-lg-5 col-md-12 col-sm-12 col-12 welcome-box">
						<div class="title">Selamat Datang Di Toko Kami</div>
						<div class="desc">{!! $data['selamat_datang'] !!}</div>
						<a href="{{ url('/tentang-kami') }}" class="btn btn-success btn-custom btn-padding" style="width: unset">Lihat Lebih</a>
					</div>
				</div>
			</div>
			@endif
			@if (!empty($data['banner']['web'][1]))
			<div class="banner-image d-sm-block d-none">
				<a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}"><img src="{{ ENV('DATA_URL').'/banner/'.$data['banner']['web'][1] }}" style="max-width:100%;max-height:100%" alt="banner"></a>
			</div>	
			{{-- @else --}}
			{{-- <div class="banner-beli paralax" style="background-image: url({{ asset('assets/img/banner.jpg') }})">
				<div class="paralax-content">
					<div class="title">Disini Kamu Bisa Membeli Produk</div>
					<div class="brand">KLOP</div>
					<div class="button"><a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}" class="btn btn-custom outline">Beli Sekarang</a></div>
				</div>
			</div> --}}
			@endif
			@if (!empty($data['banner']['mobile'][1]))
			<div class="banner-image d-sm-none d-block">
				<a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}"><img src="{{ ENV('DATA_URL').'/banner/'.$data['banner']['mobile'][1] }}" style="max-width:100%;max-height:100%" alt="banner"></a>
			</div>	
			@endif
			@if(!empty($data['galeri']['video']))
			<div class="container-gallery">
				<div class="title-gallery gallery-video">
					<span>galeri video</span>
					<a href="{{ url('/gallery/video') }}">Lihat Semua</a>
				</div>
				<div class="row me-0 ms-0" id="galleryVideo">
					@foreach($data['galeri']['video'] as $key => $value)
					<div class="col-12 pe-0 col-sm-6 col-md-4" >
						<div class="box-gallery">
							<div class="show-detail gallery-video">
								<a data-fancybox="gallery" data-caption="<?php echo $value->title;?>" class="fancybox-media" href="<?php echo $value->nama_file;?>">
									<svg width="94" height="93" viewBox="0 0 94 93" fill="none" xmlns="http://www.w3.org/2000/svg">
										<circle cx="46.9864" cy="46.4322" r="45.3589" stroke="white" stroke-width="2"/>
										<path d="M76.2734 44.2269C78.2734 45.3816 78.2734 48.2684 76.2734 49.4231L35.1822 73.1471C33.1822 74.3018 30.6822 72.8584 30.6822 70.549L30.6822 23.101C30.6822 20.7916 33.1822 19.3482 35.1822 20.5029L76.2734 44.2269Z" fill="white"/>
									</svg>	
								</a>
							</div>
							<img src="{{ $value->url_gambar }}" class="image" alt="image1">
						</div>
					</div>
					@endforeach
				</div>
			</div>
			@endif

			@if (!empty($data['banner']['web'][2]))
			<div class="banner-image d-sm-block d-none">
				<a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}"><img src="{{ ENV('DATA_URL').'/banner/'.$data['banner']['web'][2] }}" style="max-width:100%;max-height:100%" alt="banner"></a>
			</div>	
			{{-- @else --}}
			{{-- <div class="banner-long paralax" style="background-image: url({{ asset('assets/img/banner2.jpg') }})">
				<div class="paralax-content">
					<div class="title">Batu bata klop</div>
					<div class="brand">ringan dan tahan lama</div>
					<div class="button"><a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}" class="btn btn-success btn-custom" style="width: unset">Beli Sekarang</a></div>
				</div>
			</div> --}}
			@endif
			@if (!empty($data['banner']['mobile'][2]))
			<div class="banner-image d-sm-none d-block">
				<a href="{{url('/produk/detail/'.(new \App\Helpers\helper)->getSetting('produk_meta'))}}"><img src="{{ ENV('DATA_URL').'/banner/'.$data['banner']['mobile'][2] }}" style="max-width:100%;max-height:100%" alt="banner"></a>
			</div>	
			@endif
			@if(!empty($data['galeri']['instalasi']['short']) && !empty($data['galeri']['instalasi']['long']))
			<div class="container-gallery">
				<div class="title-gallery">
					<span>galeri instalasi</span>
					<a href="{{ url('/gallery/instalasi') }}">Lihat Semua</a>
				</div>
				<div class="row ms-0 me-0 gallery-instalasi">
					<div class="col-md-4 col-sm-6 col-6 d-none d-sm-block">
						<div class="box-gallery" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['short'][0]['nama_file'] }})">
						</div>
						<div class="mb-3"></div>
						<div class="box-gallery" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['short'][1]['nama_file'] }})">
						</div>
					</div>
					<div class="col-md-4 d-none d-md-block">
						@if (count($data['galeri']['instalasi']['long']) > 0)
						<?php $key = 2; ?>
						<div class="box-gallery full" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['long']['nama_file'] }})">
						</div>
						@else
						<?php $key = 3; ?>
						<div class="box-gallery full" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['short'][2]['nama_file'] }})">
						</div>
						@endif
					</div>
					<div class="col-md-4 col-sm-6 col-6 d-none d-sm-block">
						<div class="box-gallery" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['short'][$key]['nama_file'] }})">
							<?php $key++; ?>
						</div>
						<div class="mb-3"></div>
						<div class="box-gallery" style="background-image: url({{ ENV('DATA_URL')."/galeri/".$data['galeri']['instalasi']['short'][$key]['nama_file'] }})">
						</div>
					</div>
					<div class="col-12 pe-0 ps-0 row ms-0 me-0 owl-carousel owl-theme" id="galleryInstalasi">
						@foreach($data['galeri']['instalasi']['short'] as $key => $value)
						<div class="col-12 pe-0" >
							<div class="box-gallery">
								<img src="{{ ENV('DATA_URL')."/galeri/".$value['nama_file'] }}" class="w-100 h-auto" alt="image1">
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			@endif
			@if(!empty($data['review']))
			<div class="container-gallery list-testimoni">
				<div class="title-gallery">
					<span>Testimoni</span>
					<a href="{{ url('/ulasan') }}">Lihat Semua</a>
				</div>

				<div id="testimoniList" class="carousel slide" data-bs-ride="carousel">
					<div class="carousel-inner">
						@foreach ($data['review'] as $key => $item)
						<div class="carousel-item @if($key == 0) active @endif ">
							<div class="testimoni-header">
								<img src="{{ ENV('DATA_URL').'/profile/'.$item->foto_profile }}" class="icon" alt="img-profile">
								<span class="name">{{ $item->nama }}</span>
								<span class="star">
									<?php 
									$star = $item->skor;
									$rstar = 5 - $item->skor;
									?>
									@for ($i = 0; $i < $star; $i++)
									<i class="fas fa-star"></i>
									@endfor
									@for ($i = 0; $i < $rstar; $i++)
									<i class="far fa-star"></i>
									@endfor
								</span>
							</div>
							<div class="testimoni-body">
								{{ $item->review }}	
							</div>
						</div>
						@endforeach
					</div>
					<button class="carousel-control-prev" type="button" data-bs-target="#testimoniList" data-bs-slide="prev">
					  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					  <span class="visually-hidden">Previous</span>
					</button>
					<button class="carousel-control-next" type="button" data-bs-target="#testimoniList" data-bs-slide="next">
					  <span class="carousel-control-next-icon" aria-hidden="true"></span>
					  <span class="visually-hidden">Next</span>
					</button>
				  </div>
			</div>
			@endif
		</main>
@include('layouts.footer')