@include('layouts.header')
	
	<main>
        <a href="{{ url('/') }}" class="back-arrow">    
			<svg width="32" height="26" viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M13.3333 0L15.24 1.8109L5.13333 11.7H32V14.3H5.13333L15.24 24.1449L13.3333 26L0 13L13.3333 0Z" fill="#222222"/>
			</svg>
        </a>
		<div class="row me-0 ms-0 h-100">
			<div class="col-lg-4 col-md-4 col-sm-4 col-12 m-auto">
				<form id="LoginForm">
				@csrf
				<div class="card w-100">
					<div class="card-body">
						<div class="card-brand">
							<img src="{{ asset('assets/img/logo_panjang.png') }}" alt="logo">
							<div class="card-brand-text">Yuk, Login dulu</div> 
						</div>
						<div class="form-group">
							<input type="text" name="username" id="username" required class="form-control form-control-custom" placeholder="Email/Nomor Telepon">
							<span class="text-danger pb-2" id="username_validate"></span>
						</div>
						<div class="form-group">
							<input type="password" id="password" name="password" required class="form-control form-control-custom" placeholder="Password">
							<span class="show-password"></span>
							<span class="text-danger pb-2" id="password_validate"></span>
						</div>
						<div class="clearfix remember-box">
							{{-- <div class="float-start">
								<div class="form-check">
									<input type="checkbox" class="form-check-input" name="remember_me" id="remember_me">
									<label class="form-check-label" for="remember_me">Ingat Saya</label>
								</div>
							</div> --}}
							<div class="float-end"><a href="{{ url('/forget') }}">Lupa Password?</a></div>
						</div>
						<div class="d-grid gap-2">
							<button type="button" id="LoginButton" class="btn btn-success btn-custom-modal">Masuk</button>
							<div class="forget-pass">Belum punya akun? <a href="{{ url('/register') }}">Daftar di sini</a></div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>		
	</main>

@include('layouts.footer')