@include('layouts.header')
	
	<main>
        <a href="{{ url('/login') }}" class="back-arrow">
			<svg width="32" height="26" viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M13.3333 0L15.24 1.8109L5.13333 11.7H32V14.3H5.13333L15.24 24.1449L13.3333 26L0 13L13.3333 0Z" fill="#222222"/>
			</svg>
        </a>
		<div class="row me-0 ms-0 h-100">
			<div class="col-lg-4 col-md-4 col-sm-4 col-12 m-auto">
				<form id="ForgotPasswordForm">
				@csrf
				<div class="card w-100">
					<div class="card-body">
						<div class="card-brand">
							<img src="{{ asset('assets/img/logo_panjang.png') }}" alt="logo">
                            <div class="card-brand-text">Lupa Password</div> 
						</div>
                        <div class="card-desc text-center">Silakan masukkan alamat email Anda<br>
							Anda akan menerima tautan untuk membuat<br>
							kata sandi baru melalui email.</div>
                        <div class="form-group">
                            <input type="email" name="email_password" id="email_password" class="form-control form-control-custom" placeholder="Email">
                            <span class="text-danger" id="email_password_validate"></span>
                        </div>
						<div class="d-grid gap-2">
							<button type="button" id="ResetButton" class="btn btn-success btn-custom-modal">Reset Password</button>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</main>

@include('layouts.footer')