@include('layouts.header')
	
	<main>
        <a href="{{ url('/login') }}" class="back-arrow">
			<svg width="32" height="26" viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M13.3333 0L15.24 1.8109L5.13333 11.7H32V14.3H5.13333L15.24 24.1449L13.3333 26L0 13L13.3333 0Z" fill="#222222"/>
			</svg>
        </a>
		<div class="row me-0 ms-0 h-100">
			<div class="col-lg-4 col-md-4 col-sm-4 col-12 m-auto">
				<form type="POST" id="RegisterForm">
				@csrf
				<div class="card w-100">
					<div class="card-body">
						<div class="card-brand">
							<img src="{{ asset('assets/img/logo_panjang.png') }}" alt="logo">
                            <div class="card-brand-text">Yuk, Daftar dulu</div> 
						</div>
                        <div class="form-group">
                            <input type="text" name="name_register" id="name_register" class="form-control form-control-custom" placeholder="Nama Lengkap" required>
                        <span class="text-danger pb-2" id="name_register_validate"></span>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email_register" id="email_register" class="form-control form-control-custom" placeholder="Email" required>
                        <span class="text-danger pb-2" id="email_register_validate"></span>
                        </div>
                        <div class="form-group">
                            <input type="text" name="telepon_register" id="telepon_register" class="form-control telepon form-control-custom" placeholder="Nomor Telepon ex. 08123..." required>
                        <span class="text-danger pb-2" id="telepon_register_validate"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_register" id="password_register" class="form-control form-control-custom" placeholder="Password" required>
                            <span class="show-password"></span>
                            <span class="text-danger pb-2" id="password_register_validate"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" id="password_repeat" name="password_repeat" class="form-control form-control-custom" placeholder="Ulangi Password" required>
                            <span class="show-password"></span>
                            <span class="text-danger pb-2" id="password_repeat_validate"></span>
                        </div>
						<div class="d-grid gap-2">
							<button type="button" id="DaftarButton" class="btn btn-success btn-custom-modal">Daftar</button>
							<div class="forget-pass mb-4 text-center">Sudah punya akun? <a href="{{ url('/login') }}">Login</a></div>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</main>

@include('layouts.footer')