{{-- @include('layouts.header')
	
	<main id="forgetPassword">
		<div class="w-100 p-4 text-center">
			<img src="{{ asset('assets/img/logo.png') }}" alt="logo" class="logo-brand">
			<div class="title-forgot">Reset Password</div>
		</div>
		<div class="row m-0">
			<div class="col-md-3 col-sm-4 col-12 box-forget">
				<form id="ResetPassword">
				@csrf
				<div class="card w-100">
					<div class="card-body p-4">
						<div class="form-group">
							<label >Password</label>
							<input type="password" class="form-control mt-2 mb-2" id="password" name="password">
							<span class="text-danger d-inline-block mb-1" id="password_validate"></span>
						</div>
						<div class="form-group">
							<label>Ulangi Password</label>
							<input type="password" class="form-control mt-2 mb-2" id="password_repeat" name="password_repeat">
							<span class="text-danger d-inline-block mb-1" id="password_repeat_validate"></span>
						</div>
					</div>
					<div class="card-footer">
						<div class="d-grid gap-2">
							<button type="button" id="BtnReset" class="btn btn-success btn-custom">RESET PASSWORD</button>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</main>

@include('layouts.footer') --}}


@include('layouts.header')
	
	<main>
        <a href="{{ url('/') }}" class="back-arrow">
			<svg width="32" height="26" viewBox="0 0 32 26" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M13.3333 0L15.24 1.8109L5.13333 11.7H32V14.3H5.13333L15.24 24.1449L13.3333 26L0 13L13.3333 0Z" fill="#222222"/>
			</svg>
        </a>
		<div class="row me-0 ms-0  h-100">
			<div class="col-lg-4 col-md-4 col-sm-4 col-12 m-auto">
				<form id="ResetPassword">
				@csrf
				<div class="card w-100">
					<div class="card-body">
						<div class="card-brand">
							<img src="{{ asset('assets/img/logo_panjang.png') }}" alt="logo">
                            <div class="card-brand-text">Ubah Password</div> 
						</div>
						<div class="form-group">
							<input type="password" placeholder="Password" class="form-control mt-2 mb-2" id="password" name="password">
							<span class="show-password"></span>
							<span class="text-danger d-inline-block mb-1" id="password_validate"></span>
						</div>
						<div class="form-group">
							<input type="password" class="form-control mt-2 mb-2" placeholder="Ulangi Password" id="password_repeat" name="password_repeat">
							<span class="show-password"></span>
							<span class="text-danger d-inline-block mb-1" id="password_repeat_validate"></span>
						</div>
						<div class="d-grid gap-2">
							<button type="button" id="BtnReset" class="btn btn-success btn-custom-modal">Reset Password</button>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>
	</main>

@include('layouts.footer')