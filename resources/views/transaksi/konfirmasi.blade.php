@include('layouts.header')
<main id="KonfirmasiBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/transaksi">Transaksi Saya</a></li>
          <li class="breadcrumb-item active">Konfirmasi Pembayaran</li>
        </ol>
    </nav>
    <div class="row m-0">
        <div class="col-md-8 col-sm-11 col-12 mx-auto ps-0 pl-0">
            <form id="KonfirmasiForm">
            @csrf
            <div class="card border-gray">
                <div class="card-body">
                    <p>Terima kasih sudah berbelanja di KLOP!</p>
                    <p>Konfirmasi Pembayaran anda disini agar dapat kami proses segera. </p>
                    <div class="row m-0 form">
                        <div class="col-12">
                            <label for="nomorOrder" class="form-label">ID Transaksi *</label>
                            <input type="text" readonly class="form-control border-radius-0" id="nomorOrder" @if($data['is_success']) value="{{ $data['id_order'] }}" @endif name="nomorOrder">
                        </div>
                        <div class="col-12">
                            <label for="nominalTransfer" class="form-label">Nominal Transfer *</label>
                            <input type="text" readonly class="form-control border-radius-0 text-end money" @if($data['is_success']) value="{{ $data['total'] }}" @endif id="nominalTransfer" name="nominalTransfer">
                        </div>
                        <div class="col-12 row m-0">
                            <div class="col-12 col-sm-4 col-md-4 ps-0 pe-0 pe-sm-2">
                                <label for="bankTransfer" class="form-label">Bank Anda *</label>
                                <input type="text" class="form-control border-radius-0" id="bankTransfer" name="bankTransfer">
                            </div>
                            <div class="col-12 col-sm-8 col-md-8 pe-0 ps-0 ps-sm-2">
                                <label for="NamaTransfer" class="form-label">Rekening Atas Nama *</label>
                                <input type="text" class="form-control border-radius-0" id="NamaTransfer" name="NamaTransfer">
                            </div>
                        </div>
                        <div class="col-12">
                                <label for="fileimage" class="form-label">Bukti Transfer *</label>
                                <div class="file-custom">
                                    <div class="file-label border-radius-0">Max. ukuran file 2mb, format file .jpg atau .png</div>
                                    <div class="file-button"><label for="fileimage">Pilih File</label></div>
                                </div>
                            <input type="file" name="fileimage" id="fileimage" class="hiddenfile" accept="image/jpeg, image/png">
                        </div>
    
                        <label class="form-label">* Wajib Diisi</label>
    
                        <div class="col-12 mt-4 text-center">
                            <button type="submit" id="BtnSaveKonfirmasi" style="width: unset" class="btn btn-custom btn-success btn-large">KONFIRMASI</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</main>



@include('layouts.footer')