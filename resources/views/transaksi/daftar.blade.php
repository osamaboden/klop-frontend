@include('layouts.header')
<main id="ListTransaksiBox" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active">Transaksi Saya</li>
        </ol>
    </nav>

    <div class="row m-0">
        <div class="col-md-11 col-12 ps-0 pe-0 mx-auto pt-3">
            <div class="row m-0">
                <div class="col-12 mb-2 border-bottom mt-3 pe-0 ps-0">
                    <div class="row mb-2">
                        {{-- <div class="col-md-4 col-sm-6 col-12 position-relative">
                            <input type="text" class="form-control form-icon search-transaksi" name="searchtransaksi" placeholder="Cari...">
                            <span class="form-icon pointer">
                                <i class="fas fa-search"></i>
                            </span>
                        </div> --}}
                        <div class="col-md-4 col-sm-6 col-12 mt-2 mt-sm-0 position-relative">
                            <select name="statusTransaksi" id="statusTransaksi" class="form-control" style="width:100%" data-placeholder="Status Transaki">
                                <option value="all">Semua Transaksi</option>
                                <option value="menunggu pembayaran">Menunggu Pembayaran</option>
                                <option value="menunggu konfirmasi">Menunggu Konfirmasi</option>
                                <option value="diproses">Diproses</option>
                                <option value="dikirim">Dikirim</option>
                                <option value="sampai ditujuan">Sampai Ditujuan</option>
                                <option value="instalasi">Instalasi</option>
                                <option value="komplain">Komplain</option>
                                <option value="dibatalkan">Dibatalkan</option>
                                <option value="selesai">Selesai</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-3 pe-0 ps-0 list-transaksi">
                    @foreach ($data['list'] as $item)
                    <div class="box-transaksi">
                        <div class="image-transaksi">
                            <img src="{{ ENV('DATA_URL')."/galeri/2Hzmz15wGik.png" }}" alt="">
                        </div>
                        <div class="detail-transaksi">
                            <div class="col-5">
                                <div class="label">ID transaksi</div>
                                <div class="value">{{ $item->id }}</div>
                            </div>
                            <div class="col-7">
                                <div class="label">Status</div>
                                <div class="value">{!! ucwords($item->status_order) !!} <img src="{{ asset('assets/img/'.str_replace(' ', '-', $item->status_order).'.svg') }}" alt=""></div>
                            </div>
                            <div class="col-5">
                                <div class="label">Waktu Order</div>
                                <div class="value">{{ (new \App\Helpers\helper)->convertdateToDateIndo($item->waktu_order)." ".$item->time_order }}</div>
                            </div>
                            <div class="col-7">
                                <div class="label">total harga (rp)</div>
                                <div class="value">{!! number_format($item->harga, 0, '.','.') !!}</div>
                            </div>
                        </div>
                        <div class="button-transaksi">
                            <a href="{{ url('/transaksi/detail/'.$item->id) }}" class="icon-detail"><i class="fas fa-angle-right"></i></a>
                        </div>
                    </div>
                    @endforeach
                    
                    @if ($data['total'] == 0)
                    <div class="card-body text-center flex-center">
                        <div class="keranjang-blank">
                            <div><img src="{{ asset('/assets/img/keranjang_times.svg')}}" style="width: 50%" alt="keranjang_times">
                            </div>
                            <div class="mt-3">
                                <span>Anda belum memiliki transaksi</span>
                            </div>
                            <button class="btn btn-custom btn-success mt-3 pe-4 ps-4" style="width:unset;">Kembali Ke Beranda</button>
                        </div>
                    </div>
                    @endif

                    <div class="col-12 clearfix mt-3 paging">
                        @if($data['prev'] > 0)
                        <div class="float-start"><a href="#" data-page="{{ $data['prev'] }}" class="active paging" ><i class="fas fa-chevron-left"></i> Sebelumnya</a></div>
                        @endif
                        @if($data['next'] > 0)
                        <div class="float-end"><a href="#" data-page="{{ $data['next'] }}" class="active paging">Selanjutnya <i class="fas fa-chevron-right"></i></a></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@include('layouts.footer')