@include('layouts.header')
<main id="DetailTransaksi" class="container-xxl">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb produk">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item"><a href="/transaksi">Transaksi Saya</a></li>
          <li class="breadcrumb-item active">Detail Transaksi</li>
        </ol>
    </nav>
    <div class="row m-0 box-detail">
        <div class="col-md-10 col-sm-12 col-12 ps-0 pe-0 mx-auto">
            <div class="card border-0">
                <div class="card-body">
                    <div class="title">Detail Transaksi</div>
                    <div class="row me-0 ms-0 mb-5">
                        <div class="col-lg-4 col-md-4 col-sm-6 ps-0">
                            <div class="label">ID Transaksi</div>
                            <div class="nilai">{{ $data['data']->id }}</div>
                            <div class="label">Status Pesanan</div>
                            <div class="nilai">{{ $data['status'] }} <img src="{{ asset('assets/img/'.str_replace(' ', '-',$data['status']).'.svg') }}" alt="svg"></div>
                            <div class="label">Status Pembayaran</div>
                            <div class="nilai">{{ $data['status_pembayaran'] }}</div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 ps-0">
                            <div class="label">Waktu Order</div>
                            <div class="nilai">{{ $data['data']->waktu_order }}</div>
                            @if ($data['data']->delivery == "tidak")
                            <div class="label">Tanggal Pengambilan</div>
                            <div class="nilai">{{ $data['data']->request_ready }}</div>
                            @else
                            <div class="label">Tanggal Pengiriman(perkiraan)</div>
                            <div class="nilai">{{ $data['data']->request_ready }}</div>
                            @endif
                            @if ($data['data']->proyek != "")
                            <div class="label">Proyek</div>
                            <div class="nilai">{{ $data['data']->proyek }}</div> 
                            @endif
                        </div>
                        <div class="col-lg-5 col-md-4 col-sm-12 ps-0">
                            <div class="box-alamat">
                                @if ($data['data']->delivery == "tidak")
                                <div class="nama_pemesan">Alamat Pengambilan</div>
                                <div class="alamat">{{ $data['alamat'] }}</div>
                                <div class="nomor">{{ (new \App\Helpers\helper)->getSetting('nomor_telepon') }}</div>
                                @else
                                <div class="nama_pemesan">Alamat Pengiriman</div>
                                <div class="alamat">{{ $data['alamat'] }}</div>
                                <div class="nomor">{{ $data['data']->nomor_telepon }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="list-detail">
                        <table class="list-detail mb-md-5 mb-4">
                            <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Kuantitas</th>
                                    <th>Subtotal(Rp)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['detail'] as $item)
                                <?php
                                $qty = ($item->satuan_beli == 'meter') ? $item->qty_meter : $item->qty;
                                $satuan = ($item->satuan_beli == "meter") ? 'm' : 'pcs';
                                ?>
                                <td>{{ $item->nama }}</td>
                                <td class="text-center">{{ number_format($qty, 0, '.','.').$satuan }}</td>
                                <td class="text-end">{{ number_format($item->harga_total, 0, '.','.') }}</td>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix me-0 ms-0 mb-3">
                            <div class="info-summary">
                                <table class="list-detail total">
                                    <thead>
                                        <tr>
                                            <th colspan="2" class="text-center">Total Pembayaran(Rp)</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Biaya Pemasangan</td>
                                            <td class="text-end">{{ number_format($data['data']->nilai_installation, 0, '.','.') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Biaya Pengiriman</td>
                                            <td class="text-end">{{ number_format($data['data']->nilai_delivery, 0, '.','.') }}</td>
                                        </tr>
                                        <tr class="total">
                                            <td>Total Pembayaran</td>
                                            <td class="text-end">{{ number_format($data['total'], 0, '.','.') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if ($data['status'] == "menunggu pembayaran" || $data['status'] == "baru")
                            <div class="info-rekening">
                                <div class="box-rekening">
                                    <div class="bank"><img src="{{ ENV('DATA_URL').'/master_data/'.(new \App\Helpers\helper)->getSetting('logo_bank') }}"></div>
                                    <div class="mb-2">Bank &nbsp;{{ (new \App\Helpers\helper)->getSetting('nama_bank') }}  </div>
                                    <div class="mb-2"><span>Atas Nama</span>{{ (new \App\Helpers\helper)->getSetting('nama_rekening') }}  </div>
                                    <div id="NomorRekening"><span>Rekening  </span>{{ (new \App\Helpers\helper)->getSetting('nomor_rekening') }}
                                        <span data-copy-target="NomorRekening" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Copy to clipboard">Salin </span></div>
                                    <div></div>
                                </div>
                            </div>
                            @elseif(!is_null($data['review']))
                            <div class="info-rekening">
                                <div class="review-box">
                                    <div class="label">Review Anda</div>
                                    <div class="star mb-2">
                                        <?php $star = $data['review']->skor; 
                                                $rstar = 5 - $star; ?>
                                            @for ($i = 0; $i < $star; $i++)
                                                <i class="fas fa-star"></i>
                                            @endfor
                                            @for ($i = 0; $i < $rstar; $i++)
                                                <i class="far fa-star"></i>
                                            @endfor
                                    </div>
                                    <div class="status">
                                        {{ $data['review']->review }}
                                    </div>
                                </div>        
                            </div>
                            @endif
                        </div>
                        {{-- @if ($data['status'] == "menunggu pembayaran" || $data['status'] == "baru")
                        <div class="text-center text-konfirmasi">Konfirmasi Pembayaran</div>
                        <div class="text-center button-konfirmasi"><a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" class="btn btn-success btn-custom"><img src="{{ asset('assets/img/whatsapp_button.svg') }}" alt="img"> Melalui Whatsapp</a> <a href="{{ url('/konfirmasi?id_order='.$data['data']->id) }}" class="btn btn-success btn-custom"><img src="{{ asset('assets/img/konfirmasi_button.svg') }}" alt="img"> Melalui Web</a></div>
                        @endif --}}
                    </div>
                    <div class="row me-0 ms-0 justify-content-center">
                        @if ($data['status'] == "menunggu pembayaran" || $data['status'] == "baru")
                            <div class="col-12 col-sm-6 col-md-4 pe-0 ps-0">
                                <button data-toggle="batalkanPesanan" data-id="{{ $data['data']->id }}" class="btn btn-custom btn-danger w-100">Batalkan Transaksi</button>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 ps-sm-2 ps-0 pe-0 pt-sm-0 pt-3">
                                <div class="dropdown">
                                    <button class="btn btn-success btn-large btn-custom dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Konfirmasi Pembayaran
                                    </button>
                                    <ul style="width: 100%" class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                      <li><a href="{{ url('/konfirmasi?id_order='.$data['data']->id) }}" class="dropdown-item"><img src="{{ asset('assets/img/konfirmasi_button.svg') }}" alt="img" class="me-2">Melalui Website</a></li>
                                      <li><a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" target="_blank" class="dropdown-item"><img src="{{ asset('assets/img/whatsapp_button.svg') }}" style="width: 26px" alt="img" class="me-2">Melalui Whatsapp</a></li>
                                    </ul>
                                </div>
                            </div>
                        @elseif($data['status'] == "menunggu konfirmasi" || $data['status'] == "diproses" || $data['status'] == "dikirim")
                            <div class="col-12 col-md-5 mx-auto pe-0 ps-0 pt-2">
                                <a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" target="_blank" class="btn btn-custom btn-success w-100"><i class="fab fa-whatsapp"></i> Hubungi Kami</a>
                            </div>
                        @elseif($data['status'] == "selesai" && is_null($data['review']))
                        <div class="col-12 col-md-5 mx-auto pe-0 ps-0 pt-2">
                            <button data-bs-toggle="modal" data-bs-target="#ModalReview" class="btn btn-custom btn-success w-100"><i class="far fa-comment"></i> Tulis Review</button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@if($data['status'] == "selesai" && is_null($data['review']))
<div class="modal fade" id="ModalReview" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Review</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form id="FormReview">
            @csrf
        <div class="modal-body">
          <div class="form-star">
                <label class="form-label">Rating</label>
                <div class='rating-stars text-start'>
                    <ul id='stars'>
                    <li class='list-star' title='Buruk Sekali' data-value='1'>
                        <i class='fas fa-star fa-fw'></i>
                    </li>
                    <li class='list-star' title='Buruk' data-value='2'>
                        <i class='fas fa-star fa-fw'></i>
                    </li>
                    <li class='list-star' title='Cukup' data-value='3'>
                        <i class='fas fa-star fa-fw'></i>
                    </li>
                    <li class='list-star' title='Baik' data-value='4'>
                        <i class='fas fa-star fa-fw'></i>
                    </li>
                    <li class='list-star' title='Baik Sekali' data-value='5'>
                        <i class='fas fa-star fa-fw'></i>
                    </li>
                    <li class="star-message"></li>
                    </ul>
                </div>
                <input type="text" class="hiddenfile" name="startvalue" id="startvalue">
          </div>
          <div class="form-group mt-3">
            <label for="reviewValue" class="form-label">Review Anda</label>
            <textarea name="reviewValue" id="reviewValue" rows="4" class="form-control"></textarea>
          </div>
          <input type="hidden" name="idReview" id="idReview" value="0">
          <button class="btn btn-success btn-custom mt-3 float-end" style="width: unset">Simpan Review</button>
        </div>
        </form>
      </div>
    </div>
  </div>
@endif


@include('layouts.footer')