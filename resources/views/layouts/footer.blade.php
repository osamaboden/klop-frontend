@if((new \App\Helpers\helper)->getRoute() != "forgot" && (new \App\Helpers\helper)->getRoute() != "login" && (new \App\Helpers\helper)->getRoute() != "page")
<footer>
	<div class="container-xxl">
		<div class="row m-0">
			<div class="col-12 p-0">
				<div class="row m-0 align-items-center">
					<div class="col-md-2 text-center box-logo-foot d-none d-md-block">
						<a href="/">
							<img src="{{ asset('assets/img/logo.png') }}" class="logo-foot" alt="logo">
							<span class="logo-text">KLOP</span>
						</a>
					</div>
					<div class="col-md-10 col-12 list-footer row m-0">
						<div class="col-md-3 col-sm-4 col-5 col-custom-12">
							<span class="footer-title">Informasi</span>
							<ul class="foot-menu">
								<li><a href="{{ url('/syarat-dan-ketentuan') }}">Syarat dan Ketentuan</a></li>
								<li><a href="{{ url('/kebijakan-privasi') }}">Kebijakan Privasi</a></li>
								<li><a href="{{ url('/tentang-kami') }}">Tentang Kami</a></li>
								<li><a href="https://wa.me/{{ (new \App\Helpers\helper)->getSetting('whatsapp') }}" target="_blank">Bantuan</a></li>
							</ul>
						</div>
						<div class="col-md-5 col-sm-8 col-7 col-custom-12">
							<span class="footer-title">Informasi Toko</span>
							<ul class="foot-menu fa-ul">
								<?php
								$alamat = (new \App\Helpers\helper)->getSetting('alamat');
								$nomor_telepon = (new \App\Helpers\helper)->getSetting('nomor_telepon');
								$email = (new \App\Helpers\helper)->getSetting('email');
								?>
								@if ($alamat)
								<li><a href="https://www.google.com/maps/search/?api=1&query={{ $alamat }}" target="_blank"><span class="fa-li"><i class="fas fa-map-marker-alt"></i></span> {{ $alamat }}</a></li>
								@endif
								@if ($nomor_telepon)
								<li><a href="tel:{{ str_replace(['(', ')', ' ', '-'],'',$nomor_telepon) }}" target="_blank"><span class="fa-li"><i class="fas fa-phone-square-alt"></i></span> {{ $nomor_telepon }}</a></li>
								@endif
								@if ($email)
								<li><a href="mailtp:{{ $email }}" target="_blank"><span class="fa-li"><i class="far fa-envelope"></i></span>{{ $email }}</a></li>
								@endif
							</ul>
						</div>
						<div class="col-md-4 col-sm-12 col-12 row m-0">
							<div class="col-md-12 col-sm-4 col-4 col-custom-4 ps-0 box-social">
								<span class="footer-title">Ikuti Kami</span>
								<ul>
									<?php
									$facebook = (new \App\Helpers\helper)->getSetting('facebook');
									$instagram = (new \App\Helpers\helper)->getSetting('instagram');
									$twitter = (new \App\Helpers\helper)->getSetting('twitter');
									?>
									@if ($instagram)
									<li>									
										<a href="{{ $instagram }}" target="_blank" class="image-social">
											<i class="fab fa-instagram"></i>
											<!-- <img src="{{ asset('assets/img/instagram.png') }}" alt="instagram"> -->
										</a>
									</li>
									@endif
									@if ($facebook)
									<li>
										<a href="{{ $facebook }}" target="_blank" class="image-social">
											<i class="fab fa-facebook"></i>
											<!-- <img src="{{ asset('assets/img/facebook.png') }}" alt="facebook"> -->
										</a>
									</li>
									@endif
									@if ($twitter)
									<li>
										<a href="{{ $twitter }}" target="_blank" class="image-social">
											<i class="fab fa-twitter"></i>
											<!-- <img src="{{ asset('assets/img/twitter.png') }}" alt="twitter"> -->
										</a>
									</li>
									@endif
								</ul>
							</div>
							<div class="col-md-12 col-sm-8 col-8 col-custom-8 ps-0 box-gplay">
								<?php
								$google_play = (new \App\Helpers\helper)->getSetting('google_play');
								$app_store = (new \App\Helpers\helper)->getSetting('app_store');
								?>
								@if ($google_play)
								<a href="{{ $google_play }}" target="_blank" class="image-play"><img src="{{ asset('assets/img/google-play.png') }}" alt="google-play"></a>
								@endif
								@if ($app_store)
								<a href="{{ $app_store }}" target="_blank" class="image-play"><img src="{{ asset('assets/img/app-store.png') }}" alt="google-play"></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
@elseif((new \App\Helpers\helper)->getRoute() == "page")
<div class="footer-page">
	<div class="container-xxl">
		<div class="logo-page"><img src="{{ asset('assets/img/logo.png') }}" alt="logo"></div>
		<div class="page-desc">&copy; 2021, PT. Jakarta Duta Wahana</div>
	</div>
</div>
@endif
@if(is_null(auth()->user()))
@if((new \App\Helpers\helper)->getRoute() != "forgot" && (new \App\Helpers\helper)->getRoute() != "login")
	@include('layouts/modal')
@endif
	<script crossorigin="anonymous" src="{{ asset('assets/plugins/jqueryvalidator/jquery.validate.min.js') }}"></script>
	<script crossorigin="anonymous" src="{{ asset('assets/js/modal.js?v='.date('ymdhis')) }}"></script>
@endif
	<script crossorigin="anonymous" src="{{ asset('assets/js/app.js?v='.date('ymdhis')) }}"></script>
	@if(((new \App\Helpers\helper)->getRoute() == ("checkout" || "alamat" || "profil" || "konfirmasi")))
	{{-- <script crossorigin="anonymous" src="{{ asset('assets/plugins/jqueryvalidator/additional-methods.min.js') }}"></script> --}}
	<script crossorigin="anonymous" src="{{ asset('assets/plugins/jqueryvalidator/jquery.validate.min.js') }}"></script>
	@endif
	@if ((new \App\Helpers\helper)->getRoute()=="alamat")
	<script crossorigin="anonymous" src="{{ asset('assets/js/alamat.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key={{ (new \App\Helpers\helper)->getSetting('maps_api') }}&callback=initMap&libraries"async></script>
	@endif

	@if ((new \App\Helpers\helper)->getRoute()=="checkout")
	<script crossorigin="anonymous" src="{{ asset('assets/js/checkout.js') }}"></script>
	@endif
</body>
</html>