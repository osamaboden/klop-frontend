<html>
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=yes, width=device-width">
	    <title>KLOP</title>
	    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
	    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link rel="dns-prefetch" href="https://fonts.gstatic.com">
	    <link rel="preconnect" href="https://cdn.jsdelivr.net" crossorigin><link rel="dns-prefetch" href="https://cdn.jsdelivr.net">
	    <link rel="preconnect" href="http://img.youtube.com" crossorigin><link rel="dns-prefetch" href="http://img.youtube.com">
	    <link rel="stylesheet" href="{{ asset('assets/css/font.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/owlcorousel/dist/assets/owl.carousel.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/owlcorousel/dist/assets/owl.theme.green.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/easyZoom/css/easyzoom.css') }}">
        @if((new \App\Helpers\helper)->getRoute() == "checkout" || (new \App\Helpers\helper)->getRoute() == "alamat" || (new \App\Helpers\helper)->getRoute() == "transaksi" || (new \App\Helpers\helper)->getRoute() == "detail")
	    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/bootstrap-5-theme.min.css') }}">
	    <link rel="stylesheet" href="{{ asset('assets/plugins/daterange/daterangepicker.css') }}">
        @endif
      @if ((new \App\Helpers\helper)->getRoute() == ("login") || 'forgot' == (new \App\Helpers\helper)->getRoute())
	    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">      
      @else
	    <link rel="stylesheet" href="{{ asset('assets/css/style.css?v'.date('ymdhis')) }}">      
      @endif
	    <link rel="preload" as="script" crossorigin="anonymous" href="{{ asset('assets/js/jquery.min.js') }}">
	    <link rel="preload" as="script" crossorigin="anonymous" href="{{ asset('assets/js/popper.min.js') }}">
	    <link rel="preload" as="script" crossorigin="anonymous" href="{{ asset('assets/js/bootstrap.min.js') }}">
	    <link rel="preload" as="script" crossorigin="anonymous" href="{{ asset('assets/plugins/fontawesome/js/all.min.js') }}">

	    <script crossorigin="anonymous" src="{{ asset('assets/js/jquery.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/js/jquery.mask.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/js/popper.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/fontawesome/js/all.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/owlcorousel/dist/owl.carousel.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/sweetalert2/sweetalert2.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/fancybox/source/jquery.fancybox.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/easyZoom/dist/easyzoom.js') }}"></script>
      @if((new \App\Helpers\helper)->getRoute() == "checkout" || (new \App\Helpers\helper)->getRoute() == "alamat" || (new \App\Helpers\helper)->getRoute() == "transaksi" || (new \App\Helpers\helper)->getRoute() == "detail")
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/daterange/moment.min.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/daterange/daterangepicker.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/daterange/bootstrap-datepicker.id.js') }}"></script>
      @endif
      @if ((new \App\Helpers\helper)->getRoute() == "semua_video" || (new \App\Helpers\helper)->getRoute() == "semua_gambar")
      <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
      <script crossorigin="anonymous" src="{{ asset('assets/js/GridHorizontal.js') }}"></script>
	    <script crossorigin="anonymous" src="{{ asset('assets/plugins/lazyload/jquery.lazy.min.js') }}"></script>
      @endif
      @if ((new \App\Helpers\helper)->getRoute() == ("profil"))
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
      <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>
      @endif
	</head>
	<body>
		@if((new \App\Helpers\helper)->getRoute() != "forgot" && (new \App\Helpers\helper)->getRoute() != "login")
		<section id="navbar">
			<nav class="navbar navbar-light navbar-expand-sm container-xxl">
			  <div class="container-fluid">
			    <a class="navbar-brand" href="/">
			    	<img src="{{ asset('assets/img/logo.png') }}" alt="logo">
			    </a>
          @if (is_null(auth()->user()))
					<form action="{{ url('/login') }}" class="form-search d-none d-sm-flex">
          @else
					<form method="GET" action="{{ url('/transaksi') }}" class="form-search d-none d-sm-flex">
          @endif
						<div>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Cari Transaksi..." aria-label="Username" aria-describedby="basic-addon1" name="q">
								<button class="btn"><i class="fas fa-search"></i></button>
							</div>
						</div>
					</form>
          <a href="#" data-target="#BoxSearch" data-toggle="pop-up-custom" class="form-search justify-content-end d-flex d-sm-none">
            <svg width="25" height="36" viewBox="0 0 34 36" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M14.1612 29.1658C6.89223 29.1658 0.99958 22.8606 0.99958 15.0829C0.99958 7.30512 6.89223 1 14.1612 1C21.4301 1 27.3228 7.30512 27.3228 15.0829C27.3228 22.8606 21.4301 29.1658 14.1612 29.1658Z" stroke="#151515" stroke-opacity="0.74" stroke-width="2" stroke-linecap="round" stroke-linejoin="bevel"/>
            <path d="M23.6939 25.5645L32.5122 35" stroke="#151515" stroke-opacity="0.74" stroke-width="2" stroke-linecap="round" stroke-linejoin="bevel"/>
            </svg>
          </a>
          {{-- <button data-toggle="search" class="d-sm-none d-block"><i class="fas fa-search"></i></button> --}}
          @if (is_null(auth()->user()))
				  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				  </button>
          @else
          <a class="nav-link nav-icon d-block d-sm-none" data-target="#BoxKeranjang" data-toggle="pop-up-custom" aria-current="page" href="#" data-id="Imgkeranjang"><img src="{{ asset('assets/img/keranjang.svg') }}" class="nav-image" alt="user">
            @if((new \App\Helpers\helper)->cekKeranjang()) <span class="badge bg-danger badge-custom">{{ (new \App\Helpers\helper)->cekKeranjang() }}</span> @endif
          </a>
          <a class="nav-link nav-icon d-block d-sm-none" data-target="#BoxProfil" data-toggle="pop-up-custom" aria-current="page" href="#"><img src="{{ asset('assets/img/user.svg') }}" class="nav-image" alt="user"></a>
          @endif
				<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
            @if (is_null(auth()->user()))
					  <li class="nav-item">
					  	<a class="nav-link" href="{{ url('/register') }}">Daftar</a>
					  </li>
					  <li class="nav-item">
						  <a class="nav-link" href="{{ url('/login') }}">Login</a>
					  </li>
            @else
            <li class="nav-item">
              <a class="nav-link active" data-target="#BoxKeranjang" data-toggle="pop-up-custom" aria-current="page" href="#" data-id="Imgkeranjang"><img src="{{ asset('assets/img/keranjang.svg') }}" class="nav-image" alt="user">
                  @if((new \App\Helpers\helper)->cekKeranjang()) <span class="badge bg-danger badge-custom">{{ (new \App\Helpers\helper)->cekKeranjang() }}</span> @endif
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-target="#BoxProfil" data-toggle="pop-up-custom" aria-current="page" href="#"><img src="{{ asset('assets/img/user.svg') }}" class="nav-image" alt="user"></a>
            </li>
            @endif
					</ul>
				  </div>
			  </div>

              @if (!is_null(auth()->user()))                  
              <div class="pop-up" id="BoxKeranjang" data-expand="false">
                <?php  
                $data['keranjang'] = (new \App\Helpers\helper)->getlistKeranjang();
                $data['total_keranjang'] = 0;
                ?>
                <div class="pop-up-header">
                    <h5 class="pop-up-title">Keranjang</h5>
                    <button type="button" class="btn-close" aria-label="Close"></button>
                </div>
                <div class="pop-up-body list-keranjang-pop">
                    @if (count($data['keranjang']) > 0)
                    @foreach ($data['keranjang'] as $item)
                    <?php $data['total_keranjang'] += $item['harga_total'];?>
                    <div class="card mb-3 keranjang" id="{{ $item['session'] }}" style="max-width: 540px;">
                        <div class="row g-0">
                          <div class="col-3">
                            <img src="{{ ENV('DATA_URL')."/galeri/".$item['image'] }}" style="max-height:100%;max-width:100%;" alt="gambar">
                          </div>
                          <div class="col-9">
                            <div class="card-body p-0 pl-2 pr-2">
                              <div class="row m-0">
                                <div class="col-6 list-name">{{ $item['nama'] }}</div>
                                <div class="col-6 list-btn-del text-end"><a href="#" data-toggle="deletecart" data-session="{{ $item['session'] }}"><i class="far fa-trash-alt"></i></a></div>
                                <div class="col-12 star">
                                    @for ($i = 0; $i < $item['star']; $i++)
                                    <i class="fas fa-star"></i>
                                    @endfor
                                    @for ($i = 0; $i < (5-$item['star']); $i++)
                                    <i class="far fa-star"></i>
                                    @endfor
                                </div>
                                <div class="col-6 list-qty">
                                    <span>Jumlah :</span>
                                    <span> {{ $item['qty'] }}{{ $item['satuan_beli'] }}</span>
                                </div>
                                <div class="col-6 list-harga">
                                    {{ number_format($item['harga_total'], 0, '.','.') }}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="keranjang-blank">
                        <div>
                            <img src="{{ url('assets/img/keranjang_times.svg') }}" style="width:170px" alt="keranjang_times">
                        </div>
                        <div>
                            <span>Tidak ada barang di keranjang!</span>
                        </div>
                    </div>
                    @endif
                </div>
                @if (count($data['keranjang']) > 0)
                <div class="pop-up-footer">
                    <div class="col-6 subtotal">
                        <div>Subtotal</div>
                        <div>{{ number_format($data['total_keranjang'], 0,'.','.') }}</div> 
                    </div>
                    <div class="col-6">
                        <a href='{{ url('/keranjang') }}' class="btn btn-success w-100 btn-custom">Lihat Keranjang</a>
                    </div>
                </div>
                @endif
            </div>
            <div class="pop-up" id="BoxProfil" data-expand="false">
                <div class="pop-up-body">
                    <ul>
                        <li><a href="{{ url('/profil') }}"><img src="{{ asset('assets/img/profil.svg') }}" alt="Profil"> Profil</a></li>
                        <li><a href="{{ url('/transaksi') }}"><img src="{{ asset('assets/img/wallet.svg') }}" alt="transaksi"> Transaksi Saya</a></li>
                        <li><a href="{{ url('/logout') }}"><img src="{{ asset('assets/img/logout.svg') }}" alt="logouy"> Keluar</a></li>
                    </ul>
                </div>
            </div>
            @endif

            <div class="pop-up" id="BoxSearch" data-expand="false">
              <div class="pop-up-body">
              @if (is_null(auth()->user()))
              <form action="{{ url('/login') }}" class="mb-0" >
              @else
              <form method="GET" action="{{ url('/transaksi') }}" class="mb-0">
              @endif
                <div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Cari Transaksi..." aria-label="Username" aria-describedby="basic-addon1" name="q">
                    <button class="btn"><i class="fas fa-search"></i></button>
                  </div>
                </div>
              </form>
              </div>
          </div>

			</nav>
		</section>
		@endif