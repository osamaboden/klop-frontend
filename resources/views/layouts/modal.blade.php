<div class="modal fade" id="ModalLogin" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header pb-0 border-0">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form id="LoginForm">
        @csrf
      <div class="modal-body pt-0">
        <div class="modal-logo">
        	<h3>login</h3>
        	<span class="modal-icon"><i class="fas fa-user"></i></span>
        </div>
        <div class="box-form">
	        <div class="form-group mb-3 mt-4">
	        	<input type="text" name="username" id="username" required class="form-control form-control-custom" placeholder="Email/Nomor Telepon">
            <span class="text-danger pb-2" id="username_validate"></span>
	        </div>
	        <div class="form-group mb-3">
	        	<input type="password" id="password" name="password" required class="form-control form-control-custom" placeholder="Password">
            <span class="text-danger pb-2" id="password_validate"></span>
	        </div>
        </div>
      </div>
      <div class="modal-footer border-0 justify-content-center">
      	<button type="button" id="LoginButton" class="btn btn-success btn-custom-modal">LOGIN</button>
      	<a href="#" data-toggle="modal" data-bs-dismiss="modal" data-bs-target="#ModalForgot" class="text-footer">Lupa password</a>
      	<a href="#" class="text-footer" data-toggle="modal" data-bs-target="#ModalRegister" data-bs-dismiss="modal">Tidak punya akun?</a>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalForgot" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header pb-0 border-0">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form id="ForgotPasswordForm">
        @csrf
      <div class="modal-body pt-0">
        <div class="modal-logo">
        	<h3>Lupa Password</h3>
        	<span class="modal-icon"><i class="fas fa-user-cog"></i></span>
        </div>
        <div class="box-form">
	        <div class="form-group mb-3 mt-4">
	        	<input type="email" name="email_password" id="email_password" class="form-control form-control-custom" placeholder="Email">
            <span class="text-danger" id="email_password_validate"></span>
	        </div>
        </div>
      </div>
      <div class="modal-footer border-0 justify-content-center">
      	<button type="button" id="ResetButton" class="btn btn-success btn-custom-modal">RESET PASSWORD</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalRegister" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header pb-0 border-0">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form type="POST" action="/data" id="RegisterForm">
        @csrf
        <div class="modal-body pt-0">
          <div class="modal-logo">
          	<h3>Daftar</h3>
          	<span class="modal-icon"><i class="fas fa-user-plus"></i></span>
          </div>
          <div class="box-form">
  	        <div class="form-group mb-3 mt-4">
  	        	<input type="text" name="name_register" id="name_register" class="form-control form-control-custom" placeholder="Nama Lengkap" required>
              <span class="text-danger pb-2" id="name_register_validate"></span>
  	        </div>
  	        <div class="form-group mb-3 mt-4">
  	        	<input type="email" name="email_register" id="email_register" class="form-control form-control-custom" placeholder="Email" required>
              <span class="text-danger pb-2" id="email_register_validate"></span>
  	        </div>
  	        <div class="form-group mb-3 mt-4">
  	        	<input type="text" name="telepon_register" id="telepon_register" class="form-control telepon form-control-custom" placeholder="Nomor Telepon ex. 08123..." required>
              <span class="text-danger pb-2" id="telepon_register_validate"></span>
  	        </div>
  	        <div class="form-group mb-3 mt-4">
  	        	<input type="password" name="password_register" id="password_register" class="form-control form-control-custom" placeholder="Password" required>
              <span class="text-danger pb-2" id="password_register_validate"></span>
  	        </div>
  	        <div class="form-group mb-3">
  	        	<input type="password" id="password_repeat" name="password_repeat" class="form-control form-control-custom" placeholder="Ulangi Password" required>
              <span class="text-danger pb-2" id="password_repeat_validate"></span>
  	        </div>
          </div>
        </div>
        <div class="modal-footer border-0 justify-content-center">
        	<button type="button" id="DaftarButton" class="btn btn-success btn-custom-modal">DAFTAR</button>
        	<a href="#" data-toggle="modal" data-bs-dismiss="modal" data-bs-target="#ModalLogin" class="text-footer">Sudah punya akun</a>
        </div>
      </form>
    </div>
  </div>
</div>
