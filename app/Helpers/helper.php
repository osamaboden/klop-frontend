<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
class Helper
{
	public static function getSetting($name){
		$data = DB::table('default_setting')
					->select('nilai')
					->where('nama', $name)
					->first();
		return $data->nilai;
	}

	public static function getRoute(){
		$routeName = Route::currentRouteName();
		return $routeName;
	}

	public static function cleanText($nama) {
		$kecil = strtolower($nama);
		$text = str_replace(" : ", "", $kecil);
		$text = str_replace(":", "", $text);
		$text = str_replace("\"", "", $text);
		$text = str_replace("'", "", $text);
		$text = str_replace(".", "", $text);
		$text = str_replace("&", "", $text);
		$text = str_replace(",", "", $text);
		$text = str_replace("!", "", $text);
		$text = str_replace("?", "", $text);
		$text = str_replace("/", "", $text);
		$text = str_replace("-", "", $text);
		$text = str_replace("#", "", $text);
		$text = str_replace(" ", "-", $text);
		
		return $text;
	}

	public static function cekKeranjang()
	{
		if(is_null(Auth::user())){
			return 0;
		}
		$id = Auth::user()->id;
		return DB::table('keranjang')
				  ->where('id_member', $id)
				  ->count();
	}

	public static function getlistKeranjang(){
		$data = [];
		$hasil = [];
		if(!is_null(Auth::user())){
			self::checkPerubahanHarga();
			$id = Auth::user()->id;
			$list = DB::table('keranjang')
						->join('produk', 'produk.id', '=', 'keranjang.id_produk')
						->where('keranjang.id_member', '=', $id)
						->select('keranjang.session','keranjang.id_produk', 'produk.nama','produk.meta_url', 'keranjang.qty', 'keranjang.harga_total', 'keranjang.qty_meter', 'keranjang.satuan_beli', 'produk.panjang')
						->get();
			foreach ($list as $key => $value) {
				$image = self::getSingleImage($value->id_produk);
				$star = self::getStarProduk($value->id_produk); 
				if(round($star) > 5){
					$star = 5;
				}
				$qty = $value->qty;
				if($value->satuan_beli == "meter"){
					$qty = $value->qty_meter;
				}
				$hasil[$key]['session'] = $value->session;
				$hasil[$key]['nama'] = $value->nama;
				$hasil[$key]['qty'] = $qty;
				$hasil[$key]['qty_pcs'] = $value->qty;
				$hasil[$key]['harga_total'] = $value->harga_total;
				$hasil[$key]['image'] = $image;
				$hasil[$key]['harga_satuan'] = self::getHargaSatuan($value->id_produk, 'harga');
				$hasil[$key]['qty_satuan'] = self::getHargaSatuan($value->id_produk, 'qty');
				$hasil[$key]['meta'] = $value->meta_url;
				$hasil[$key]['id_produk'] = $value->id_produk;
				$hasil[$key]['star'] = $star;
				$hasil[$key]['panjang'] = $value->panjang;
				$hasil[$key]['satuan_beli'] = ($value->satuan_beli == 'meter') ? 'm' : 'Pcs';
			}
			$data = collect($hasil);
		}
		// print('<pre>');print_r($data);exit;
		return $data;
	}

	public static function getSingleImage($id_produk){
		$data = DB::table('galeri')
					->where('kategori', 'produk')
					->where('tipe', 'image')
					->where('status', 'aktif')
					->where('id_produk', $id_produk)
					->select('nama_file')
					->limit('1')
					->first();
		return (!is_null($data)) ? $data->nama_file : '';
	}
	public static function getStarProduk($id_produk){
		$data = round(DB::table('produk_review')
							->where('status', 'aktif')    
							->where('id_produk', $id_produk)
							->avg('skor'));
		return $data;
	}

	public static function getHargaSatuan($id_produk, $field){
		$data = DB::table('produk_harga')
					->where('id_produk', $id_produk)
					->select($field)
					->orderBy('qty', 'asc')
					->first();
		return (!is_null($data)) ? $data->$field : 0;
	}

	public static function checkPerubahanHarga(){
		$id = Auth::user()->id;
		$list = DB::table('keranjang')
					->where('id_member', $id)
					->select('id_produk', 'qty', 'session')
					->get();
		foreach ($list as $key => $value) {
			$harga = self::getHargaNew($value->id_produk, $value->qty);
			DB::table('keranjang')
				->where('session', $value->session)
				->update([
					'harga_total' => $harga,
				]);
		}
	}

	public static function getHargaNew($id_produk, $qty)
	{
		$listharga = DB::table('produk_harga')
						->where('id_produk', $id_produk)
						->where('status', 'aktif')
						->select('qty', 'harga')
						->orderBy('qty', 'desc')
						->get();
		$harga = 0;
		$SelectQty = self::selectQty($listharga,$qty);
		$harga_satuan = "";
		foreach ($listharga as $key => $value) {
			if($key == $SelectQty){
				$harga_satuan = $value->harga;
			}
		}
		return round(intval($harga_satuan) * intval($qty));
	}

	public static function selectQty($listharga, $qty){
		$selectQty = "";
		foreach ($listharga as $key => $value) {
			if($selectQty == "" && $value->qty <= $qty){
				$selectQty = $key;
			}
		}

		if($selectQty == ""){
			$selectQty = 0;
		}
		return $selectQty;
	}

	public static function convertDate($string){
		list($d, $m, $y) = explode(' ', $string);
		$array = ['Januari' => '01', 'Februari' => '02', 'Maret' => '03', 'April' => '04', 'Mei' => '05', 'Juni' => '06', 'Juli' => '07', 'Agustus' => '08', 'September' => '09', 'Oktober' => '10', 'November' => '11', 'Desember' => '12'];
		return $y."-".$array[$m].'-'.$d;
	}

	public static function checkOrderExist($id){
		$data = DB::table('order')
					->where('id', $id)
					->select('id')
					->first();
		if(!is_null($data)){
			return true;
		}
		return false;
	}

	public static  function convertdateToDateIndo($string){
		list($y, $m, $d) = explode('-', $string);
		$array = ['01' => 'Januari','02' => 'Februari','03' => 'Maret','04' => 'April','05' => 'Mei' , '06' => 'Juni','07' => 'Juli','08' => 'Agustus','09' => 'September','10' => 'Oktober','11' => 'November','12' => 'Desember'];
		return $d." ".$array[$m]." ".$y;

	}

	public static function getKontenById($id){
		$data = DB::table('konten')
					->where('id', '=',$id)
					->select('konten')
					->first();
		if(is_null($data)){
			return null;
		}
		return $data->konten;
	}
	
}