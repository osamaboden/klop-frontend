<?php

namespace App\Http\Controllers;

use App\Helpers\helper;
use Illuminate\Http\Request;
use App\Models\HomeModel;
use GrahamCampbell\ResultType\Result;
use Illuminate\Support\Facades\Auth;
use Validator;

class HomeController extends Controller
{
	protected $HomeModel;
	function __construct()
	{
		$this->HomeModel = new HomeModel();
	}
    public function home(){
    	$data = $this->HomeModel->getdatahome();
    	return view('home', ['data' => $data]);
    }

    public function uniquedata(Request $request){
    	return response()->json($this->HomeModel->checkuniquedata($$request->field, $request->value), 200);
    }

    public function postregister(Request $request){
		$validator = Validator::make($request->all(), [
			'name_register' => 'required',
			'email_register'	=> 'required|email|unique:App\Models\Member,email',
			'telepon_register'	=> 'required|min:9|max:13|unique:App\Models\Member,no_hp',
			'password_register'	=> 'required|min:8',

        ]);
    	if ($request->method() == "POST" && $validator->passes()) {
    		$data = $this->HomeModel->setMember($request);
    	}else{
    		$data['is_error'] = true;
    	}

		$data['validator'] = $validator->passes();

    	return response()->json($data, 200);
    }

    public function verifikasi(Request $request){
    	$data = $this->HomeModel->CheckVerify($request);
    	return redirect('/login');
    }

    public function postlogin(Request $request){
		$validator = Validator::make($request->all(), [
			'username' => 'required',
			'password'	=> 'required|min:8'
        ]);
    	if ($request->method() == "POST" && $validator->passes()) {
    		$data = $this->HomeModel->CheckLogin($request);
    	}else{
    		$data['is_success'] = false;
    	}
    	return response()->json($data, 200);
    }

    public function sendverify(Request $request, $token){
    	return response()->json($this->HomeModel->setsendVerify($token));
    }

    public function resendverify(Request $request){
    	return response()->json($this->HomeModel->setResendVerify($request));
    }

    public function forgetpass(Request $request){
    	if ($request->method() == "POST" && $request->input('email_password')!="") {
    		$data = $this->HomeModel->sendForgetPass($request);
    	}else{
    		$data['is_success'] = false;
    	}

    	return response()->json($data, 200);
    }

    public function forgot(Request $request){
    	$check = $this->HomeModel->checkavailableForgetPass($request->email, $request->token);
    	if ($check['is_exist'] ==  true && $check['is_success'] == true) {
    		if ($check['is_expired'] == false && $check['is_success'] == true) {
    			return view('password.reset');
    		}else{
	    		return redirect('/');
    		}
    	}else{
    		return redirect('/');
    	}
    }

    public function resetpassword(Request $request){
    	if ($request->input('password') !="" && $request->input('token') != "") {
    		$data = $this->HomeModel->setresetpassword($request);
    	}else{
    		$data['is_success'] = false;
    	}

    	return response()->json($data);
    }

	public function logout(Request $request)
	{
		Auth::logout();

		$request->session()->invalidate();
	
		$request->session()->regenerateToken();
	
		return redirect('/');
	}

	public function login(Request $request){
		return view('password.login');
	}

	public function register(Request $request){
		return view('password.register');
	}

	public function forget(Request $request){
		return view('password.forget');
	}

	public function syarat(Request $request){
		$data = ['title' => 'Syarat dan Ketentuan', 'value' => Helper::getKontenById(2)];
		return view('page', ['data' => $data]);
	}

	public function kebijakan(Request $request){
		$data = ['title' => 'Kebijakan Privasi', 'value' => Helper::getKontenById(3)];
		return view('page', ['data' => $data]);
	}

	public function about(Request $request){
		$data = ['title' => 'Tentang Kami', 'value' => Helper::getKontenById(4)];
		return view('page', ['data' => $data]);
	}
}