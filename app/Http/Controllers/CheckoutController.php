<?php

namespace App\Http\Controllers;

use App\Helpers\helper as Helper;
use Illuminate\Http\Request;
use App\Models\CheckoutModel;
use Illuminate\Support\Facades\URL;
use Validator;

class CheckoutController extends Controller
{
    //
    protected $CheckoutModel;
    public function __construct() {
        $this->CheckoutModel = new CheckoutModel();
    }

    public function index(Request $request)
    {
        // if($this->CheckoutModel->CheckAddresexist()){
            $data = $this->CheckoutModel->getDataCheckout($request);
            if(count($data['keranjang']) > 0){
                return view('checkout.index',['data' => $data]);
            }else{
                return redirect('/');
            }
        // }else{
            // return redirect(URL::to('/alamat'));
        // }
    }

    public function alamat(Request $request){
        $data = $this->CheckoutModel->getDataPageAlamat();
        return view('checkout.alamat', ['data' => $data]);
    }

    public function ubahalamat(Request $request, $id){
        $data = $this->CheckoutModel->getDataPageAlamat($id);
        return view('checkout.alamat', ['data' => $data]);
    }

    public function pemasangan(Request $request)
    {
        return response()->json($this->CheckoutModel->hitungPemasangan(), 200);
    }

    public function changealamat(Request $request)
    {
        return response()->json($this->CheckoutModel->setChangeAlamat($request->id_alamat), 200);
    }

    public function kabupaten(Request $request)
    {
        return response()->json($this->CheckoutModel->listKabupaten($request->id), 200);
    }

    public function kecamatan(Request $request)
    {
        return response()->json($this->CheckoutModel->listkecamatan($request->id), 200);
    }

    public function simpanalamat(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'address' => 'required',
			'kecamatan'	=> 'required',
			'kabupaten'	=> 'required',
			'provinsi'	=> 'required',
			'label_alamat'	=> 'required',
        ]); 
        if(!$validator->passes()){
            return response()->json(['is_success' => false],400);
        }
        return response()->json($this->CheckoutModel->setsimpanalamat($request),200);
    }

    public function success(Request $request, $id)
    {
        if(!Helper::checkOrderExist($id)){
            return redirect('/');
        }
        $data = $this->CheckoutModel->getDataPageOrder($id);
        return view('checkout.success', ['data' => $data]);
    }

    public function saveorder(Request $request){
		$validator = Validator::make($request->all(), [
			'delivery' => 'required',
			'instalasi'	=> 'required',
			'nomor'	=> 'required',
        ]); 
        if(!$validator->passes()){
            return response()->json(['is_success' => false],400);
        }
        return response()->json($this->CheckoutModel->setsaveOrder($request), 200);
    }

    public function hapusalamat(Request $request){
        $data = $this->CheckoutModel->hapusAlamat($request);
        return response()->json($data, 200);
    }

}
