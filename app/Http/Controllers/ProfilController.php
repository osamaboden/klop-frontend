<?php

namespace App\Http\Controllers;

use App\Models\ProfilModel;
use Illuminate\Http\Request;
use File;


class ProfilController extends Controller
{
    //
    protected $profilModel;
    public function __construct() {
        $this->profilModel = new ProfilModel();
    }
    
    public function profil(Request $request){
        $data = $this->profilModel->getdataProfil();
        return view('profile.index', ['data' => $data]);
    }

    public function validatepassword(Request $request)
    {
        if(!is_null($request->old_password)){
            echo $this->profilModel->checkPassword($request->old_password);
        }else{
            echo "false";
        }
    }

    public function resetpassword(Request $request)
    {
        $data = $this->profilModel->setnewPassword($request);
        return response()->json($data, 200);
    }

    public function validateemail(Request $request)
    {
        if(!is_null($request->biodata_email)){
            echo $this->profilModel->checkEmail($request->biodata_email);
        }
    }

    public function updatebiodata(Request $request)
    {
        $data = $this->profilModel->setNewBiodata($request);
        return response()->json($data, 200);
    }

    public function changeprofil(Request $request)
    {
        $folderPath = public_path('storage/profile/');
 
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
 
        $imageName = uniqid() . '.png';
 
        $imageFullPath = $folderPath.$imageName;
 
        if(file_put_contents($imageFullPath, $image_base64)){
            File::copy($imageFullPath, public_path('../../backend/storage/app/public/profile/'.$imageName));
            unlink($imageFullPath);
            $data = $this->profilModel->setProfileNew($imageName);
        }else{
            $data = ['is_success' => false];
        }
        return response()->json($data, 200);
    }
}
