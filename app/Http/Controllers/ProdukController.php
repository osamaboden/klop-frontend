<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProdukModel;
use Validator;

class ProdukController extends Controller
{
    //
    protected $ProdukModel;
    public function __construct() {
        $this->ProdukModel = new ProdukModel();
    }
    public function detail(Request $request, $meta)
    {
        if($meta == ""){
            return redirect('/');
        }
        $dataProduk = $this->ProdukModel->dataDetailProduk($request,$meta);
        return view('produk.detail', ['data' => $dataProduk]);
    }

    public  function harga(Request $request)
    {
        return response()->json($this->ProdukModel->getDataHargaById($request->meta), 200);
    }

    public  function savekeranjang(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'meta'     => 'required',
            'qty-val'     => 'required',
            'subtotal'     => 'required',
        ]);
        $data['is_success'] = false;
        if($validator->passes()){
            $data = $this->ProdukModel->saveKeranjangSet($request);
        }
        return response()->json($data, 200);
    }

    public  function keranjangdelete(Request $request)
    {
        $data['is_success'] = false;  
        if($request->data_session != ""){
            $data = $this->ProdukModel->setkeranjangdelete($request);
        }
        return response()->json($data, 200);
    }

    public function ulasan(Request $request){
        if($request->ajax()){
            $data = $this->ProdukModel->getUlasan($request);
            return $data;
        }
        $data = $this->ProdukModel->getUlasan($request);
        return view('produk.review', ['data' => $data]);
    }
    
    public function semua_video(Request $request){
        if($request->ajax()){
            $data = $this->ProdukModel->getDataVideo($request);
            return $data;
        }
        $data = $this->ProdukModel->getDataVideo($request);
        return view('produk.video', ['data' => $data]);
    }

    public function semua_gambar(Request $request){
        if($request->ajax()){
            $data = $this->ProdukModel->getDataGambarAll($request);
            return $data;
        }
        $data = $this->ProdukModel->getDataGambarAll($request);
        return view('produk.gambar', ['data' => $data, 'title' => 'Galeri Gambar']);
    }

    public function semua_installasi(Request $request)
    {
        if($request->ajax()){
            $data = $this->ProdukModel->getDataInstallasiAll($request);
            return $data;
        }
        $data = $this->ProdukModel->getDataInstallasiAll($request);
        return view('produk.gambar', ['data' => $data, 'title' => 'Galeri Instalasi']);
    }
}
