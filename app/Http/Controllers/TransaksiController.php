<?php

namespace App\Http\Controllers;

use App\Models\TransaksiModel;
use Illuminate\Http\Request;
use Validator;
use File;
use GrahamCampbell\ResultType\Result;

class TransaksiController extends Controller
{
    //
    protected $TransaksiModel;
    public function __construct() {
        $this->TransaksiModel = new TransaksiModel();
    }

    public function daftar(Request $request)
    {
        $data = $this->TransaksiModel->getDataTransaksi($request);
        return view('transaksi.daftar', ['data' => $data]);
    }

    public function konfirmasi(Request $request)
    {
        $data = $this->TransaksiModel->checkKonfirmasi($request->id_order);
        return view('transaksi.konfirmasi', ['data' => $data]);
    }

    public function check_konfirmasi(Request $request)
    {
        if($request->id_order==""){
            return response()->json(['is_success' => false], 200);
        }
        $data = $this->TransaksiModel->checkKonfirmasi($request->id_order);
        return response()->json($data, 200);
    }

    public function postkonfirmasi(Request $request)
    {
        $image = $request->file('fileimage');
        $imageName = uniqid('konfirmasi') . '.' . $image->getClientOriginalExtension();
        if($image->move(public_path('storage/profile/'), $imageName)){
            File::copy(public_path('storage/profile/'.$imageName), public_path('../../backend/storage/app/public/bukti_transfer/'.$imageName));
            unlink('storage/profile/'.$imageName);
            $data = $this->TransaksiModel->saveKonfirmasi($request, $imageName);
            return response($data, 200);
        }else{
            return response(['is_success' => false], 200);
        }
    }

    public function detail(Request $request, $id){
        $data = $this->TransaksiModel->getDataOrder($id);
        if($data['is_exits'] && $data['order_is_member']){
            return view('transaksi.detail',  ['data' => $data]);
        }else{
            return redirect('transaksi');
        }
    }

    public function cancel(Request $request){
        if(is_null($request->id)){
            return response()->json(['is_success' => false],200);
        }
        $data = $this->TransaksiModel->cancelTransaksi($request->id);
        return response()->json($data, 200);
    }

    public function setreview(Request $request)
    {
        if($request->input('startvalue') == "" || $request->input('reviewValue') == "" || $request->input('id_order') == ""){
            return response()->json(['is_success' => false], 200);
        }
        $data = $this->TransaksiModel->setAddReview($request);
        return response()->json($data, 200);
    }
}
