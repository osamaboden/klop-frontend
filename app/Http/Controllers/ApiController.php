<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiModel;
use Validator;
use File;

class ApiController extends Controller
{
    //
    protected $apiModel;
    public function __construct() {
        $this->apiModel = new ApiModel();
    }

    public function dologin(Request $request){
        // $hasil['token'] = "123456";
        // return response()->json($request,200);
		$validator = Validator::make($request->all(), [
			'username' => 'required',
			'password'	=> 'required',

        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->setdologin($request);
        return response()->json($data,200);
    }

    public function updateprofile(Request $request){
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
            'email' => 'required',
            'id'    => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setupdateProdfil($request);
        return response()->json($data,200);
    }

    public function registrasi(Request $request) {
        $validator = Validator::make($request->all(),[
            'nama'      => 'required',
            'email'     => 'required',
            'noHP'      => 'required',
            'password'  => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setregistrasi($request);
        return response()->json($data,200);
    }

    public function homedata(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->sethomedata($request);
        return response()->json($data,200);
    }

    public function detailproduk(Request $request) {
        $validator = Validator::make($request->all(),[
            'id'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setdetailproduk($request);
        return response()->json($data,200);
    }

    public function simpankeranjang(Request $request) {
        $validator = Validator::make($request->all(),[
            'id'      => 'required',
            'member'      => 'required',
            'qty'  => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setsimpankeranjang($request);
        return response()->json($data,200);
    }

    public function lupapassword(Request $request){
        $validator = Validator::make($request->all(),[
            'email'      => 'required',
        ]);
        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }
        $data = $this->apiModel->sendForgetPass($request);
        return response()->json($data,200);
    }

    public function precheckout(Request $request) {
        $validator = Validator::make($request->all(),[
            'member'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setprecheckout($request);
        return response()->json($data,200);
    }

    public function getprovinsi(Request $request) {
        $validator = Validator::make($request->all(),[
            'member'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetprovinsi($request);
        return response()->json($data,200);
    }

    public function getkabupaten(Request $request) {
        $validator = Validator::make($request->all(),[
            'provinsi'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetkabupaten($request);
        return response()->json($data,200);
    }

    public function getkecamatan(Request $request) {
        $validator = Validator::make($request->all(),[
            'kabupaten'      => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetkecamatan($request);
        return response()->json($data,200);
    }

    public function simpanalamat(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required',
            'nama'          => 'required',
            'alamat'        => 'required',
            'kecamatan'     => 'required',
            'kabupaten'     => 'required',
            'provinsi'      => 'required',
            'catatan'       => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setsimpanalamat($request);
        return response()->json($data,200);
    }

    public function simpanorder(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'         => 'required',
            'delivery'          => 'required',
            'nilai_delivery'    => 'required',
            'id_installation'   => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false, 'dimana' => "satu"], 400);
        }

        $data = $this->apiModel->setsimpanorder($request);
        return response()->json($data,200);
    }

    public function getongkoskirim(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required',
            'id_alamat'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetongkoskirim($request);
        return response()->json($data,200);
    }

    public function getdaftaralamat(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetdaftaralamat($request);
        return response()->json($data,200);
    }

    public function getdataalamat(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetdataalamat($request);
        return response()->json($data,200);
    }

    public function getcontent(Request $request) {
        $validator = Validator::make($request->all(),[
            'id'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetcontent($request);
        return response()->json($data,200);
    }

    public function getdaftarkeranjang(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->setgetdaftarkeranjang($request);
        return response()->json($data,200);
    }

    public function hapuskeranjang(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->deleteKeranjang($request);
        return response()->json($data,200);
    }

    public function getriwayat(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_member'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->ambilriwayat($request);
        return response()->json($data,200);
    }

    public function detailtransaksi(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_order'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->getdetailtransaksi($request);
        return response()->json($data,200);
    }

    public function bataltransaksi(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_order'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->canceltransaksi($request);
        return response()->json($data,200);
    }

    public function hapusalamat(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_alamat'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->deletealamat($request);
        return response()->json($data,200);
    }

    public function reorder(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_order'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->orderkembali($request);
        return response()->json($data,200);
    }

    public function daftartestimoni(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_produk'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->getdaftartestimoni($request);
        return response()->json($data,200);
    }

    public function daftargaleri(Request $request) {
        $validator = Validator::make($request->all(),[
            'kategori'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->getdaftargaleri($request);
        return response()->json($data,200);
    }

    public function slidegaleri(Request $request) {
        $validator = Validator::make($request->all(),[
            'id_foto'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->getslidegaleri($request);
        return response()->json($data,200);
    }

    public function daftargalerivideo(Request $request) {
        $validator = Validator::make($request->all(),[
            'kategori'     => 'required'
        ]);

        if(!$validator->passes()){
            return response()->json(['is_success' => false], 400);
        }

        $data = $this->apiModel->getdaftargalerivideo($request);
        return response()->json($data,200);
    }

    public function uploadavatar(Request $request) {
        $folderPath = public_path('storage/profile/');
 
        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        //$image_type = $image_type_aux[1];
        $image_base64 = base64_decode($request->image);
 
        $imageName = uniqid() . '.png';
 
        $imageFullPath = $folderPath.$imageName;
 
        if(file_put_contents($imageFullPath, $image_base64)){
            File::copy($imageFullPath, public_path('../../backend/storage/app/public/profile/'.$imageName));
            unlink($imageFullPath);
            $data = $this->apiModel->setProfileNew($imageName, $request->id_member);
        }else{
            $data = ['is_success' => false];
        }
        return response()->json($data, 200);

    }

    public function agenlogin(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password'  => 'required',

        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->getagenlogin($request);
        return response()->json($data,200);
    }

    public function agentask(Request $request){
        $validator = Validator::make($request->all(), [
            'id_agen' => 'required',
            'tipe'  => 'required',

        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->getagentask($request);
        return response()->json($data,200);
    }

    public function agendetailtask(Request $request){
        $validator = Validator::make($request->all(), [
            'id_penugasan' => 'required'
        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->getagendetailtask($request);
        return response()->json($data,200);
    }

    public function agenbiodata(Request $request){
        $validator = Validator::make($request->all(), [
            'id_agen' => 'required'
        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->getagenbiodata($request);
        return response()->json($data,200);
    }

    public function agenselesai(Request $request){
        $validator = Validator::make($request->all(), [
            'id_penugasan' => 'required'
        ]);   

        if(!$validator->passes()){
            return response()->json(array('is_success' => false), 400);
        }
        $data = $this->apiModel->setagenselesai($request);
        return response()->json($data,200);
    }
}
