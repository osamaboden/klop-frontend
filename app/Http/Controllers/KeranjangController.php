<?php

namespace App\Http\Controllers;

use App\Models\KeranjangModel;
use Illuminate\Http\Request;
use App\Helpers\helper;

class KeranjangController extends Controller
{
    public $keranjangModel;
    public function __construct() {
        $this->keranjangModel = new KeranjangModel();
    } 

    public function keranjang(Request $request)
    {
     return view('keranjang.index');
    }

    public function updatekeranjang(Request $request)
    {
        $data = $this->keranjangModel->setUpdateKeranjang($request);
        return response()->json($data, 200);
    }
}
