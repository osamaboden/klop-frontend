<?php

namespace App\Models;

use App\Helpers\helper as Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\JoinClause;

class TransaksiModel extends Model
{
    use HasFactory;

    public function getDataTransaksi($request){
        $id = Auth::user()->id;
        $page = 1;
        $start = 0;
        $where = "order.id_member ='".$id."' ";
        if(!is_null($request->page)){
            $page = intval($request->page);
        }
        if(!is_null($request->status)){
            $where .= " AND order_status.status_order='".$request->status."' ";
        }
        if(!is_null($request->q)){
            $where .= " AND (order_status.status_order LIKE '%".$request->q."%' OR order.id LIKE '%".$request->q."%' OR produk.nama_produk LIKE '%".$request->q."%' OR (order.nilai_delivery + order.nilai_installation + (order_produk.harga_total)) LIKE '".str_replace(['.', 'Rp'],'',$request->q)."' OR order_produk.qty LIKE '".str_replace(['.', 'Rp'],'',$request->q)."' ) ";
        }
        $total = count(DB::table('order')
                    ->join(DB::raw('(SELECT z.id_order, z.status as status_order FROM order_status as z INNER JOIN (SELECT id_order, MAX(waktu_perubahan) as waktu_perubahan FROM order_status GROUP BY id_order) as x ON z.id_order=x.id_order AND z.waktu_perubahan=x.waktu_perubahan GROUP BY z.id_order ORDER BY z.waktu_perubahan asc) as order_status'),'order_status.id_order', '=', 'order.id')
                    ->join(DB::raw('(SELECT * FROM order_produk GROUP BY id_order,id_produk) as order_produk'), 'order_produk.id_order', '=', 'order.id')
                    ->join(DB::raw('(SELECT id as id_produk, nama as nama_produk, deskripsi as deskripsi_produk, status as status_produk, meta_url as url_produk FROM produk) as produk'), 'produk.id_produk', '=', 'order_produk.id_produk')
                    ->join(DB::raw("(SELECT id_produk, nama_file, status as status_gambar FROM galeri WHERE tipe='image' AND kategori='produk' AND status='aktif') as galeri"),'galeri.id_produk','=', 'order_produk.id_produk')
                    ->whereRaw($where)
                    ->select('order.id')
                    ->groupBy('order.id')
                    ->get());
        $perhalaman = 10;
        $jumlah_page = ceil($total/$perhalaman);
        if($page > 1){
            $start = ($page - 1) * $perhalaman;
        }
        // DB::enableQueryLog();
        // echo $jumlah_page." ".$total;
        $hasil['current_page'] = $page;
        $hasil['total'] = $total;
        $hasil['prev'] = (($page <= 1) || $total == 0) ? 0 : $page-1;
        $hasil['next'] = (($page == $jumlah_page) || $total == 0) ? 0 : $page+1;
        $hasil['list'] = DB::table('order')
                    ->join(DB::raw('(SELECT z.id_order, z.status as status_order FROM order_status as z INNER JOIN (SELECT id_order, MAX(waktu_perubahan) as waktu_perubahan FROM order_status GROUP BY id_order) as x ON z.id_order=x.id_order AND z.waktu_perubahan=x.waktu_perubahan GROUP BY z.id_order ORDER BY z.waktu_perubahan asc) as order_status'),'order_status.id_order', '=', 'order.id')
                    ->join(DB::raw('(SELECT * FROM order_produk GROUP BY id_order,id_produk) as order_produk'), 'order_produk.id_order', '=', 'order.id')
                    ->join(DB::raw('(SELECT id as id_produk, nama as nama_produk, deskripsi as deskripsi_produk, status as status_produk, meta_url as url_produk FROM produk) as produk'), 'produk.id_produk', '=', 'order_produk.id_produk')
                    ->join(DB::raw("(SELECT id_produk, nama_file, status as status_gambar FROM galeri WHERE tipe='image' AND kategori='produk' AND status='aktif') as galeri"),'galeri.id_produk','=', 'order_produk.id_produk')
                    ->whereRaw($where)
                    ->selectRaw('order.id, produk.nama_produk, date(order.waktu_order) as waktu_order, DATE_FORMAT(order.waktu_order, "%H:%i") as time_order, order_produk.qty, (order.nilai_delivery + order.nilai_installation + (order_produk.harga_total)) as harga, order_status.status_order, galeri.nama_file')
                    ->groupBy('order.id')
                    ->orderBy('order.waktu_order', 'DESC')
                    ->offset($start)
                    ->limit($perhalaman)
                    ->get();

        // dd(DB::getQueryLog());
        return $hasil;
        // exit();
    }

    public function checkKonfirmasi($id){
        $hasil['is_success'] = false;
        $data = DB::table('order')
                    ->where('id','=', $id)
                    ->select('id', 'nilai_delivery', 'nilai_installation')
                    ->first();
        if(!is_null($data)){
            $total = intval($data->nilai_delivery) + intval($data->nilai_installation);
            $detail = DB::table('order_produk')
                        ->where('id_order', '=', $id)
                        ->selectRaw('SUM(harga_total) as total')
                        ->first();
            $total += intval($detail->total);
            $hasil['is_success'] = true;
            $hasil['total'] = $total;
            $hasil['id_order'] = $id;
        }
        return $hasil;
    }

    public function saveKonfirmasi($request, $bukti)
    {
        $hasil['is_success'] = false;
        $id = DB::table('order_konfirmasi')->insertGetId([
            'id_order' => $request->input('nomorOrder'),
            'waktu_konfirmasi' => new Carbon(),
            'status' => 'baru',
            'nominal_transfer' => str_replace('.','',$request->input('nominalTransfer')),
            'bank'  => $request->input('bankTransfer'), 
            'atas_nama' => $request->input('NamaTransfer'),
            'bukti_transfer' => $bukti
        ]);
        if($id > 0){
            DB::table('order_status')
                ->insert(['id_order' => $request->input('nomorOrder'), 'waktu_perubahan' => new Carbon(), 'status' => 'menunggu konfirmasi', 'id_admin' => '0']);
            $hasil['is_success'] = true;
            $hasil['id_order'] = $request->input('nomorOrder');
        }

        return $hasil;
    }

    public function getDataOrder($id){
        $hasil['is_exits'] = Helper::checkOrderExist($id);
        $hasil['order_is_member'] = false;
        if($hasil['is_exits']){
            $id_member = Auth::user()->id;
            $hasil['data'] = DB::table('order')
                        ->where('id', '=', $id)
                        ->select('id','id_member', 'waktu_order', 'delivery', 'request_ready', 'alamat_kirim', 'id_proyek', 'nilai_delivery', 'nilai_installation', 'installation', 'nomor_telepon')
                        ->first();
            if($id_member == $hasil['data']->id_member){
                $hasil['order_is_member'] = true;
                $data = DB::table('order_status')
                            ->where('id_order', '=', $id)
                            ->select('status')
                            ->orderBy('waktu_perubahan', 'desc')
                            ->limit(1)->first();
                $hasil['data']->proyek = "";
                if($hasil['data']->id_proyek > 0){
                    $proyek = DB::table('proyek_kategori')
                                    ->where('id', $hasil['data']->id_proyek)
                                    ->select('nama')
                                    ->first();
                    $hasil['data']->proyek = $proyek->nama;
                }
                $hasil['status'] = $data->status;
                $hasil['status_pembayaran'] = $this->getStatusPembayaran($id);
                $hasil['alamat'] = $this->getAlamatBy($hasil['data']->alamat_kirim);
                $request_ready = new Carbon($hasil['data']->request_ready);
                $waktu_order = new Carbon($hasil['data']->waktu_order);
                $hasil['data']->request_ready = Helper::convertdateToDateIndo($request_ready->format('Y-m-d'));
                $hasil['data']->waktu_order = Helper::convertdateToDateIndo($waktu_order->format('Y-m-d'))." ".$waktu_order->format('H:i');
                $hasil['detail'] = DB::table('order_produk')
                                    ->join('produk', 'order_produk.id_produk', '=', 'produk.id')
                                    ->where('id_order', '=', $id)
                                    ->select('produk.nama','order_produk.qty', 'order_produk.harga_total', 'order_produk.id_produk', 'order_produk.satuan_beli', 'order_produk.qty_meter')
                                    ->get(); 
                $total = DB::table('order_produk')
                            ->where('id_order', '=', $id)
                            ->sum('harga_total');
                $hasil['subtotal'] = $total;
                $hasil['total'] = $total + $hasil['data']->nilai_installation + $hasil['data']->nilai_delivery;
                $hasil['review'] = $this->getReviewBy($id);
            }
        }

        // dd($hasil);
        return $hasil;
    }

    public function getReviewBy($id_order){
        $data = DB::table('produk_review')
                    ->where('id_order', '=',$id_order)
                    ->select('id','review', 'skor')
                    ->first();
        if(!is_null($data)){
            return $data;
        }
        return null;
    }

    public function getAlamatBy($id){
        if($id == 0){
            return Helper::getSetting('alamat');
        }else{
            $data = DB::table('member_alamat')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('member_alamat.id', $id)
                    ->selectRaw("member_alamat.id as id, member_alamat.nama, concat(member_alamat.alamat,', ',kecamatan.nama,', ',kabupaten.nama, ', ', provinsi.nama) as alamat")
                    ->first();
            if(!is_null($data)){
                return ucwords(strtolower($data->alamat));
            }else{
                return null;
            }
        }
    }

    public function cancelTransaksi($id)
    {
        $hasil['is_success'] = false;
        $date = Carbon::now();

        if(DB::table('order_status')->insert(['id_order' => $id, 'waktu_perubahan' => $date, 'status' => 'dibatalkan', 'id_admin' => '0'])){
            $hasil['is_success'] = true;
            $hasil['boxStatus'] = 'Dibatalkan&nbsp;<img src="'.url('assets/img/dibatalkan.svg').'" alt="gambar">';
        }

        return $hasil;
    }

    public function getStatusPembayaran($id)
    {
        $status = "Belum Dibayar";
        $data = DB::table('order_konfirmasi')
                    ->where('id_order', '=', $id)
                    ->where('status', '=', 'lunas')
                    ->select('status')
                    ->orderBy('waktu_konfirmasi', 'DESC')
                    ->limit(1)->first();
        if(!is_null($data)){
            $status = 'Lunas';
        }
        return $status;
    }

    public function setAddReview($request){
        $hasil['is_success'] = false;
        $id = Auth::user()->id;
        $data = DB::table('order_produk')
                    ->where('id_order', '=', $request->input('id_order'))
                    ->select('id_produk')->limit(1)->first();
        $id_produk = $data->id_produk;
        $date = Carbon::now();
        if($request->input('idReview') != 0 && $request->input('idReview') != ""){
         if(DB::table('produk_review')->where('id_review', '=', $request->input('idReview'))->upadte(['review' => $request->input('reviewValue'), 'skor' => htmlentities($request->input('startvalue'))])){
            $hasil['is_success'] = true;
         }
        }else{
            if(DB::table('produk_review')->insertGetId(['id_produk' => $id_produk, 'id_member' => $id, 'review' => $request->input('reviewValue'), 'skor' => htmlentities($request->input('startvalue')), 'waktu' => $date, 'status' => 'aktif', 'id_order' => $request->input('id_order')])){
                $hasil['is_success'] = true;
            }
        }
        return $hasil;
    }
}
