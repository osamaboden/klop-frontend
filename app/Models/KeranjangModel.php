<?php

namespace App\Models;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class KeranjangModel extends Model
{
    use HasFactory;
    public function setUpdateKeranjang($request)
    {
        $hasil['is_success'] = false;
        $hasil['exist_address'] = false;
        $cek = true;
        for ($i=0; $i < count($request->input('session')); $i++) { 
            $meter = null;
            $qty_pcs = $request->input('qty_pcs')[$i];
            $satuan_beli = $request->input('satuan_beli')[$i];
            if($satuan_beli == "meter"){
                $meter = $request->input('qty-val')[$i];
            }
            $session = $request->input('session')[$i];
            $harga = str_replace(['Rp.', '.', ' '], '',$request->input('subtotal')[$i]);
            DB::table('keranjang')->where('session', $session)
                ->update([
                    'qty' => $qty_pcs,
                    'satuan_beli' => $satuan_beli,
                    'qty_meter' => $meter,
                    'harga_total' => intval($harga),
                ]);
            if(!$this->checkUpdateKeranjang($request->input('session')[$i],$qty_pcs)){
                $cek = false;
            }
        }
        if($cek){
            $hasil['is_success'] = true;
            $hasil['exist_address'] = true;
        }
        return $hasil;
    }

    public function checkUpdateKeranjang($session, $qty){
        $hasil = false;
        $data = DB::table('keranjang')
                    ->where('session', $session)
                    ->where('qty', $qty)
                    ->select('session')
                    ->first();
        if(!is_null($data)){
            $hasil = true;
        }

        return $hasil;
    }


    public function CheckAddresexist()
    {
        $id =Auth::user()->id;
        $data = DB::table('order')
                    ->where('id_member', $id)
                    ->select('alamat_kirim')
                    ->orderBy('waktu_order', 'desc')
                    ->first();
        if(!is_null(session('id_alamat'))){
            return true;
        }
        if(!is_null($data)){
            session(['id_alamat' => $data->alamat_kirim]);
            return true;
        }else{
            $alamat = DB::table('member_alamat')
                        ->where('id_member', $id)
                        ->where('status', 'aktif')
                        ->select('id')
                        ->limit(1)
                        ->first();
            if(!is_null($alamat)){
                session(['id_alamat' => $alamat->id]);
                return true;
            }
        }
        return false;
    }
}
