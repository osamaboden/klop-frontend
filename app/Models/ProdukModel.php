<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\helper as helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
class ProdukModel extends Model
{
    use HasFactory;

    protected $limitInfinateScroll;
    protected $limitInfinateScrollgallery;
    public function __construct() {
        $this->limitInfinateScroll = 10;
        $this->limitInfinateScrollgallery = 15;
    }

    public  function dataDetailProduk($request,$meta)
    {
        $hasil['detail'] = DB::table('produk')
                              ->select('id', 'nama', 'deskripsi', 'meta_url', 'panjang')
                              ->where('meta_url', '=', $meta)
                              ->first();
        $id_produk = $hasil['detail']->id;
        $hasil['image'] = $this->getDataImage($id_produk,$request->image);
        $hasil['harga'] = $this->getDataHarga($id_produk);
        $hasil['ulasan'] = $this->getDataUlasan($id_produk, '3');
        // print('<pre>');print_r($hasil);exit;
        return $hasil;
    }

    public function getDataImage($id,$image)
    {
        $hasil['list'] = DB::table('galeri')
                            ->select('nama_file', 'thumbnail', 'title')
                            ->where('id_produk', '=', $id)
                            ->where('kategori', '=', 'produk')
                            ->where('tipe', '=', 'image')
                            ->where('status', 'aktif')
                            ->get();
        $hasil['active'] = '';
        $first = "";
        foreach ($hasil['list'] as $key => $value) {
            if($value->nama_file == $image){
                $hasil['active'] = $value->nama_file;
            }else if($image == "" && $key == 0){
                $hasil['active'] = $value->nama_file;
            }
            if($key == 0){
                $first = $value->nama_file;
            }
        }
        if($hasil['active'] == ""){
            $hasil['active'] = $first;
        }
        return $hasil;
    }

    public function getDataHarga($id){
        $hasil['list'] = DB::table('produk_harga')
                    ->select('qty', 'harga')
                    ->where('id_produk', '=', $id)
                    ->where('status', '=','aktif')
                    ->get();
        $hasil['active'] = DB::table('produk_harga')
                                ->select('qty', 'harga')
                                ->where('id_produk', '=', $id)
                                ->where('status', '=','aktif')
                                ->orderBy('qty', 'ASC')
                                ->first();
        return $hasil;
    }

    public function getDataUlasan($id, $limit = "")
    {
        $list = DB::table('produk_review')
                        ->join('member', 'member.id','=', 'produk_review.id_member')
                        ->select('produk_review.id', 'produk_review.review', 'produk_review.skor', 'member.nama', 'member.foto_profile')
                        ->where('produk_review.id_produk', '=', $id)
                        ->where('produk_review.status', '=', 'aktif')
                        ->orderBy('produk_review.waktu', 'DESC');
        if($limit != ""){
            $hasil['list'] = $list->limit($limit)->get();
        }else{
            $hasil['list'] = $list->limit(10)->get();
        }
        $star = round(DB::table('produk_review')
                        ->where('status', 'aktif')    
                        ->where('id_produk', $id)
                        ->avg('skor')); 
        if(round($star) > 5){
            $star = 5;
        }
        $hasil['star'] = $star;
        $hasil['jml'] = DB::table('produk_review')
                        ->where('status', 'aktif')    
                        ->where('id_produk', $id)
                        ->count();   
        return $hasil;
    }

    public function getDataHargaById($meta)
    {
        $hasil['is_success'] = false;
        if($meta!=""){
            if($meta != "keranjang"){
                $data = DB::table('produk')
                            ->select('id')
                            ->where('meta_url', $meta)
                            ->first();
                if(!is_null($data)){
                    $hasil['is_success'] = true;
                    $harga = $this->getDataHarga($data->id);
                    $hasil['list'][$meta] = $harga['list'];
                }
            }else{
                if(!is_null(AUth::user())){
                    $list = helper::getlistKeranjang();
                    foreach ($list as $key => $value) {
                        $harga = $this->getDataHarga($value['id_produk']);
                        $hasil['list'][$value['meta']] = $harga['list'];
                    }
                    if(count($hasil) > 0){
                        $hasil['is_success'] = true;
                    }
                }
            }
        }
        return $hasil;
    }

    public function saveKeranjangSet($request)
    {
        $meter = null;
        $qty_pcs = $request->input('qty_pcs');
        $satuan_beli = $request->input('satuan_beli');
        if($satuan_beli == "meter"){
            $meter = $request->input('qty-val');
        }
        $hasil['is_success'] = false;
        $data = DB::table('produk')
                ->select('id')
                ->where('meta_url', $request->input('meta'))
                ->first();  
        if(!is_null($data)){
            $session = md5(uniqid('keranjang'.$data->id, true));
            DB::table('keranjang')
                ->where('id_member', Auth::user()->id)
                ->where('id_produk', $data->id)
                ->delete();
                DB::table('keranjang')
                    ->insert([
                        'session' => $session,
                        'id_produk' => $data->id,
                        'qty'       => $qty_pcs,
                        'satuan_beli' => $satuan_beli,
                        'qty_meter' => $meter,
                        'harga_total'  => str_replace( ['.','Rp', 'RP', 'rp'],'',$request->input('subtotal')),
                        'id_member'     => Auth::user()->id
                    ]);
                if($this->checkKeranjang($session, 'session')){
                    $hasil['is_success'] = true;
                    $hasil['jml'] = helper::cekKeranjang();
                    $html = "";
                    $listkeranjang = helper::getlistKeranjang();
                    $total = 0;
                    $nama = $star = $img = $harga = "";
                    foreach ($listkeranjang as $key => $item) {
                        if($session == $item['session']){
                            $star = $item['star'];
                            $img = ENV('DATA_URL')."/galeri/".$item['image'];
                            $harga = number_format($item['harga_total'], 0, '.','.');
                            $nama = $item['nama'];
                        }
                        $html .= '<div class="card mb-3 keranjang" id="'.$item['session'].'" style="max-width: 540px;"><div class="row g-0"><div class="col-3"><img src="'.ENV('DATA_URL')."/galeri/".$item['image'].'" style="max-height:100%;max-width:100%;" alt="gambar"></div><div class="col-9"><div class="card-body p-0 pl-2 pr-2"><div class="row m-0"><div class="col-6 list-name">'.$item['nama'].'</div><div class="col-6 list-btn-del text-end"><a href="#" data-toggle="deletecart" data-session="'.$item['session'].'"><i class="far fa-trash-alt"></i></a></div><div class="col-12 star">';
                        for ($i = 0; $i < $item['star']; $i++) { $html .= '<i class="fas fa-star"></i>'; }
                        for ($i = 0; $i < (5-$item['star']); $i++){ $html .= '<i class="far fa-star"></i>';}
                        $html .= '</div><div class="col-6 list-qty"><span>Jumlah :</span><span> '.$item['qty'].$item['satuan_beli'].'</span></div><div class="col-6 list-harga">'.number_format($item['harga_total'], 0, '.','.').'</div></div></div></div></div></div>';
                        $total += $item['harga_total'];
                    }
                    $hasil['html'] = $html;
                    $hasil['html_footer'] = '<div class="pop-up-footer"><div class="col-6 subtotal"><div>Subtotal</div><div>'.number_format($total, 0, '.','.').'</div> </div><div class="col-6"><a href="'.URL::to('/keranjang').'" class="btn btn-success btn-custom">Lihat Keranjang</a></div></div>';
                    if($img !="" && $star != "" && $harga !=""){
                        $hasil['html_popup'] = '<div id="swal2-content" class="swal2-html-container" style="margin-top: 10px;"> <div class="row m-0"><div class="col-md-4 col-12"><img src="'.$img.'"class="w-100 h-auto"></div><div class="col-md-8 col-12" style="text-align: start;"><div style="font-size: 25px;color: black;font-weight: 600;">'.$nama.'</div><div class="star" style="font-size: 13px;color: #FDBC15;margin-top: 2px;">';
                        for ($i = 0; $i < $star; $i++) { $hasil['html_popup'] .= '<i class="fas fa-star"></i>'; }
                        for ($i = 0; $i < (5-$star); $i++){ $hasil['html_popup'] .= '<i class="far fa-star"></i>'; }
                        $hasil['html_popup'] .= '</div><div style="color: #57D024;font-weight: 600;font-size: 20px;margin-top: 5px;">'.$harga.'</div><a href="/keranjang" class="btn btn-success btn-custom" style="margin-top: 12px;">Lihat Keranjang</a></div> </div></div>';
                    }else{
                        $hasil['html_popup'] = "";
                    }
                }
        } 
        return $hasil;
    }

    public function checkKeranjang($val, $field)
    {
        $hasil = false;
        $cek = DB::table('keranjang')
                    ->select('session')
                    ->where($field, $val)
                    ->first();
        if(!is_null($cek)){
            $hasil = true;
        }
        return $hasil;
    }

    public function setkeranjangdelete($request)
    {
        $hasil['is_success'] = false;
        DB::table('keranjang')
            ->where('session', $request->data_session)
            ->delete();
            if(!$this->checkKeranjang($request->data_session,'session')){
                $hasil['is_success'] = true;
                $hasil['html']      = '<div class="keranjang-blank"><div><img src="'.URL::to('assets/img/keranjang_times.svg').'" style="width:170px" alt="keranjang_times"></div><div class="mt-3"><span>Tidak ada barang di keranjang!</span></div>';
                $hasil['html_keranjang'] = '<div class="keranjang-blank"><div> <img src="'.URL::to('assets/img/keranjang_times.svg').'" style="width: 55%;" alt="keranjang_times"></div><div><span>Tidak ada barang di keranjang!</span><a href="/" class="btn btn-success btn-custom mt-3">Kembali Ke Beranda</a></div></div>';
                $hasil['jml'] = helper::cekKeranjang();
            }
        return $hasil;
    }

    public function getUlasan($request){
        $start = 0;
        $page = ($request->page) ? $request->page : 1;
        $filter_star = ($request->star) ? $request->star : "";
        $data = DB::table('produk')
                    ->where('meta_url', '=', helper::getSetting('produk_meta'))
                    ->select('id')
                    ->first();
        if($page > 1){
            $start = ($page - 1) * $this->limitInfinateScroll;
        }
        $data = DB::table('produk_review')
                            ->join('member', 'member.id','=', 'produk_review.id_member')
                            ->select('produk_review.id', 'produk_review.review', 'produk_review.skor', 'member.nama', 'member.foto_profile')
                            ->where('produk_review.id_produk', '=', $data->id)
                            ->where('produk_review.status', '=', 'aktif');
        if($filter_star != "" && $filter_star !="all"){
            $hasil['data'] = $data->where('produk_review.skor', '=', $filter_star)->orderBy('produk_review.waktu', 'DESC')->offset($start)->limit($this->limitInfinateScroll)->get();
        }else{
            $hasil['data'] = $data->orderBy('produk_review.waktu', 'DESC')->offset($start)->limit($this->limitInfinateScroll)->get();
        }
        $hasil['html'] = "";
        foreach ($hasil['data'] as $key => $item) {
            $hasil['html'] .= '<div class="card card-body ps-0 ps-sm-3 pe-0 pe-sm-3"><div class="row m-0"><div class="col-lg-2 col-md-3 col-12 d-flex"><div class="icon-ulasan"><img src="'.ENV('DATA_URL')."/profile/".$item->foto_profile.'" alt="user"></div><div class="ulasan-name"><div>'.$item->nama.'</div><div class="star"><span class="star">';
            $star = $item->skor;
            $rstar = 5-$star;
            for ($i = 0; $i < $star; $i++){
                $hasil['html'] .= '<i class="fas fa-star"></i>';
            }
            for ($i = 0; $i < ($rstar); $i++){
                $hasil['html'] .='<i class="far fa-star"></i>';
            }
            $hasil['html'] .= '</div></div></div><div class="col-lg-10 col-md-9 pt-3 pt-md-0 col-12 desc">'.$item->review.'</div></div></div>';
        }
        // printf('<pre>');print_r($hasil);exit;
        return $hasil;  
    }

    public function getDataVideo($request)
    {
        $page = ($request->page) ? $request->page : 1;
        $start = 0;
        if($page > 1){
            $start = ($page - 1) * $this->limitInfinateScrollgallery;
        }
        $data = DB::table('produk')
                    ->where('meta_url', '=', helper::getSetting('produk_meta'))
                    ->select('id')
                    ->first();
    	$datavideo = DB::table('galeri')
    					->select('title', 'nama_file', 'id')
    					->where('tipe', 'video')
    					->where('status', 'aktif')
                        ->offset($start)
    					->limit($this->limitInfinateScrollgallery)
    					->get();
    	$dataarr = [];
        $hasil['html'] = "";
    	foreach ($datavideo as $key => $value) {
    		$nama_file = explode('?', $value->nama_file);
    		$id_video  = substr(end($nama_file), 2);
    		$gambar	   = "http://img.youtube.com/vi/".$id_video."/maxresdefault.jpg";
    		$arr['title'] = $value->title;
    		$arr['nama_file'] = $value->nama_file;
    		$arr['id'] = helper::cleanText($value->nama_file)."-".$value->id;
    		$arr['url_gambar'] = $gambar; 
    		array_push($dataarr, json_decode(json_encode($arr)));
            $hasil['html'] .= '<div class="col-lg-3 col-md-4 col-sm-6 col-12"><div class="box-gallery lazy_loading"><div class="show-detail gallery-video"><a  data-caption="'.$value->title.'" class="fancybox-media" data-href="'.$value->nama_file.'"><svg width="94" height="93" viewBox="0 0 94 93" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="46.9864" cy="46.4322" r="45.3589" stroke="white" stroke-width="2"/><path d="M76.2734 44.2269C78.2734 45.3816 78.2734 48.2684 76.2734 49.4231L35.1822 73.1471C33.1822 74.3018 30.6822 72.8584 30.6822 70.549L30.6822 23.101C30.6822 20.7916 33.1822 19.3482 35.1822 20.5029L76.2734 44.2269Z" fill="white"/></svg>	</a></div><img data-src="'.$gambar.'" class="image lazy d-none" alt="image1"></div></div>';
    	}

    	$hasil['data'] = collect($dataarr);
        $hasil['count'] = DB::table('galeri')
        ->select('title', 'nama_file', 'id')
        ->where('tipe', 'video')
        ->where('status', 'aktif')->count('galeri.id');
        // printf('<pre>');print_r($hasil);exit;
        return $hasil;
    }

    public function getDataGambarAll($request){
        $page = ($request->page) ? $request->page : 1;
        $start = 0;
        if($page > 1){
            $start = ($page - 1) * $this->limitInfinateScrollgallery;
        }
        $data = DB::table('produk')
                    ->where('meta_url', '=', helper::getSetting('produk_meta'))
                    ->select('id')
                    ->first();
        $hasil['data'] = DB::table('galeri')
                    ->select('nama_file', 'thumbnail', 'title')
                    ->where('tipe', '=', 'image')
                    ->where('status', 'aktif')
                    ->offset($start)
                    ->limit($this->limitInfinateScrollgallery)
                    ->get();
        $hasil['html'] = "";
        foreach ($hasil['data'] as $key => $item) {
            $hasil['html'] .= '<div class="gallery-item lazy_loading"><a data-href="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="content"><img src="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="hiddenfile" data-src="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="lazy" alt="'.$item->nama_file.'"></a></div>';
        }

        $hasil['count'] = DB::table('galeri')
                    ->select('nama_file', 'thumbnail', 'title')
                    ->where('tipe', '=', 'image')
                    ->where('status', 'aktif')->count('galeri.id');
        
        return $hasil;
    }

    public function getDataInstallasiAll($request){
        $page = ($request->page) ? $request->page : 1;
        $start = 0;
        if($page > 1){
            $start = ($page - 1) * $this->limitInfinateScrollgallery;
        }
        DB::enableQueryLog();
        $data = DB::table('produk')
                    ->where('meta_url', '=', helper::getSetting('produk_meta'))
                    ->select('id')
                    ->first();
        $hasil['data'] = DB::table('galeri')
                    ->select('nama_file', 'title', 'tipe')
                    ->where('kategori', '=', 'instalasi')
                    ->where('status', 'aktif')
                    ->offset($start)
                    ->limit($this->limitInfinateScrollgallery)
                    ->get();
        $hasil['html'] = "";
        foreach ($hasil['data'] as $key => $item) {
            if($item->tipe == "image"){
                $hasil['html'] .= '<div class="gallery-item lazy_loading"><a data-href="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="content"><img src="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="hiddenfile" data-src="'.ENV('DATA_URL').'/galeri/'.$item->nama_file.'" class="lazy" alt="'.$item->nama_file.'"></a></div>';
            }else{
                $nama_file = explode('?', $item->nama_file);
                $id_video  = substr(end($nama_file), 2);
                $gambar	   = "http://img.youtube.com/vi/".$id_video."/maxresdefault.jpg";
                $hasil['html'] .= '<div class="gallery-item gal-video lazy_loading"><a data-href="'.$item->nama_file.'" class="content"><svg width="94" height="93" viewBox="0 0 94 93" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="46.9864" cy="46.4322" r="45.3589" stroke="white" stroke-width="2"/><path d="M76.2734 44.2269C78.2734 45.3816 78.2734 48.2684 76.2734 49.4231L35.1822 73.1471C33.1822 74.3018 30.6822 72.8584 30.6822 70.549L30.6822 23.101C30.6822 20.7916 33.1822 19.3482 35.1822 20.5029L76.2734 44.2269Z" fill="white"/></svg><img src="'.$gambar.'" class="hiddenfile" data-src="'.$gambar.'" class="lazy" alt="gambar"></a></div>';
                // $hasil['html'] .= '<div class="box-gallery gallery-item lazy_loading"><div class="show-detail gallery-video"><a  data-caption="'.$item->title.'" class="fancybox-media" data-href="'.$item->nama_file.'"><svg width="94" height="93" viewBox="0 0 94 93" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="46.9864" cy="46.4322" r="45.3589" stroke="white" stroke-width="2"/><path d="M76.2734 44.2269C78.2734 45.3816 78.2734 48.2684 76.2734 49.4231L35.1822 73.1471C33.1822 74.3018 30.6822 72.8584 30.6822 70.549L30.6822 23.101C30.6822 20.7916 33.1822 19.3482 35.1822 20.5029L76.2734 44.2269Z" fill="white"/></svg>	</a></div><img data-src="'.$gambar.'" class="hiddenfile" alt="image1"></div>';
            }
        }

        $hasil['count'] = DB::table('galeri')
        ->select('nama_file', 'thumbnail', 'title')
        ->where('kategori', '=', 'instalasi')
        ->where('status', 'aktif')->count('galeri.id');

        // print('<pre>');print_r($hasil);exit;
        
        return $hasil;
    }
}
