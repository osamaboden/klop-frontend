<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\CheckoutModel;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Hash;

class ProfilModel extends Model
{
    use HasFactory;


    public function getdataProfil()
    {
        $id = Auth::user()->id;
        $CheckoutModel = new CheckoutModel();
        $hasil['biodata'] = DB::table('member')
                                ->where('id', $id)
                                ->select('no_hp', 'nama', 'foto_profile', 'email')
                                ->first();

        $hasil['alamat'] = $CheckoutModel->getAllAlamat();
        // print('<pre>');print_r($hasil);
        // exit;
        return $hasil;
    }

    public function checkPassword($password)
    {
        $id = Auth::user()->id;
        $data = DB::table('member')
                    ->where('id', '=', $id)
                    ->select('password')
                    ->first();
        if($data->password != "" && Hash::check('01X'.$password.'@#', $data->password)){
            return "true";
        }else{
            return "false";
        }
    }

    public function setnewPassword($request)
    {
        $id = Auth::user()->id;
        $hasil['is_success'] = false;
        $hasil['is_valid'] = false;
        if($this->checkPassword($request->input('old_password'))){
            $hasil['is_valid'] = true;
            $newpass = Hash::make('01X'.$request->input('new_password').'@#');
            if(DB::table('member')->where('id', '=', $id)->update([
                'password' => $newpass
            ])){
                $hasil['is_success'] = true;
            }
        }
        return $hasil;
    }

    public function checkEmail($email)
    {
        $id = Auth::user()->id;
        $data = DB::table('member')
                    ->where('id', '=', $id)
                    ->select('email')
                    ->first();
        if($data->email == $email){
            return "true";
        }else{
            $count = DB::table('member')
                        ->where('email', '=', $email)
                        ->count();
            if($count > 0){
                return "false";
            }else{
                return "true";
            }
        }
    }

    public function setNewBiodata($request)
    {
        $id = Auth::user()->id;
        $hasil['is_success'] = false;
        if($this->checkEmail($request->input('biodata_email'))){
            if(DB::table('member')->where('id', '=', $id)->update(['nama' => $request->input('biodata_nama'), 'email' => $request->input('biodata_email')])){
                $hasil['is_success'] = true;
                $hasil['nama'] = $request->input('biodata_nama');
                $hasil['email'] = $request->input('biodata_email');
            }
        }
        return $hasil;
    }

    public function setProfileNew($name){
        $id = Auth::user()->id;
        $hasil['is_success'] = false;
        if(DB::table('member')->where('id', '=', $id)->update(['foto_profile' => $name])){
            $hasil['is_success'] = true;
            $hasil['foto'] = Env('DATA_URL')."/profile/".$name;
        }
        return $hasil;
    }
}
