<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Helpers\helper as helper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class ApiModel extends Model
{
    use HasFactory;

    public function setdologin($request){
        $token = Str::random(80);
        $hasil['is_success'] = false;
        $hasil['is_exist'] = false;
        $hasil['is_verified'] = true;
        if (is_numeric($request->input('username')) == 1) {
            $field = 'no_hp';
        }else{
            $field = 'email';
        }

        $data = DB::table('member')
                    ->select('id','email', 'no_hp','status', 'password', 'nama', 'foto_profile')
                    ->where($field, '=', $request->input('username'))
                    ->first();
        if (!is_null($data)) {
            $hasil['is_exist'] = true;
            if ($data->status=="not verified") {
                $hasil['is_verified'] = false;
            }else{
                if ($request->input('password') !="" && Hash::check('01X'.$request->input('password').'@#', $data->password)) {
                    $hasil['is_success'] = true;
                    /*DB::table('member')
                        ->where('id', $data->id)
                        ->update([
                            'api_token' => hash('sha256', $token)
                        ]);*/
                    if (Hash::needsRehash($data->password)) {
                        $hashed = Hash::make($request->input('password'));
                        DB::table('member')
                            ->where('id', $data->id)
                            ->update([
                                'password' => $hashed
                        ]);
                    }
                    $hasil['api_token'] = $token;
                    $hasil['id_member'] = $data->id;
                    $hasil['email'] = $data->email;
                    $hasil['no_hp'] = $data->no_hp;
                    $hasil['nama'] = $data->nama;
                    $hasil['foto_profile'] = $data->foto_profile;
                }
            }
        }
        return $hasil;
    }

    public function setupdateProdfil($request)
    {
        $hasil['is_success'] = false;
        $update = DB::table('member')
                    ->where('id', $request->input('id'))
                    ->update([
                        'nama'  => $request->input('nama'),
                        'email' => $request->input('email')
                    ]);
        if($update){
            $hasil['is_success'] = true;
        }

        return $hasil;
    }

    public function setregistrasi($request) {
        $token = Str::random(80);
        $hasil['is_success'] = false;
        $hashed = Hash::make('01X'.$request->input('password').'@#');
        $insert = DB::table('member')
                    ->insertGetId([
                        'no_hp'         => $request->input('noHP'),
                        'password'      => $hashed,
                        'nama'          => $request->input('nama'),
                        'email'         => $request->input('email'),
                        'status'        => 'aktif',
                        'foto_profile'  => 'user.png',
                        'api_token'     => $token
                    ]);
        if($insert){
            $hasil['api_token'] = $token;
            $hasil['id_member'] = $insert;
            $hasil['email'] = $request->input('email');
            $hasil['no_hp'] = $request->input('noHP');
            $hasil['nama'] = $request->input('nama');
            $hasil['foto_profile'] = 'user.png';
            $hasil['is_success'] = true;
        }

        return $hasil;
    }

    public function sethomedata($request) {
        $hasil['is_success'] = false;
        $sliders = DB::table('slide_show')
                    ->select('gambar')
                    ->where('kategori', '=', 'mobile')
                    ->get();
        $i = 0;
        foreach ($sliders as $slider) {
            $hasil['slide'][$i]['gambar'] = $slider->gambar;
            $i++;
        }

        $produkImages = DB::table('galeri')
                    ->select('id', 'nama_file')
                    ->where('kategori', '=', 'produk')
                    ->where('tipe', '=', 'image')
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($produkImages)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($produkImages as $image) {
                $hasil['galeri'][$i]['nama_file'] = $image->nama_file;
                $i++;
            }
        }

        $banners = DB::table('banner')
                    ->select('gambar')
                    ->where('kategori', '=', 'mobile')
                    ->where('status', '=', 'aktif')
                    ->get();
        $i = 0;
        foreach ($banners as $banner) {
            $hasil['banner'][$i]['gambar'] = $banner->gambar;
            $i++;
        }

        $videos = DB::table('galeri')
                    ->select('thumbnail', 'id_youtube')
                    //->where('kategori', '=', 'produk')
                    ->where('tipe', '=', 'video')
                    ->where('status', '=', 'aktif')
                    ->orderBy(DB::raw('RAND()'))
                    ->limit(4)
                    ->get();
        $i = 0;
        foreach ($videos as $video) {
            $hasil['video'][$i]['thumbnail'] = $video->thumbnail;
            $hasil['link_video'][$i]['id_youtube'] = $video->id_youtube;
            $i++;
        }

        $instalasis = DB::table('galeri')
                    ->select('nama_file')
                    ->where('kategori', '=', 'instalasi')
                    ->where('tipe', '=', 'image')
                    ->where('status', '=', 'aktif')
                    ->orderBy(DB::raw('RAND()'))
                    ->limit(4)
                    ->get();
        $i = 0;
        foreach ($instalasis as $instalasi) {
            $hasil['instalasi'][$i]['thumbnail'] = $instalasi->nama_file;
            $i++;
        }
        $reviews = DB::table('produk_review')
                    ->select('id_member', 'review', 'skor')
                    ->orderBy(DB::raw('RAND()'))
                    ->limit(2)
                    ->get();
        $i = 0;
        foreach ($reviews as $review) {
            $hasil['review'][$i]['teks'] = $review->review;
            $hasil['review'][$i]['skor'] = $review->skor;
            $member = DB::table('member')
                        ->select('nama', 'foto_profile')
                        ->where('id', '=', $review->id_member)
                        ->first();
            $hasil['review'][$i]['nama'] = $member->nama;
            $hasil['review'][$i]['foto'] = $member->foto_profile;
            $i++;
        }
        $qty = DB::table('keranjang')
                        ->where('id_member', $request->input('id_member'))
                        ->sum('qty');
        if ($qty != "") {
            $hasil['keranjang'] = $qty;
        } else {
            $hasil['keranjang'] = "0";
        }

        return $hasil;
    }

    public function setdetailproduk($request) {
        $hasil['is_success'] = false;
        $hasil['dimana'] = 'dua';
        $produk = DB::table('produk')
                    ->select('id', 'nama', 'deskripsi', 'meta_url')
                    ->where('id', '=', $request->input('id'))
                    ->first();
        if (!is_null($produk)) {
            $hasil['is_success'] = true;
            $hasil['dimana'] = 'tiga';
            $hasil['id'] = $produk->id;
            $hasil['nama'] = $produk->nama;
            $hasil['deskripsi'] = $produk->deskripsi;
            $hasil['meta_url'] = $produk->meta_url;
            $harga = DB::table('produk_harga')
                        ->select('harga', 'qty')
                        ->where('id_produk', '=', $request->input('id'))
                        ->orderBy('qty', 'asc')
                        ->first();
            $hasil['harga'] = $harga->harga;
            $hasil['qty_min'] = $harga->qty;
            $totalSkor = DB::table('produk_review')
                        ->select(DB::raw('SUM(skor) as skor'))
                        ->where('id_produk', '=', $request->input('id'))
                        ->first();
            $hasil['total_skor'] = $totalSkor->skor;
            $jumlahReview = DB::table('produk_review')
                        ->select(DB::raw('COUNT(id) as jumlah'))
                        ->where('id_produk', '=', $request->input('id'))
                        ->first();
            $hasil['jumlah_review'] = $jumlahReview->jumlah;
            $hasil['skor_review'] = $totalSkor->skor / $jumlahReview->jumlah;
            $review = DB::table('produk_review')
                        ->select('id_member', 'review', 'skor')
                        ->where('id_produk', '=', $request->input('id'))
                        ->orderBy(DB::raw('RAND()'))
                        ->first();
            if (!is_null($review)) {
                $hasil['review_teks'] = $review->review;
                $hasil['review_skor'] = $review->skor;
                $member = DB::table('member')
                        ->select('nama', 'foto_profile')
                        ->where('id', '=', $review->id_member)
                        ->first();
                $hasil['review_nama'] = $member->nama;
                $hasil['review_foto'] = $member->foto_profile;
            }

            $produkImages = DB::table('galeri')
                    ->select('id', 'nama_file')
                    ->where('kategori', '=', 'produk')
                    ->where('tipe', '=', 'image')
                    ->where('status', '=', 'aktif')
                    ->where('id_produk', '=', $request->input('id'))
                    ->get();
            if (!is_null($produkImages)) {
                $i = 0;
                foreach ($produkImages as $image) {
                    $hasil['galeri'][$i]['nama_file'] = $image->nama_file;
                    $i++;
                }
            }
        }
        return $hasil;
    }

    public function setsimpankeranjang($request) {
        $hasil['is_success'] = false;
        $hasil['dimana'] = "dua";
        $harga = DB::table('produk_harga')
                        ->select('harga')
                        ->where('id_produk', '=', $request->input('id'))
                        ->orderBy('qty', 'asc')
                        ->first();
        $harga_total = $harga->harga * $request->input('qty');
        if ($request->input('session') == "") {
            $hasil['dimana'] = "tiga";
            $session = md5(sha1(date('Ymdhis'),$request->input('member')));
        } else {
            $hasil['dimana'] = "empat";
            $session = $request->input('session');
        }
        $cek = DB::table('keranjang')
                        ->select('id_produk', 'id_member')
                        ->where('id_produk', '=', $request->input('id'))
                        ->where('id_member', '=', $request->input('member'))
                        ->first();
        if (is_null($cek)) {
            $hasil['dimana'] = "lima";
            $hasil['is_success'] = true;
            $hasil['session'] = $session;
            $insert = DB::table('keranjang')
                        ->insertGetId([
                            'session'       => $session,
                            'id_produk'     => $request->input('id'),
                            'qty'           => $request->input('qty'),
                            'harga_total'   => $harga_total,
                            'id_member'     => $request->input('member')
                        ]);
        } else {
            $hasil['dimana'] = "enam";
            $hasil['is_success'] = true;
            $hasil['session'] = $session;
            $insert = DB::table('keranjang')
                    ->where('id_produk', $request->input('id'))
                    ->where('id_member', $request->input('member'))
                    ->update([
                        'qty'           => $request->input('qty'),
                        'harga_total'   => $harga_total
                    ]);
        }
        /*if($insert){
            $hasil['dimana'] = "tujuh";
            $hasil['session'] = $session;
            $hasil['is_success'] = true;
        }*/

        return $hasil;
    }

    public function sendForgetPass($request){
        Config::set('mail.mailers.smtp.username', helper::getSetting('conf_email'));
        Config::set('mail.mailers.smtp.password', helper::getSetting('conf_pass'));
        Config::set('mail.from.address', helper::getSetting('conf_email'));
        Config::set('mail.from.name', "KLOP");
        $hasil['is_success'] = false;
        $dateTime = Carbon::now();
        $waktu_selesai = $dateTime->addHours(1);
        $data = DB::table('member')
                    ->select('id')
                    ->where('email', '=', $request->input('email'))
                    ->first();
        if (!is_null($data)) {
            $token = md5(uniqid('forgetPass-'.$data->id, true)).md5(uniqid('forgetPass-'.$data->id, true));
            DB::table('member_forgot_password')
                ->insert([
                    'id_member' => $data->id,
                    'email'     => $request->input('email'),
                    'token'     => $token,
                    'waktu'     => $dateTime,
                    'waktu_selesai' => $waktu_selesai,
                    'status'        => 'belum'              
                ]);
            \Mail::to($request->input('email'))
            ->send(new \App\Mail\ResetPassword($token, $request->input('email')));
            DB::table('member_forgot_password')
                ->where('token', $token)
                ->update([
                    'status' => 'terkirim'
                ]);
                $hasil['is_success'] = true;
        }

        return $hasil;
    }

    public function setprecheckout($request) {
        $hasil['is_success'] = false;
        $alamat_ambil = DB::table('default_setting')
                    ->where('nama', '=', 'alamat')
                    ->select('nilai')
                    ->first();
        $hasil['alamat_ambil'] = $alamat_ambil->nilai;
        $proyeks = DB::table('proyek_kategori')
                    ->select('id', 'nama')
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($proyeks)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($proyeks as $proyek) {
                $hasil['proyek'][$i]['id'] = $proyek->id;
                $hasil['proyek'][$i]['nama'] = $proyek->nama;
                $i++;
            }

            $qty = DB::table('keranjang')
                        ->where('id_member', $request->input('member'))
                        ->sum('qty');
            $instalasi = DB::table('biaya_installation')
                            ->where('status', 'aktif')
                            ->whereRaw(" '".$qty."' BETWEEN min_qty_produk AND max_qty_produk")
                            ->select('harga','id')
                            ->first();
            if(is_null($instalasi)){
                $instalasi = DB::table('biaya_installation')
                        ->where('status', 'aktif')
                        ->where('max_qty_produk', '<', $qty)
                        ->select('harga', 'id')
                        ->orderBy('max_qty_produk', 'desc')
                        ->first();
                $hasil['instalasi_harga'] = $instalasi->harga;
                $hasil['instalasi_id'] = $instalasi->id;
            } else {
                $hasil['instalasi_harga'] = $instalasi->harga;
                $hasil['instalasi_id'] = $instalasi->id;
            }

            $harga = DB::table('keranjang')
                        ->where('id_member', $request->input('member'))
                        ->sum('harga_total');
            $hasil['harga_total'] = $harga;
            $hasil['id_alamat'] = $request->input('id_alamat');

            if (($request->input('id_alamat') != "") && ($request->input('id_alamat') != null) && ($request->input('id_alamat') != "null")) {
                $alamat = DB::table('member_alamat')
                            ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                            ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                            ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                            ->where('member_alamat.id', $request->input('id_alamat'))
                            ->select('member_alamat.alamat', 'kecamatan.nama as kecamatan', 'kabupaten.nama as kabupaten', 'provinsi.nama as provinsi', 'member_alamat.koordinat')
                            ->first();
                $hasil['alamat'] = $alamat->alamat.' '.ucfirst(strtolower($alamat->kecamatan)).', '.ucfirst(strtolower($alamat->kabupaten)).', '.ucfirst(strtolower($alamat->provinsi));
                $origin = Helper::getSetting('koordinat_toko');
                $destination = str_replace(' ','',$alamat->koordinat);
                $jarak = $this->countDistanceTwoPointsMaps($origin, $destination);
                // DB::enableQueryLog();
                $perkem = DB::table('biaya_delivery')
                                ->where('status', 'aktif')
                                ->whereRaw(" '".$qty."' BETWEEN min_qty_produk AND max_qty_produk")
                                ->select('harga_per_km','id')
                                ->first();
                if(is_null($perkem)){
                    $perkem = DB::table('biaya_delivery')
                        ->where('status', 'aktif')
                        ->where('max_qty_produk', '<', $qty)
                        ->select('harga_per_km', 'id')
                        ->orderBy('max_qty_produk', 'desc')
                        ->first();
                }
                $hasil['ongkos_kirim'] = ceil($perkem->harga_per_km * $jarak);
            } else {
                $alamat = DB::table('member_alamat')
                            ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                            ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                            ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                            ->where('member_alamat.id_member', $request->input('member'))
                            ->select('member_alamat.alamat', 'kecamatan.nama as kecamatan', 'kabupaten.nama as kabupaten', 'provinsi.nama as provinsi', 'member_alamat.koordinat', 'member_alamat.id')
                            ->first();
                if (!is_null($alamat)) {
                    $hasil['alamat'] = $alamat->alamat.' '.ucfirst(strtolower($alamat->kecamatan)).', '.ucfirst(strtolower($alamat->kabupaten)).', '.ucfirst(strtolower($alamat->provinsi));
                    $hasil['id_alamat'] = $alamat->id;
                }
            }

            $keranjangs = DB::table('keranjang')
                            ->join('produk', 'keranjang.id_produk', '=', 'produk.id')
                            ->where('keranjang.id_member', $request->input('member'))
                            ->select('produk.nama','keranjang.qty', 'produk.id', 'keranjang.harga_total')
                            ->get();
            $i = 0;
            foreach($keranjangs as $keranjang) {
                $hasil['keranjang'][$i]['id'] = $keranjang->id;
                $hasil['keranjang'][$i]['nama'] = $keranjang->nama;
                $hasil['keranjang'][$i]['qty'] = $keranjang->qty;
                $hasil['keranjang'][$i]['harga_total'] = $keranjang->harga_total;
                $gambar = DB::table('produk_galeri')
                    ->where('id_produk', '=', $keranjang->id)
                    ->select('nama_file')
                    ->first();
                $hasil['keranjang'][$i]['foto'] = $gambar->nama_file;
                $i++;
            }
            
        }

        return $hasil;
    }

    public function setgetprovinsi($request) {
        $hasil['is_success'] = false;
        $provinsis = DB::table('provinsi')
                    ->select('id', 'nama')
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($provinsis)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($provinsis as $provinsi) {
                $hasil['provinsi'][$i]['id'] = $provinsi->id;
                $hasil['provinsi'][$i]['nama'] = $provinsi->nama;
                $i++;
            }
        }

        return $hasil;
    }

    public function setgetkabupaten($request) {
        $hasil['is_success'] = false;
        $kabupatens = DB::table('kabupaten')
                    ->select('id', 'nama')
                    ->where('id_prov', '=', $request->input('provinsi'))
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($kabupatens)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($kabupatens as $kabupaten) {
                $hasil['kabupaten'][$i]['id'] = $kabupaten->id;
                $hasil['kabupaten'][$i]['nama'] = $kabupaten->nama;
                $i++;
            }
        }

        return $hasil;
    }

    public function setgetkecamatan($request) {
        $hasil['is_success'] = false;
        $kecamatans = DB::table('kecamatan')
                    ->select('id', 'nama')
                    ->where('id_kab', '=', $request->input('kabupaten'))
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($kecamatans)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($kecamatans as $kecamatan) {
                $hasil['kecamatan'][$i]['id'] = $kecamatan->id;
                $hasil['kecamatan'][$i]['nama'] = $kecamatan->nama;
                $i++;
            }
        }

        return $hasil;
    }

    public function setsimpanalamat($request) {
        $hasil['is_success'] = false;

        ////////////// KOORDINAT /////////////
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->input('alamat'))."&sensor=true_or_false&key=".Helper::getSetting('maps_api');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xml = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($xml, true);
        if($response_a['status'] == "OK"){
            $koordinat  = $response_a['results'][0]['geometry']['location']['lat'].",".$response_a['results'][0]['geometry']['location']['lng'];
        }else{
            $koordinat = null;
        }
        //////////////////////////////////////
        if (($request->input('id') == "") || ($request->input('id') == 0)) {
            $insert = DB::table('member_alamat')
                    ->insertGetId([
                        'id_member'     => $request->input('id_member'),
                        'nama'          => $request->input('nama'),
                        'alamat'        => $request->input('alamat'),
                        'kecamatan'     => $request->input('kecamatan'),
                        'kabupaten'     => $request->input('kabupaten'),
                        'provinsi'      => $request->input('provinsi'),
                        'koordinat'     => $koordinat,
                        'catatan'       => $request->input('catatan'),
                        'status'        => 'aktif'
                ]);
            $hasil['id_alamat'] = $insert;
        } else {
            $insert = DB::table('member_alamat')
                    ->where('id', $request->input('id'))
                    ->update([
                        'id_member'     => $request->input('id_member'),
                        'nama'          => $request->input('nama'),
                        'alamat'        => $request->input('alamat'),
                        'kecamatan'     => $request->input('kecamatan'),
                        'kabupaten'     => $request->input('kabupaten'),
                        'provinsi'      => $request->input('provinsi'),
                        'koordinat'     => $koordinat,
                        'catatan'       => $request->input('catatan')
                    ]);
        }
        if($insert){
            $hasil['is_success'] = true;
        }

        return $hasil;
    }

    public function setsimpanorder($request) {
        $hasil['is_success'] = false;
        $hasil['dimana'] = "dua";
        $date = Carbon::now();
        $id = "KP";
        $hasil['id'] = $id;
        $last = DB::table('order')
                    ->whereRaw("date(waktu_order)='".$date->format('Y-m-d')."'")
                    ->selectRaw('cast(substr(id, 12) as SIGNED) as num')
                    ->orderByRaw('cast(substr(id, 12) as SIGNED) DESC')
                    ->limit(1)
                    ->first();
        if(is_null($last)){
            $num = 1;
        }else{
            $num = $last->num + 1;
        }
        $id .= $date->format('Ymd')."-".$num;
        $hasil['idOrder'] = $id;

        $insert = DB::table('order')
                    ->insert([
                        'id'                    => $id,
                        'id_member'             => $request->input('id_member'),
                        'waktu_order'           => date('Y-m-d H:i:s'),
                        'delivery'              => $request->input('delivery'),
                        'request_ready'         => date('Y-m-d H:i:s'),
                        'alamat_kirim'          => $request->input('alamat'),
                        'id_proyek'             => $request->input('id_proyek'),
                        'id_delivery'           => $request->input('id_delivery'),
                        'nilai_delivery'        => $request->input('nilai_delivery'),
                        'id_installation'       => $request->input('id_installation'),
                        'nilai_installation'    => $request->input('nilai_installation'),
                        'installation'          => $request->input('installation')
                ]);
        if($insert){
            $hasil['is_success'] = true;
            $hasil['dimana'] = "tiga";
            $status = DB::table('order_status')->insert([
                        'id_order'          => $id,
                        'waktu_perubahan'   => date('Y-m-d H:i:s'),
                        'status'            => 'menunggu pembayaran',
                        'id_admin'          => '0'
                ]);
            $keranjangs = DB::table('keranjang')
                    ->select('id_produk', 'qty', 'harga_total')
                    ->where('id_member', '=', $request->input('id_member'))
                    ->get();
            $totalHarga = 0;
            foreach ($keranjangs as $keranjang) {
                $simpanproduk = DB::table('order_produk')->insert([
                                    'id_order'              => $id,
                                    'id_produk'             => $keranjang->id_produk,
                                    'qty'                   => $keranjang->qty,
                                    'harga_total'           => $keranjang->harga_total,
                                    'status'                => 'aktif'
                                ]);
                if ($simpanproduk) {
                    $totalHarga += $keranjang->harga_total;
                    DB::table('keranjang')
                        ->where('id_produk', $keranjang->id_produk)
                        ->where('id_member', $request->input('id_member'))
                        ->delete();
                }
            }
            if ($request->input('delivery') == "ya") {
                $totalHarga += $request->input('nilai_delivery');
            }
            if ($request->input('installation') == "ya") {
                $totalHarga += $request->input('nilai_installation');
            }
            $hasil['total_harga'] = $totalHarga;
            $nama_bank = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nama_bank')
                    ->first();
            $hasil['nama_bank'] = $nama_bank->nilai;
            $logo_bank = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'logo_bank')
                    ->first();
            $hasil['logo_bank'] = $logo_bank->nilai;
            $nama_rekening = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nama_rekening')
                    ->first();
            $hasil['nama_rekening'] = $nama_rekening->nilai;
            $nomor_rekening = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nomor_rekening')
                    ->first();
            $hasil['nomor_rekening'] = $nomor_rekening->nilai;

        }

        return $hasil;
    }

    public function setgetongkoskirim($request) {
        $hasil['is_success'] = true;
        $qty = DB::table('keranjang')
                        ->where('id_member', $request->input('member'))
                        ->sum('qty');
        $dataAlamat = DB::table('member_alamat')
                        ->where('id', $request->input('id_alamat'))
                        ->select('koordinat')
                        ->first();
        $origin = Helper::getSetting('koordinat_toko');
        $destination = str_replace(' ','',$dataAlamat->koordinat);
        $jarak = $this->countDistanceTwoPointsMaps($origin, $destination);
        // DB::enableQueryLog();
        $perkem = DB::table('biaya_delivery')
                        ->where('status', 'aktif')
                        ->whereRaw(" '".$qty."' BETWEEN min_qty_produk AND max_qty_produk")
                        ->select('harga_per_km','id')
                        ->first();
        $hasil['dimana'] = "dua";
        if(is_null($perkem)){
            $perkem = DB::table('biaya_delivery')
                ->where('status', 'aktif')
                ->where('max_qty_produk', '<', $qty)
                ->select('harga_per_km', 'id')
                ->orderBy('max_qty_produk', 'desc')
                ->first();
            $hasil['dimana'] = "tiga";
        }  
        $hasil['id'] = $perkem->id;
        $hasil['ongkos_kirim'] = ceil($perkem->harga_per_km * $jarak);

        return $hasil;
    }

    public function countDistanceTwoPointsMaps($origin, $destination){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origin."&destinations=".$destination."&mode=driving&key=".Helper::getSetting('maps_api');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        if (is_null($response_a['rows'][0]['elements'][0]['distance']['value'])) {
            $dist = 0;
        }else{
            $dist = round($response_a['rows'][0]['elements'][0]['distance']['value']/1000, 1);
        }
        return $dist;
        // print('<pre>'); print_r($response_a);
    }

    public function setgetdaftaralamat($request) {
        $hasil['is_success'] = true;
        $alamats = DB::table('member_alamat')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('member_alamat.id_member', '=', $request->input('id_member'))
                    ->where('member_alamat.status', '<>', "delete")
                    ->select('member_alamat.alamat', 'kecamatan.nama as kecamatan', 'kabupaten.nama as kabupaten', 'provinsi.nama as provinsi', 'member_alamat.koordinat', 'member_alamat.id', 'member_alamat.nama')
                    ->get();
        $i = 0;
        foreach($alamats as $alamat) {
            $hasil['address'][$i]['id'] = $alamat->id;
            $hasil['address'][$i]['nama'] = $alamat->nama;
            $hasil['address'][$i]['alamat'] = $alamat->alamat.' '.ucfirst(strtolower($alamat->kecamatan)).', '.ucfirst(strtolower($alamat->kabupaten)).', '.ucfirst(strtolower($alamat->provinsi));
            $i++;

        }

        return $hasil;
    }

    public function setgetdataalamat($request) {
        $hasil['is_success'] = true;
        $alamat = DB::table('member_alamat')
                    ->where('id', '=', $request->input('id'))
                    ->select('alamat', 'kecamatan', 'kabupaten', 'provinsi', 'koordinat', 'nama', 'catatan')
                    ->first();
        
        $hasil['nama'] = $alamat->nama;
        $hasil['alamat'] = $alamat->alamat;
        $hasil['kecamatan'] = $alamat->kecamatan;
        $hasil['kabupaten'] = $alamat->kabupaten;
        $hasil['provinsi'] = $alamat->provinsi;
        $hasil['koordinat'] = $alamat->koordinat;
        $hasil['catatan'] = $alamat->catatan;

        $provinsis = DB::table('provinsi')
                    ->select('id', 'nama')
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($provinsis)) {
            $i = 0;
            foreach ($provinsis as $provinsi) {
                $hasil['daftar_provinsi'][$i]['id'] = $provinsi->id;
                $hasil['daftar_provinsi'][$i]['nama'] = $provinsi->nama;
                $i++;
            }
        }

        $kabupatens = DB::table('kabupaten')
                    ->select('id', 'nama')
                    ->where('id_prov', '=', $alamat->provinsi)
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($kabupatens)) {
            $i = 0;
            foreach ($kabupatens as $kabupaten) {
                $hasil['daftar_kabupaten'][$i]['id'] = $kabupaten->id;
                $hasil['daftar_kabupaten'][$i]['nama'] = $kabupaten->nama;
                $i++;
            }
        }

        $kecamatans = DB::table('kecamatan')
                    ->select('id', 'nama')
                    ->where('id_kab', '=', $alamat->kabupaten)
                    ->where('status', '=', 'aktif')
                    ->get();
        if (!is_null($kecamatans)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach ($kecamatans as $kecamatan) {
                $hasil['daftar_kecamatan'][$i]['id'] = $kecamatan->id;
                $hasil['daftar_kecamatan'][$i]['nama'] = $kecamatan->nama;
                $i++;
            }
        }

        return $hasil;
    }

    public function setgetcontent($request) {
        $hasil['is_success'] = true;
        $konten = DB::table('konten')
                    ->where('id', '=', $request->input('id'))
                    ->select('title', 'konten')
                    ->first();
        
        $hasil['title'] = $konten->title;
        $hasil['konten'] = $konten->konten;

        $alamat = DB::table('default_setting')
                    ->where('nama', '=', 'alamat')
                    ->select('nilai')
                    ->first();

        $hasil['alamat'] = $alamat->nilai;

        $notelp = DB::table('default_setting')
                    ->where('nama', '=', 'nomor_telepon')
                    ->select('nilai')
                    ->first();

        $hasil['no_telp'] = $notelp->nilai;

        $email = DB::table('default_setting')
                    ->where('nama', '=', 'email')
                    ->select('nilai')
                    ->first();

        $hasil['email'] = $email->nilai;

        return $hasil;
    }

    public function setgetdaftarkeranjang($request) {
        $hasil['is_success'] = true;
        $keranjangs = DB::table('keranjang')
                            ->join('produk', 'keranjang.id_produk', '=', 'produk.id')
                            ->where('keranjang.id_member', $request->input('id_member'))
                            ->select('produk.nama','keranjang.qty', 'produk.id', 'keranjang.harga_total')
                            ->get();
        $i = 0;
        $total = 0;
        foreach($keranjangs as $keranjang) {
            $hasil['keranjang'][$i]['id'] = $keranjang->id;
            $hasil['keranjang'][$i]['nama'] = $keranjang->nama;
            $hasil['keranjang'][$i]['qty'] = $keranjang->qty;
            $hasil['keranjang'][$i]['harga_total'] = $keranjang->harga_total;
            $gambar = DB::table('produk_galeri')
                    ->where('id_produk', '=', $keranjang->id)
                    ->select('nama_file')
                    ->first();
            $hasil['keranjang'][$i]['foto'] = $gambar->nama_file;
            $total += $keranjang->harga_total;
            $i++;
        }
        $hasil['total_harga'] = $total;

        return $hasil;
    }

    public function deleteKeranjang($request) {
        $hasil['is_success'] = true;
        DB::table('keranjang')
            ->where('id_produk', $request->input('id_produk'))
            ->where('id_member', $request->input('id_member'))
            ->delete();

        return $hasil;
    }

    public function ambilriwayat($request) {
        $hasil['is_success'] = true;
        $orders = DB::table('order')
                    ->where('id_member', $request->input('id_member'))
                    ->select('id','waktu_order', 'nilai_delivery', 'nilai_installation', 'delivery', 'installation')
                    ->orderBy('waktu_order', 'desc')
                    ->get();
        $i = 0;
        foreach($orders as $order) {
            $total = 0;
            $status = DB::table('order_status')
                    ->where('id_order', '=', $order->id)
                    ->select('status')
                    ->orderBy('waktu_perubahan', 'desc')
                    ->first();
            if (($request->input("status") == "all") || ($request->input("status") == $status->status)) {
                $hasil['riwayat'][$i]['id'] = $order->id;
                $hasil['riwayat'][$i]['waktu_order'] = date('d M Y H:i', strtotime($order->waktu_order));
                $hasil['riwayat'][$i]['status'] = ucwords($status->status);
                switch ($status->status) {
                    case 'menunggu pembayaran':
                        $icon_status = "menunggu_pembayaran.png";
                        break;
                    case 'menunggu konfirmasi':
                        $icon_status = "menunggu_konfirmasi.png";
                        break;
                    case 'diproses':
                        $icon_status = "diproses.png";
                        break;
                    case 'dikirim':
                        $icon_status = "dikirim.png";
                        break;
                    case 'sampai ditujuan':
                        $icon_status = "sampai_tujuan.png";
                        break;
                    case 'dikomplain':
                        $icon_status = "dikomplain.png";
                        break;
                    case 'dibatalkan':
                        $icon_status = "dibatalkan.png";
                        break;
                    default:
                        $icon_status = "selesai.png";
                        break;
                }
                $hasil['riwayat'][$i]['icon_status'] = $icon_status;
                $produk = DB::table('order_produk')
                        ->where('id_order', '=', $order->id)
                        ->select('id_produk')
                        ->first();
                $gambar = DB::table('produk_galeri')
                        ->where('id_produk', '=', $produk->id_produk)
                        ->select('nama_file')
                        ->first();
                $hasil['riwayat'][$i]['foto'] = $gambar->nama_file;
                $harga = DB::table('order_produk')
                            ->where('id_order', '=', $order->id)
                            ->sum('harga_total');
                $total += $harga;
                if ($order->delivery == "ya") {
                    $total += $order->nilai_delivery;
                }
                if ($order->installation == "ya") {
                    $total += $order->nilai_installation;
                }
                $hasil['riwayat'][$i]['harga_total'] = $total;
            $i++;
            }
        }

        return $hasil;
    }

    public function getdetailtransaksi($request) {
        $hasil['is_success'] = true;
        $total = 0;
        $status = DB::table('order_status')
                    ->where('id_order', '=', $request->input('id_order'))
                    ->select('status')
                    ->orderBy('waktu_perubahan', 'desc')
                    ->first();
        $hasil['status'] = ucwords($status->status);
        switch ($status->status) {
            case 'menunggu pembayaran':
                $icon_status = "menunggu_pembayaran.png";
                break;
            case 'menunggu konfirmasi':
                $icon_status = "menunggu_konfirmasi.png";
                break;
            case 'diproses':
                $icon_status = "diproses.png";
                break;
            case 'dikirim':
                $icon_status = "dikirim.png";
                break;
            case 'sampai ditujuan':
                $icon_status = "sampai_tujuan.png";
                break;
            case 'dikomplain':
                $icon_status = "dikomplain.png";
                break;
            case 'dibatalkan':
                $icon_status = "dibatalkan.png";
                break;
            default:
                $icon_status = "instalasi.png";
                break;
        }
        $hasil['icon_status'] = $icon_status;
        $order = DB::table('order')
                    ->where('id', $request->input('id_order'))
                    ->select('id','waktu_order', 'nilai_delivery', 'nilai_installation', 'delivery', 'installation', 'id_member', 'alamat_kirim')
                    ->first();
        $harga = DB::table('order_produk')
                    ->where('id_order', '=', $request->input('id_order'))
                    ->sum('harga_total');
        $total += $harga;
        $hasil['total_harga'] = $total;
        $hasil['ongkos_kirim'] = $order->nilai_delivery;
        $hasil['biaya_pemasangan'] = $order->nilai_installation;
        
        if ($order->delivery == "ya") {
            $hasil['delivery'] = "Dikirim";
            $hasil['title_alamat'] = "Alamat Pengiriman";
            $total += $order->nilai_delivery;
            $ambil = DB::table('member_alamat')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('member_alamat.id', '=', $order->alamat_kirim)
                    ->select('member_alamat.alamat', 'kecamatan.nama as kecamatan', 'kabupaten.nama as kabupaten', 'provinsi.nama as provinsi', 'member_alamat.koordinat', 'member_alamat.id', 'member_alamat.nama')
                    ->first();
            $hasil['nama'] = $ambil->nama;
            $hasil['alamat'] = $ambil->alamat.' '.ucwords(strtolower($ambil->kecamatan)).', '.ucwords(strtolower($ambil->kabupaten)).', '.ucwords(strtolower($ambil->provinsi));
        } else {
            $hasil['delivery'] = "Diambil Sendiri";
            $hasil['title_alamat'] = "Alamat Pengambilan";
            $nama_ambil = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nama')
                    ->first();
            $hasil['nama'] = $nama_ambil->nilai;
            $alamat_ambil = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'alamat')
                    ->first();
            $hasil['alamat'] = $alamat_ambil->nilai;
        }
        if ($order->installation == "ya") {
            $hasil['pemasangan'] = "Ya";
            $total += $order->nilai_installation;
        } else {
            $hasil['pemasangan'] = "Tidak";
        }
        $hasil['harga_total'] = $total;
        $nama_bank = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nama_bank')
                    ->first();
        $hasil['nama_bank'] = $nama_bank->nilai;
        $logo_bank = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'logo_bank')
                    ->first();
        $hasil['logo_bank'] = $logo_bank->nilai;
        $nama_rekening = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nama_rekening')
                    ->first();
        $hasil['nama_rekening'] = $nama_rekening->nilai;
        $nomor_rekening = DB::table('default_setting')
                    ->select('nilai')
                    ->where('nama', 'nomor_rekening')
                    ->first();
        $hasil['nomor_rekening'] = $nomor_rekening->nilai;
        $produks = DB::table('order_produk')
                    ->join('produk', 'order_produk.id_produk', '=', 'produk.id')
                    ->where('order_produk.id_order', '=', $request->input('id_order'))
                    ->select('order_produk.id_produk', 'order_produk.qty', 'order_produk.harga_total', 'produk.nama')
                    ->get();
        $i = 0;
        foreach($produks as $produk) {
            $hasil['detail'][$i]['qty'] = $produk->qty;
            $hasil['detail'][$i]['nama'] = $produk->nama;
            $hasil['detail'][$i]['harga_total'] = $produk->harga_total;
            $gambar = DB::table('produk_galeri')
                    ->where('id_produk', '=', $produk->id_produk)
                    ->select('nama_file')
                    ->first();
            $hasil['detail'][$i]['foto'] = $gambar->nama_file;
            $i++;
        }

        return $hasil;
    }

    public function canceltransaksi($request) {
        $hasil['is_success'] = true;
        $insert = DB::table('order_status')
                    ->insert([
                        'id_order'              => $request->input('id_order'),
                        'waktu_perubahan'       => date('Y-m-d H:i:s'),
                        'status'                => 'dibatalkan',
                        'id_admin'              => '0'
                ]);

        return $hasil;
    }

    public function deletealamat($request) {
        $hasil['is_success'] = false;
        $update = DB::table('member_alamat')
                    ->where('id', $request->input('id_alamat'))
                    ->update([
                        'status'  => "delete"
                    ]);
        if($update){
            $hasil['is_success'] = true;
        }

        return $hasil;
    }

    public function orderkembali($request) {
        $hasil['is_success'] = true;
        $hasil['id_order'] = $request->input('id_order');
        $hasil['id_member'] = $request->input('id_member');
        // md5(sha1())
        $session = md5(sha1(date('Ymdhis').$request->input('id_member')));
        $produks = DB::table('order_produk')
                    ->where('id_order', '=', $request->input('id_order'))
                    ->select('id_produk', 'qty',)
                    ->get();
        $i = 0;
        foreach($produks as $produk) {
            $harga = DB::table('produk_harga')
                        ->select('harga')
                        ->where('id_produk', '=', $produk->id_produk)
                        ->orderBy('qty', 'asc')
                        ->first();
            $harga_total = $harga->harga * $produk->qty;
            $cek = DB::table('keranjang')
                    ->where('id_produk', '=', $produk->id_produk)
                    ->where('id_member', '=', $request->input('id_member'))
                    ->select('qty')
                    ->first();
            if (is_null($cek)) {
                $insert = DB::table('keranjang')->insert([
                            'session'       => $session,
                            'id_produk'     => $produk->id_produk,
                            'qty'           => $produk->qty,
                            'harga_total'   => $harga_total,
                            'id_member'     => $request->input('id_member')
                ]);
            } else {
                $update = DB::table('keranjang')
                            ->where('id_produk', $produk->id_produk)
                            //->where('id_member', '=', $request->input('id_member'))
                            ->update(['qty'  => $produk->qty]);
            }
        }

        return $hasil;
    }

    public function getdaftartestimoni($request) {
        $hasil['is_success'] = true;
        if ($request->input('id_produk') != "0") {
            if ($request->input('bintang') == 0) {
                $testimonis = DB::table('produk_review')
                            ->join('member', 'produk_review.id_member', '=', 'member.id')
                            ->where('produk_review.id_produk', $request->input('id_produk'))
                            ->select('produk_review.review','produk_review.skor', 'produk_review.waktu', 'member.nama')
                            ->get();
            } else {
                $testimonis = DB::table('produk_review')
                            ->join('member', 'produk_review.id_member', '=', 'member.id')
                            ->where('produk_review.id_produk', $request->input('id_produk'))
                            ->where('produk_review.skor', $request->input('bintang'))
                            ->select('produk_review.review','produk_review.skor', 'produk_review.waktu', 'member.nama')
                            ->get();
            }
            $sum_1 = DB::table('produk_review')
                            ->where('skor', '1')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $sum_2 = DB::table('produk_review')
                            ->where('skor', '2')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $sum_3 = DB::table('produk_review')
                            ->where('skor', '3')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $sum_4 = DB::table('produk_review')
                            ->where('skor', '4')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $sum_5 = DB::table('produk_review')
                            ->where('skor', '5')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $sum = DB::table('produk_review')
                            ->where('id_produk', $request->input('id_produk'))
                            ->count();
            $bintang = DB::table('produk_review')
                            ->where('id_produk', $request->input('id_produk'))
                            ->sum('skor');
        } else {
            if ($request->input('bintang') == 0) {
                $testimonis = DB::table('produk_review')
                            ->join('member', 'produk_review.id_member', '=', 'member.id')
                            ->select('produk_review.review','produk_review.skor', 'produk_review.waktu', 'member.nama')
                            ->get();
            } else {
                $testimonis = DB::table('produk_review')
                            ->join('member', 'produk_review.id_member', '=', 'member.id')
                            ->where('produk_review.skor', $request->input('bintang'))
                            ->select('produk_review.review','produk_review.skor', 'produk_review.waktu', 'member.nama')
                            ->get();
            }
            $sum = DB::table('produk_review')
                            ->count();
            $sum_1 = DB::table('produk_review')
                            ->where('skor', '1')
                            ->count();
            $sum_2 = DB::table('produk_review')
                            ->where('skor', '2')
                            ->count();
            $sum_3 = DB::table('produk_review')
                            ->where('skor', '3')
                            ->count();
            $sum_4 = DB::table('produk_review')
                            ->where('skor', '4')
                            ->count();
            $sum_5 = DB::table('produk_review')
                            ->where('skor', '5')
                            ->count();
            $bintang = DB::table('produk_review')
                            ->sum('skor');
        }
        $i = 0;
        foreach($testimonis as $testimoni) {
            $hasil['testimoni'][$i]['review'] = $testimoni->review;
            $hasil['testimoni'][$i]['skor'] = $testimoni->skor.".00";
            $hasil['testimoni'][$i]['waktu'] = date('d M Y, H:i', strtotime($testimoni->waktu));
            $hasil['testimoni'][$i]['nama'] = $testimoni->nama;
            $i++;
        }
        $hasil['jumlah'] = $sum;
        $hasil['jumlah1'] = $sum_1;
        $hasil['jumlah2'] = $sum_2;
        $hasil['jumlah3'] = $sum_3;
        $hasil['jumlah4'] = $sum_4;
        $hasil['jumlah5'] = $sum_5;
        $persen_1 = number_format(($sum_1/$sum), 2, '.', '.');
        $hasil['persen1'] = $persen_1;
        $persen_2 = number_format(($sum_2/$sum), 2, '.', '.');
        $hasil['persen2'] = $persen_2;
        $persen_3 = number_format(($sum_3/$sum), 2, '.', '.');
        $hasil['persen3'] = $persen_3;
        $persen_4 = number_format(($sum_4/$sum), 2, '.', '.');
        $hasil['persen4'] = $persen_4;
        $persen_5 = number_format(($sum_5/$sum), 2, '.', '.');
        $hasil['persen5'] = $persen_5;
        $hasil['bintang'] = number_format(($bintang/$sum), 2, '.', '.');

        return $hasil;
    }

    public function getdaftargaleri($request) {
        $hasil['is_success'] = true;
        $galeries = DB::table('galeri')
                    ->where('status', "aktif")
                    ->where('kategori', $request->input("kategori"))
                    ->where('tipe', $request->input("tipe"))
                    ->select('nama_file', 'thumbnail')
                    ->get();
        
        $i = 0;
        foreach($galeries as $galeri) {
            $hasil['galeri'][$i]['nama_file'] = $galeri->nama_file;
            $i++;
        }
        $hasil['jumlah'] = $i;

        return $hasil;
    }

    public function getslidegaleri($request) {
        $hasil['is_success'] = true;
        $foto = DB::table('galeri')
                    ->where('nama_file', $request->input('id_foto'))
                    ->select('nama_file', 'kategori')
                    ->first();
        $hasil['galeri'][0]['nama'] = $foto->nama_file;
        $galeries = DB::table('galeri')
                    ->where('status', "aktif")
                    ->where('kategori', $foto->kategori)
                    ->where('tipe', 'image')
                    ->select('nama_file')
                    ->get();
        
        $i = 1;
        foreach($galeries as $galeri) {
            $hasil['galeri'][$i]['nama'] = $galeri->nama_file;
            $i++;
        }

        return $hasil;
    }

    public function getdaftargalerivideo($request) {
        $hasil['is_success'] = true;
        $galeries = DB::table('galeri')
                    ->where('status', "aktif")
                    //->where('kategori', $request->input("kategori"))
                    ->where('tipe', $request->input("tipe"))
                    ->select('nama_file', 'thumbnail', 'id_youtube')
                    ->get();
        
        $i = 0;
        foreach($galeries as $galeri) {
            $hasil['galeri'][$i]['nama_file'] = $galeri->thumbnail;
            $hasil['galeri'][$i]['id_youtube'] = $galeri->id_youtube;
            $i++;
        }
        $hasil['jumlah'] = $i;

        return $hasil;
    }

    public function setProfileNew($name, $id) {
        $hasil['is_success'] = false;
        if(DB::table('member')->where('id', '=', $id)->update(['foto_profile' => $name])){
            $hasil['is_success'] = true;
            $hasil['foto'] = $name;
        }
        return $hasil;

    }

    public function getagenlogin($request){
        $token = Str::random(80);
        $hasil['is_success'] = false;
        $hasil['is_exist'] = false;
        $hasil['is_verified'] = true;

        $data = DB::table('karyawan')
                    ->select('id','email', 'no_hp','status', 'password', 'nama', 'foto_profile')
                    ->where('no_hp', '=', $request->input('username'))
                    ->first();
        if (!is_null($data)) {
            $hasil['is_exist'] = true;
            if ($data->status=="delete") {
                $hasil['is_verified'] = false;
            }else{
                if ($request->input('password') !="" && Hash::check('01X'.$request->input('password').'@#', $data->password)) {
                    $hasil['is_success'] = true;
                    $hasil['id_member'] = $data->id;
                    $hasil['email'] = $data->email;
                    $hasil['no_hp'] = $data->no_hp;
                    $hasil['nama'] = $data->nama;
                    $hasil['foto_profile'] = $data->foto_profile;
                }
            }
        }
        return $hasil;
    }

    public function getagentask($request) {
        $hasil['is_success'] = false;
        if ($request->input("tipe") == "baru") {
            $tasks = DB::table('order_penugasan')
                    ->join('order', 'order_penugasan.id_order', '=', 'order.id')
                    ->join('member', 'order.id_member', '=', 'member.id')
                    ->where('order_penugasan.status', "baru")
                    ->where('order_penugasan.id_karyawan', $request->input("id_agen"))
                    ->select('order_penugasan.id', 'order_penugasan.id_order', 'order_penugasan.kategori', 'order_penugasan.waktu_penugasan', 'member.nama')
                    ->get();
        } else {
            $tasks = DB::table('order_penugasan')
                    ->join('order', 'order_penugasan.id_order', '=', 'order.id')
                    ->join('member', 'order.id_member', '=', 'member.id')
                    ->where('order_penugasan.status', "<>", "baru")
                    ->where('order_penugasan.id_karyawan', $request->input("id_agen"))
                    ->select('order_penugasan.id', 'order_penugasan.id_order', 'order_penugasan.kategori', 'order_penugasan.waktu_penugasan', 'member.nama')
                    ->get();
        }
        if (!is_null($tasks)) {
            $hasil['is_success'] = true;
            $i = 0;
            foreach($tasks as $task) {
                $hasil['task'][$i]['id_order'] = $task->id_order;
                $hasil['task'][$i]['kategori'] = ucfirst($task->kategori);
                $hasil['task'][$i]['waktu_penugasan'] = date('d M Y, H:i', strtotime($task->waktu_penugasan));
                $hasil['task'][$i]['nama'] = $task->nama;
                $hasil['task'][$i]['id'] = $task->id;
                $i++;
            }
        }
        return $hasil;
    }

    public function getagendetailtask($request) {
        $hasil['is_success'] = false;
        $hasil['dimana'] = "dua";

        $data = DB::table('order_penugasan')
                    ->join('order', 'order_penugasan.id_order', '=', 'order.id')
                    ->join('member', 'order.id_member', '=', 'member.id')
                    ->join('member_alamat', 'order.alamat_kirim', '=', 'member_alamat.id')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('order_penugasan.id', $request->input("id_penugasan"))
                    ->select('order_penugasan.id_order', 'order_penugasan.kategori', 'order_penugasan.waktu_penugasan', 'member.nama', 'order.waktu_order', 'member_alamat.alamat', 'kecamatan.nama as kecamatan', 'kabupaten.nama as kabupaten', 'provinsi.nama as provinsi', 'order_penugasan.status', 'order_penugasan.waktu_selesai', 'member_alamat.koordinat')
                    ->first();
        if (!is_null($data)) {
            $hasil['is_success'] = true;
            $hasil['id_order'] = $data->id_order;
            $hasil['kategori'] = ucfirst($data->kategori);
            $hasil['waktu_penugasan'] = date('d M Y, H:i', strtotime($data->waktu_penugasan));
            $hasil['nama'] = $data->nama;
            $hasil['waktu_order'] = date('d M Y, H:i', strtotime($data->waktu_order));
            $hasil['alamat'] = $data->alamat;
            $hasil['kecamatan'] = $data->kecamatan;
            $hasil['kabupaten'] = $data->kabupaten;
            $hasil['provinsi'] = $data->provinsi;
            $hasil['koordinat'] = $data->koordinat;
            $hasil['status'] = ucfirst($data->status);
            $hasil['waktu_selesai'] = date('d M Y, H:i', strtotime($data->waktu_selesai));
            $produks = DB::table('order_produk')
                    ->join('produk', 'order_produk.id_produk', '=', 'produk.id')
                    ->where('order_produk.id_order', '=', $data->id_order)
                    ->select('order_produk.id_produk', 'order_produk.qty', 'order_produk.harga_total', 'produk.nama')
                    ->get();
            $i = 0;
            foreach($produks as $produk) {
                $hasil['detail'][$i]['id_produk'] = $produk->id_produk;
                $hasil['detail'][$i]['qty'] = number_format($produk->qty, 0, '.', '.');
                $hasil['detail'][$i]['harga_total'] = number_format($produk->harga_total, 0, '.', '.');
                $hasil['detail'][$i]['nama'] = $produk->nama;
                $gambar = DB::table('produk_galeri')
                        ->where('id_produk', '=', $produk->id_produk)
                        ->select('nama_file')
                        ->first();
                $hasil['detail'][$i]['nama_file'] = $gambar->nama_file;
                $i++;
            }
        }
        return $hasil;
    }

    public function getagenbiodata($request) {
        $data = DB::table('karyawan')
                ->where('id', $request->input("id_agen"))
                ->select('id', 'nama', 'no_hp', 'email', 'foto_profile')
                ->first();
        $hasil['id'] = $data->id;
        $hasil['nama'] = $data->nama;
        $hasil['no_hp'] = $data->no_hp;
        $hasil['email'] = $data->email;
        $hasil['foto_profile'] = $data->foto_profile;
        return $hasil;
    }

    public function setagenselesai($request) {
        $update = DB::table('order_penugasan')
            ->where('id', $request->input("id_penugasan"))
            ->update([
                'status' => 'selesai',
                'waktu_selesai' => date('Y-m-d H:i:s')
            ]);
        if ($update) {
            $hasil['is_success'] = true;
        }
        return $hasil;
    }
}
