<?php

namespace App\Models;

use App\Helpers\helper as HelpersHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class HomeModel extends Model
{
    use HasFactory;

    public function getdatahome(){
    	$hasil = [];
    	$hasil['galeri']['gambar'] = DB::table('galeri')
    									->select('title', 'nama_file', 'id')
    									->where('tipe', 'image')
    									->where('kategori', 'produk')
    									->where('status', 'aktif')
    									->limit(4)
    									->get();

    	$datavideo = DB::table('galeri')
    					->select('title', 'nama_file', 'id')
						->where('tipe', 'video')
						->where('kategori', 'produk')
    					->where('status', 'aktif')
    					->limit(3)
    					->get();
    	$dataarr = [];
    	foreach ($datavideo as $key => $value) {
    		$nama_file = explode('?', $value->nama_file);
    		$id_video  = substr(end($nama_file), 2);
    		$gambar	   = "http://img.youtube.com/vi/".$id_video."/hqdefault.jpg";
    		$arr['title'] = $value->title;
    		$arr['nama_file'] = $value->nama_file;
    		$arr['id'] = HelpersHelper::cleanText($value->nama_file)."-".$value->id;
    		$arr['url_gambar'] = $gambar; 
    		array_push($dataarr, json_decode(json_encode($arr)));
    	}

    	$hasil['galeri']['video'] = collect($dataarr);

    	$hasil['review'] = DB::table('produk_review')
    							->join('member', 'member.id', '=', 'produk_review.id_member')
    							->select('member.nama','member.foto_profile', 'produk_review.review', 'produk_review.skor')
    							->where('produk_review.status', 'aktif')
    							->orderBy('waktu', 'desc')
    							->limit(5)
    							->get();

		$hasil['galeri']['instalasi'] = $this->getListInstalasi5();
		$hasil['selamat_datang'] = HelpersHelper::getKontenById(1);
		$hasil['slideshow'] = $this->getSlideShow('web');
		$hasil['banner'] = $this->getBanner();

    	// print('<pre>');print_r($hasil);exit();

    	return $hasil;
    }

	public function getBanner()
	{
		$hasil['web'] = [];
		$hasil['mobile'] = [];
		$data = DB::table('banner')
					->where('status', 'aktif')
					->select('gambar', 'kategori')->get();
		$keym = 1;
		$keyw = 1;
		foreach ($data as $key => $value) {
			if($value->kategori == "mobile"){
				$hasil['mobile'][$keym] = $value->gambar;
				$keym++;
			}else if($value->kategori == "web"){
				$hasil['web'][$keyw] = $value->gambar;
				$keyw++;
			}
		}
		return $hasil;
	}

	public function getSlideShow($jenis)
	{
		$data = DB::table('slide_show')
					->where('status', 'ya')
					->where('kategori', $jenis)
					->select('gambar')
					->get();
		return $data;
	}

	public function getListInstalasi5()
	{
		$list = DB::table('galeri')
					->where('kategori', 'instalasi')
					->where('tipe', 'image')
					->where('status', 'aktif')
					->select('nama_file')
					->limit(5)
					->get();
		$array['short'] = [];
		$array['long'] = [];
		// print('<pre>');
		$long = 0;
		foreach($list as $key => $value){
			$url = ENV('DATA_URL')."/galeri/".$value->nama_file;
			$size = getimagesize($url);
			$width = $size[0];
			$height = $size[1];
			if($height > $width && $long == 0){
				$long = 1;
				$array['long'] = array('nama_file' => $value->nama_file);
			}else{
				array_push($array['short'], array('nama_file' => $value->nama_file));
			}
			// array_push($array, )
		}
		// print_r($array);
		return collect($array);
		// exit;
	}

    public function checkuniquedata($field, $value){
    	$hasil['is_exist'] = false;
    	$data = DB::table('member')
    				->select('id')
    				->where($field, '=', $value)
    				->first();
    	if (!is_null($data)) {
    		$hasil['is_exist'] = true;
    	}

    	return $hasil['is_exist'];
    }

    public function setMember($request){
    	$hasil['is_error'] = false;
    	$hasil['no_hp'] = false;
    	$hasil['email'] = false;
    	if ($this->checkuniquedata('email', $request->input('email_register'))) {
    		$hasil['email'] = true;
    		$hasil['is_error'] = true;
    	}
    	if ($this->checkuniquedata('no_hp', $request->input('telepon_register'))) {
    		$hasil['no_hp'] = true;
    		$hasil['is_error'] = true;
    	}

    	if (!$hasil['is_error']) {
    		$id = DB::table('member')
    				->insertGetId([
    					'nama' 		=> $request->input('name_register'),
    					'password'  => Hash::make('01X'.$request->input('password_register').'@#'),
    					'no_hp'		=> $request->input('telepon_register'),
    					'email'		=> $request->input('email_register'),
						'foto_profile' => 'user.png',
    					'status'	=> 'not verified',
						'waktu_daftar' => Carbon::now()
    				]);
    		$token = md5(uniqid('newmember-'.$id, true));
    		$dt = Carbon::now();
    		DB::table('member_verifikasi')
    			->insert([
    				'email' 		=> $request->input('email_register'),
    				'token'			=> $token,
    				'waktu_selesai'	=> $dt->addHours(24),
    				'status_email'	=> 'belum',
    				'waktu'			=> $dt->toDateTimeString(),
    				'id_member'		=> $id
    		]);
    		if (!$this->checkuniquedata('id', $id)) {
    			$hasil['is_error'] = true;
    		}else{
    			$this->setsendVerify($token);
    		}
    	}

    	return $hasil;

    }

    public function CheckVerify($request){
    	$hasil['expired'] = false;
    	$hasil['is_exist'] = false;
    	$hasil['verified'] = false;
		$dt   = Carbon::now();
		$data = DB::table('member_verifikasi')
				->select('id_member', 'waktu_selesai')
				->where('token', '=', $request->token)
				->where('email', '=', $request->email)
				->first();
		if (!is_null($data)) {
			$datamember = DB::table('member')
							->select('status')
							->where('id', '=', $data->id_member)
							->first();
			$selesai = new Carbon($data->waktu_selesai);
			if ($datamember->status=="aktif" || ($selesai->greaterThanOrEqualTo($dt))) {
				if ($datamember->status == "not verified") {
					DB::table('member')
						->where('id', $data->id_member)
						->update([
							'status' => 'aktif'
						]);
					setcookie('success_verifiy', 'true', time() + (86400 * 1), "/");
				}
				$hasil['verified'] = true;
			}			
		}
		return $hasil;
	    // print('<pre>');
	    // print_r(json_encode($hasil));
	    // exit();
    }

    public function CheckLogin($request){
    	$hasil['is_success'] = false;
    	$hasil['is_exist'] = false;
    	$hasil['is_verified'] = true;
    	if (is_numeric($request->input('username')) == 1) {
    		$field = 'no_hp';
    	}else{
    		$field = 'email';
    	}

    	$data = DB::table('member')
    				->select('id','email', 'no_hp','status', 'password')
    				->where($field, '=', $request->input('username'))
    				->first();
    	if (!is_null($data)) {
    		$hasil['is_exist'] = true;
    		if ($data->status=="not verified") {
    			$hasil['is_verified'] = false;
    		}else{
    			$hasil['s']=$request->input('password');
	    		if ($request->input('password') !="" && Hash::check('01X'.$request->input('password').'@#', $data->password)) {
	    			$hasil['is_success'] = true;
					$credentials = ['email' => $data->email, 'password' => '01X'.$request->input('password').'@#'];
			        if (Auth::guard('web')->attempt($credentials)) {
			            $request->session()->regenerate();
			        }
					if (Hash::needsRehash($data->password)) {
					    $hashed = Hash::make('01X'.$request->input('password').'@#');
					    DB::table('member')
					    	->where('id', $data->id)
					    	->update([
					    		'password' => $hashed
					    	]);
					}
	    		}
    		}
    	}
    	return $hasil;
    }

    public function setsendVerify($token){
        Config::set('mail.mailers.smtp.username', HelpersHelper::getSetting('conf_email'));
        Config::set('mail.mailers.smtp.password', HelpersHelper::getSetting('conf_pass'));
        Config::set('mail.from.address', HelpersHelper::getSetting('conf_email'));
        Config::set('mail.from.name', "KLOP");

        $data = DB::table('member_verifikasi')
        			->join('member', 'member.id', '=', 'member_verifikasi.id_member')
        			->select('member.nama', 'member.email', 'member.id')
        			->where('member_verifikasi.token', $token)
        			->first();
        \Mail::to($data->email)
        ->send(new \App\Mail\VerifyMail($data->id, $token, $data->nama,$data->email));
        DB::table('member_verifikasi')
        	->where('token', $token)
        	->update([
        		'status_email'	=> 'terkirim'
        	]);
    }

    public function setResendVerify($request){
    	if (is_numeric($request->input('username')) == 1) {
    		$field = 'no_hp';
    	}else{
    		$field = 'email';
    	}

    	$data = DB::table('member')
    				->select('id', 'email')
    				->where($field,'=',$request->username)
    				->first();
    	if (!is_null($data)) {
    		$dataverify = DB::table('member_verifikasi')
    						->select('token')
    						->where('id_member','=', $data->id)
    						->where('email', '=',$data->email)
    						->first();
    		$dt = Carbon::now();
    		if (is_null($dataverify)) {
    			$token = md5(uniqid('newmember-'.$data->id, true));
	    		DB::table('member_verifikasi')
	    			->insert([
	    				'email' 		=> $data->email,
	    				'token'			=> $token,
	    				'waktu_selesai'	=> $dt->addHours(24),
	    				'status_email'	=> 'belum',
	    				'waktu'			=> $dt->toDateTimeString(),
	    				'id_member'		=> $data->id
	    		]);
    		}else{
    			$token = $dataverify->token;
    			DB::table('member_verifikasi')
    				->where('token', $token)
    				->update([
    					'waktu_selesai'	=> $dt->addHours(24),
    				]);
    		}
    		$this->setsendVerify($token);
    		return true;
    	}else{
    		return false;
    	}

    }

    public function sendForgetPass($request){
        Config::set('mail.mailers.smtp.username', HelpersHelper::getSetting('conf_email'));
        Config::set('mail.mailers.smtp.password', HelpersHelper::getSetting('conf_pass'));
        Config::set('mail.from.address', HelpersHelper::getSetting('conf_email'));
        Config::set('mail.from.name', "KLOP");
    	$hasil['is_success'] = false;
    	$dateTime = Carbon::now();
    	$waktu_selesai = $dateTime->addHours(1);
    	$data = DB::table('member')
    				->select('id', 'nama')
    				->where('email', '=', $request->input('email_password'))
    				->first();
    	if (!is_null($data)) {
    		$token = md5(uniqid('forgetPass-'.$data->id, true)).md5(uniqid('forgetPass-'.$data->id, true));
    		DB::table('member_forgot_password')
    			->insert([
    				'id_member' => $data->id,
    				'email'		=> $request->input('email_password'),
    				'token'		=> $token,
    				'waktu'		=> $dateTime,
    				'waktu_selesai'	=> $waktu_selesai,
    				'status'		=> 'belum'    			
    			]);
	        \Mail::to($request->input('email_password'))->send(new \App\Mail\ResetPassword($token, $request->input('email_password'), $data->nama));
			DB::table('member_forgot_password')
    			->where('token', $token)
    			->update([
    				'status' => 'terkirim'
    			]);
    			$hasil['is_success'] = true;
    	}

    	return $hasil;
    }

    public function checkavailableForgetPass($email, $token){
    	$hasil['is_exist'] = false;
    	$hasil['is_expired'] = true;
    	$hasil['is_success'] = false;
    	if (isset($email) && isset($token) && $email !="" && $token !="") {
    		$dateTime = Carbon::now();
    		$data = DB::table('member_forgot_password')
    					->select('waktu_selesai', 'status', 'id_member')
    					->where('token', '=', $token)
    					->first();
    		if (!is_null($data)) {
    			if ($data->status != 'diklaim') {
    				$hasil['is_exist'] = true;
	    			$waktu_selesai = new Carbon ($data->waktu_selesai);
	    			if ($waktu_selesai->greaterThanOrEqualTo($dateTime)) {
	    				$hasil['is_expired'] = false;
	    				$hasil['is_success'] = true;
	    				$hasil['token'] = $token;
	    			}
    			}
    		}
    	}
    	return $hasil;
    }

    public function setresetpassword($request){
    	$hasil['is_success'] = false;
    	$check = $this->checkavailableForgetPass($request->input('email'), $request->input('token'));
    	if ($check['is_success'] && $request->input('password') == $request->input('password_repeat')) {
    		$data = DB::table('member_forgot_password')
    					->select('waktu_selesai', 'status', 'id_member')
    					->where('token', '=', $request->input('token'))
    					->first();
    		$password_new = Hash::make('01X'.$request->input('password').'@#');
    		DB::table('member')
    			->where('id', $data->id_member)
    			->update([
    				'password' => $password_new
    			]);

    		DB::table('member_forgot_password')
    			->where('token', $request->input('token'))
    			->update([
    				'status' => 'diklaim'
    			]);
    		$hasil['is_success'] = true;
    	}

    	return $hasil;
    }


}
