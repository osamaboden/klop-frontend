<?php

namespace App\Models;

use App\Helpers\helper as Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\URL;

class CheckoutModel extends Model
{
    use HasFactory;

    public function getDataCheckout($request)
    {
        $id = Auth::user()->id;
        $list = [];
        $total_cart = DB::table('keranjang')
                        ->where('id_member', $id)
                        ->sum('qty');
        $hasil['proyek'] = $this->getListProyek();
        $hasil['data'] = (object) ['nama' => Auth::user()->nama,'nomor' => Auth::user()->no_hp,'id' => Auth::user()->id];
        $data = $this->getAllAlamat();
        foreach ($data as $key => $value) {
            // $ongkir = $this->countOngkir($value->id, $total_cart);
            array_push($list, (object) ['id' => $value->id, 'nama' => $value->nama, 'alamat' => $value->alamat]);
        }
        $listalamat = collect($list);
        $hasil['alamat']['list'] = $listalamat;
        if($this->CheckAddresexist()){
            $hasil['alamat']['active'] = $this->getAlamatBy(session('id_alamat'));
            $hasil['ongkir']['active'] =  0;
        }else{
            $hasil['alamat']['active'] = null;
            $hasil['ongkir']['active'] = 0;
        }
        $hasil['keranjang'] = Helper::getlistKeranjang();
        // print('<pre>');print_r($hasil);exit;
        return $hasil;
    }

    public function CheckAddresexist()
    {
        $id =Auth::user()->id;
        $data = DB::table('order')
                    ->where('id_member', $id)
                    ->where('delivery', 'ya')
                    ->select('alamat_kirim')
                    ->orderBy('waktu_order', 'desc')
                    ->first();
        if(!is_null(session('id_alamat')) && session('id_alamat') != 0 && session('id_alamat') != "0"){
            return true;
        }
        if(!is_null($data)){
            if($data->alamat_kirim != "0" && $data->alamat_kirim != 0){
                session(['id_alamat' => $data->alamat_kirim]);
                return true;
            }
        }
        $alamat = DB::table('member_alamat')
                    ->where('id_member', $id)
                    ->where('status', 'aktif')
                    ->select('id')
                    ->limit(1)
                    ->first();
        if(!is_null($alamat)){
            echo '2';
            session(['id_alamat' => $alamat->id]);
            return true;
        }
        return false;
    }

    public function countOngkir($id_alamat,$qty){
        $dataAlamat = DB::table('member_alamat')
                        ->where('id', $id_alamat)
                        ->select('koordinat')
                        ->first();
        $origin = Helper::getSetting('koordinat_toko');
        $destination = str_replace(' ','',$dataAlamat->koordinat);
        $jarak = $this->countDistanceTwoPointsMaps($origin, $destination);
        // DB::enableQueryLog();
        $perkem = DB::table('biaya_delivery')
                        ->where('status', 'aktif')
                        ->whereRaw(" '".$qty."' BETWEEN min_qty_produk AND max_qty_produk")
                        ->select('harga_per_km','id')
                        ->first();
        if(is_null($perkem)){
            $perkem = DB::table('biaya_delivery')
            ->where('status', 'aktif')
            ->where('max_qty_produk', '<', $qty)
            ->select('harga_per_km', 'id')
            ->orderBy('max_qty_produk', 'desc')
            ->first();
        }  
        $hasil['id'] = $perkem->id;
        $hasil['ongkir'] = ceil($perkem->harga_per_km * $jarak);
        return $hasil;
    }

    public function countDistanceTwoPointsMaps($origin, $destination){
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$origin."&destinations=".$destination."&mode=driving&key=".Helper::getSetting('maps_api');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        if (is_null($response_a['rows'][0]['elements'][0]['distance']['value'])) {
            $dist = 0;
        }else{
            $dist = round($response_a['rows'][0]['elements'][0]['distance']['value']/1000, 1);
        }
        return $dist;
        // print('<pre>'); print_r($response_a);
    }

    public function getListProyek(){
        $list = DB::table('proyek_kategori')
                    ->where('status', 'aktif')
                    ->select('id','nama')
                    ->get();
        return $list;
    }

    public function getAlamatBy($id)
    {
        $data = DB::table('member_alamat')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('member_alamat.id', $id)
                    ->selectRaw("member_alamat.id as id, member_alamat.nama, concat(member_alamat.alamat,', ',kecamatan.nama,', ',kabupaten.nama, ', ', provinsi.nama) as alamat")
                    ->first();
        return $data;
    }

    public function getAllAlamat()
    {
        $id = Auth::user()->id;
        $data = DB::table('member_alamat')
                    ->join('kecamatan', 'member_alamat.kecamatan', '=', 'kecamatan.id')
                    ->join('kabupaten', 'member_alamat.kabupaten', '=', 'kabupaten.id')
                    ->join('provinsi', 'member_alamat.provinsi', '=', 'provinsi.id')
                    ->where('member_alamat.id_member', $id)
                    ->where('member_alamat.status', 'aktif')
                    ->selectRaw("member_alamat.id as id, member_alamat.nama, concat(member_alamat.alamat,', ',kecamatan.nama,', ',kabupaten.nama, ', ', provinsi.nama) as alamat")
                    ->get();
        return $data;
    }

    public function hitungPemasangan(){
        $id = Auth::user()->id;
        $qty = DB::table('keranjang')
                        ->where('id_member', $id)
                        ->sum('qty');
        $hasil['data'] = DB::table('biaya_installation')
                        ->where('status', 'aktif')
                        ->whereRaw(" '".$qty."' BETWEEN min_qty_produk AND max_qty_produk")
                        ->select('harga','id')
                        ->first();
        if(is_null($hasil['data'])){
            $hasil['data'] = DB::table('biaya_installation')
            ->where('status', 'aktif')
            ->where('max_qty_produk', '<', $qty)
            ->select('harga', 'id')
            ->orderBy('max_qty_produk', 'desc')
            ->first();
        }  
        $hasil['is_success'] = (is_null($hasil['data'])) ? false : true;
        return $hasil;
    }

    public function setChangeAlamat($id_alamat)
    {
        $hasil['is_success'] = true;
        session(['id_alamat' => $id_alamat]);
        $id = Auth::user()->id;
        $total_cart = DB::table('keranjang')
                        ->where('id_member', $id)
                        ->sum('qty');
        $hasil['alamat'] = $this->getAlamatBy($id_alamat);
        $hasil['ongkir'] =  $this->countOngkir($id_alamat,$total_cart);
        if(is_null($hasil['alamat'])){
            $hasil['is_success'] = false;
        }
        return $hasil;
    }

    public function getDataPageAlamat($id = ""){
        $hasil['provinsi'] = $this->listProvinsi();
        if($id != ""){
            $hasil['data'] = DB::table('member_alamat')
                                ->where('id', $id)
                                ->select('*')
                                ->first();
        }else{
            $hasil['data'] = null;
        }
        return $hasil;
    }

    public function listProvinsi()
    {
        $data = DB::table('provinsi')
                    ->where('status', 'aktif')
                    ->select('id', 'nama')
                    ->get();
        return $data;
    }

    public function listKabupaten($id)
    {
        $hasil['data'] = DB::table('kabupaten')
                    ->where('status', 'aktif')
                    ->where('id_prov', $id)
                    ->selectRaw('id, nama as text')
                    ->get();
        $hasil['is_success'] = (is_null($hasil['data'])) ? false : true;
        return $hasil;
    }
    public function listkecamatan($id)
    {
        $hasil['data'] = DB::table('kecamatan')
                    ->where('status', 'aktif')
                    ->where('id_kab', $id)
                    ->selectRaw('id, nama as text')
                    ->get();
        $hasil['is_success'] = (is_null($hasil['data'])) ? false : true;
        return $hasil;
    }

    public function setsimpanalamat($request){
        $koordinat = $request->input('koordinat');
        $id = Auth::user()->id;
        $hasil['is_success'] = false;
        if(!$koordinat){
            $koordinat = $this->getCoordinateFromAddress($request->input('address'));
        }
        if($request->input('id_alamat') == ""){
            $hasil['id'] = DB::table('member_alamat')
            ->insertGetId([
                'id_member' => $id,
                'alamat' => $request->input('address'),
                'nama' => $request->input('label_alamat'),
                'kecamatan' => $request->input('kecamatan'),
                'kabupaten' => $request->input('kabupaten'),
                'provinsi' => $request->input('provinsi'),
                'catatan' => $request->input('catatan'),
                'koordinat' => $koordinat,
                'status' => 'aktif',
            ]);
        }else{
            DB::table('member_alamat')
            ->where('id', $request->input('id_alamat'))
            ->update([
                'alamat' => $request->input('address'),
                'nama' => $request->input('label_alamat'),
                'kecamatan' => $request->input('kecamatan'),
                'kabupaten' => $request->input('kabupaten'),
                'provinsi' => $request->input('provinsi'),
                'catatan' => $request->input('catatan'),
                'koordinat' => $koordinat,
                'status' => 'aktif',
            ]);
            $hasil['id'] = $request->input('id_alamat');
        }

        if($hasil['id'] !=""){
            session(['id_alamat' => $hasil['id']]);
            $hasil['is_success'] = true;
        }
        return $hasil;
    }

    public function getCoordinateFromAddress($alamat)
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($alamat)."&sensor=true_or_false&key=".Helper::getSetting('maps_api');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $xml = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($xml, true);
        if($response_a['status'] == "OK"){
            $koordinat  = $response_a['results'][0]['geometry']['location']['lat'].",".$response_a['results'][0]['geometry']['location']['lng'];
        }else{
            $koordinat = null;
        }
        return $koordinat;
    }

    public function CreateIdOrder(){
        // KP20210501-1
        $date = Carbon::now();
        $id = "KP";
        $last = DB::table('order')
                    ->whereRaw("date(waktu_order)='".$date->format('Y-m-d')."'")
                    ->selectRaw('cast(substr(id, 12) as SIGNED) as num')
                    ->orderByRaw('cast(substr(id, 12) as SIGNED) DESC')
                    ->limit(1)
                    ->first();
        if(is_null($last)){
            $num = 1;
        }else{
            $num = $last->num + 1;
        }
        $id .= $date->format('Ymd')."-".$num;
        return $id;
    }

    public function setsaveOrder($request){
        $hasil['is_success'] = true;
        $hasil['id'] = "";
        $id_member = Auth::user()->id;
        $listdetail = Helper::getlistKeranjang();
        $id_order = $this->CreateIdOrder();
        $tanggal = Helper::convertDate($request->input('date'));
        $total_cart = DB::table('keranjang')
                        ->where('id_member', $id_member)
                        ->sum('qty');
        if($request->input('delivery') == "ya"){
            $id_alamat = $request->input('alamat');
            $ongkir = $this->countOngkir($id_alamat,$total_cart);
            $id_delivery = $ongkir['id'];
        }else{
            $id_alamat = 0;
            $id_delivery = 0;
        }
        if($request->input('instalasi')){
            $pemasangan = $this->hitungPemasangan();
            $id_installation = $pemasangan['data']->id;
        }else{
            $id_installation = 0;
        }

        $dateTime = Carbon::now();
        
        DB::table('order')
            ->insert([
                'id'                => $id_order,
                'id_member'         => $id_member,
                'waktu_order'       => $dateTime,
                'delivery'          => $request->input('delivery'),
                'request_ready'     => $tanggal." 00:00:00",
                'alamat_kirim'      => $id_alamat,
                'id_proyek'         => $request->input('proyek'),
                'nilai_delivery'    => intval($request->input('total_pengiriman')),
                'id_delivery'   => $id_delivery,
                'id_installation'   => $id_installation,
                'installation'   => $request->input('instalasi'),
                'nilai_installation' => intval($request->input('total_pemasangan')),
                'nomor_telepon'      => $request->input('nomor'),
            ]);
        if(!Helper::checkOrderExist($id_order)){
            $hasil['is_success'] = false;
            return $hasil;
        }

        foreach ($listdetail as $key => $value) {
            $satuan_beli = ($value['satuan_beli'] == "m") ? 'meter' : 'pcs';
            $meter = ($value['satuan_beli'] == "m") ? $value['qty'] : null;
            DB::table('order_produk')
                ->insert([
                    'id_order' => $id_order,
                    'id_produk' => $value['id_produk'],
                    'qty'       => $value['qty_pcs'],
                    'qty_meter'       => $meter,
                    'satuan_beli'       => $satuan_beli,
                    'harga_total' => $value['harga_total'],
                    'diskon' => null,
                    'satuan_diskon' => 'persen',
                    'catatan'       => null,
                    'status'        => 'aktif'
                ]);
        }

        // DB::table('order_status')
        //     ->insert([
        //         'id_order'          => $id_order,
        //         'waktu_perubahan'   => $dateTime,
        //         'status'            => 'baru',
        //         'id_admin'          => '0',
        //     ]);
        DB::table('order_status')
            ->insert([
                'id_order'          => $id_order,
                'waktu_perubahan'   => $dateTime->addSecond(1),
                'status'            => 'menunggu pembayaran',
                'id_admin'          => '0',
            ]);
        $hasil['id'] = $id_order;
        $hasil['redirect'] = URL::to('/success/'.$id_order);
        DB::table('keranjang')
            ->where('id_member','=',$id_member)
            ->delete();
        return $hasil;
    }

    public function getDataPageOrder($id){
        DB::enableQueryLog();
        $hasil['list'] = DB::table('order_produk')
                    ->join('produk', 'order_produk.id_produk', '=', 'produk.id')
                    ->where('id_order', '=', $id)
                    ->select('produk.nama', 'order_produk.qty', 'order_produk.harga_total')
                    ->get();
        $total_cart = 0;
        foreach ($hasil['list'] as $key => $value) {
            $total_cart += $value->harga_total;  
        }
        $hasil['data'] = DB::table('order')
                            ->where('id', '=', $id)
                            ->select('nilai_delivery', 'nilai_installation')
                            ->first();
        $hasil['total'] = $total_cart + $hasil['data']->nilai_delivery + $hasil['data']->nilai_installation;
        // dd(DB::flushQueryLog());
        // exit;
        $hasil['id'] = $id;
        return $hasil;
    }

    public function hapusAlamat($request){
        $hasil['is_success'] = false;
        if(!is_null($request->id_alamat)){
            if(DB::table('member_alamat')->where('id', '=', $request->id_alamat)->update(['status' => 'delete'])){
                $hasil['is_success'] = true;
                $hasil['id_alamat'] = $request->id_alamat;
            }
        }
        $hasil['request'] = $request;
        return $hasil;
    }

}
