<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data = array();
    public function __construct($id, $token, $nama, $email)
    {
        //
        $this->data['id'] = $id;
        $this->data['token'] = URL::to('/verifikasi')."?email=".$email."&token=".$token;
        $this->data['nama']  = $nama;
        // var_dump($this->data);exit();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Verifikasi Email')->view('emails.verifikasi');
    }
}
